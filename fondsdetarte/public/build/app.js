(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["app"],{

/***/ "./assets/css/app.scss":
/*!*****************************!*\
  !*** ./assets/css/app.scss ***!
  \*****************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./assets/js/app.js":
/*!**************************!*\
  !*** ./assets/js/app.js ***!
  \**************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(global) {/* harmony import */ var core_js_modules_es_array_find__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! core-js/modules/es.array.find */ "./node_modules/core-js/modules/es.array.find.js");
/* harmony import */ var core_js_modules_es_array_find__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(core_js_modules_es_array_find__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _css_app_scss__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../css/app.scss */ "./assets/css/app.scss");
/* harmony import */ var _css_app_scss__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(_css_app_scss__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! jquery */ "./node_modules/jquery/dist/jquery.js");
/* harmony import */ var jquery__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(jquery__WEBPACK_IMPORTED_MODULE_2__);


/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */
// any CSS you import will output into a single css file (app.css in this case)
 // Need jQuery? Install it with "yarn add jquery", then uncomment to import it.


global.$ = global.jQuery = jquery__WEBPACK_IMPORTED_MODULE_2___default.a; //console.log('Hello Webpack Encore! Edit me in assets/js/app.js');

__webpack_require__(/*! bootstrap */ "./node_modules/bootstrap/dist/js/bootstrap.js");

__webpack_require__(/*! @fortawesome/fontawesome-free/css/all.min.css */ "./node_modules/@fortawesome/fontawesome-free/css/all.min.css");

__webpack_require__(/*! @fortawesome/fontawesome-free/js/all.js */ "./node_modules/@fortawesome/fontawesome-free/js/all.js");

jquery__WEBPACK_IMPORTED_MODULE_2___default()(document).ready(function () {
  jquery__WEBPACK_IMPORTED_MODULE_2___default()('[data-toggle="tooltip"]').tooltip(); // AJAX sur listing d'entités

  jquery__WEBPACK_IMPORTED_MODULE_2___default()('form[data-filtre]').change(function () {
    var form = jquery__WEBPACK_IMPORTED_MODULE_2___default()(this);
    jquery__WEBPACK_IMPORTED_MODULE_2___default.a.ajax(jquery__WEBPACK_IMPORTED_MODULE_2___default.a.extend({
      url: form.data('action'),
      type: 'POST',
      data: form.serialize(),
      success: function success(data) {
        jquery__WEBPACK_IMPORTED_MODULE_2___default()("#" + form.data('list')).html(data);
      }
    }, defaultAjaxOptions()));
  }).change();
  jquery__WEBPACK_IMPORTED_MODULE_2___default()('form[data-filtre]').submit(function () {
    return false;
  });
}); // Modal de confirmation de suppression

jquery__WEBPACK_IMPORTED_MODULE_2___default()(document).on('submit', 'form[data-confirmation]', function (event) {
  var $form = jquery__WEBPACK_IMPORTED_MODULE_2___default()(this),
      $confirm = jquery__WEBPACK_IMPORTED_MODULE_2___default()('#confirmationModal');

  if ($confirm.data('result') !== 'yes') {
    //cancel submit event
    event.preventDefault();
    $confirm.off('click', '#btnYes').on('click', '#btnYes', function () {
      $confirm.data('result', 'yes');
      $form.find('input[type="submit"]').attr('disabled', 'disabled');
      $form.submit();
    }).modal('show');
  }
}); // Modal de confirmation de suppression en AJAX

jquery__WEBPACK_IMPORTED_MODULE_2___default()(document).on('submit', 'form[data-ajaxconfirmation]', function (event) {
  var $form = jquery__WEBPACK_IMPORTED_MODULE_2___default()(this),
      $confirm = jquery__WEBPACK_IMPORTED_MODULE_2___default()('#confirmationModal');

  if ($confirm.data('result') !== 'yes') {
    //cancel submit event
    event.preventDefault();
    $confirm.off('click', '#btnYes').on('click', '#btnYes', function () {
      $confirm.data('result', 'yes');
      $form.find('input[type="submit"]').attr('disabled', 'disabled'); // Suppression via AJAX

      jquery__WEBPACK_IMPORTED_MODULE_2___default.a.ajax(jquery__WEBPACK_IMPORTED_MODULE_2___default.a.extend({
        url: $form.attr('action'),
        type: $form.attr('method'),
        data: $form.serialize(),
        success: function success(data) {
          if (data == 'ok') {
            jquery__WEBPACK_IMPORTED_MODULE_2___default.a.ajax(jquery__WEBPACK_IMPORTED_MODULE_2___default.a.extend({
              url: $form.data('callback'),
              type: 'GET',
              success: function success(data) {
                jquery__WEBPACK_IMPORTED_MODULE_2___default()("#" + $form.data('list')).html(data);
              }
            }, defaultAjaxOptions()));
          } else {
            alert(data);
          }
        }
      }, defaultAjaxOptions()));
    }).modal('show');
  }
}); // Initialisation des var et chargement du modal pour formulaire AJAX

jquery__WEBPACK_IMPORTED_MODULE_2___default()("#formModal").on("show.bs.modal", function (e) {
  var title = jquery__WEBPACK_IMPORTED_MODULE_2___default()(e.relatedTarget).attr("data-title");
  var href = jquery__WEBPACK_IMPORTED_MODULE_2___default()(e.relatedTarget).attr("data-href");

  if (title && href) {
    jquery__WEBPACK_IMPORTED_MODULE_2___default()("#formModalBody").load(href, function (response, status, xhr) {
      if (xhr.status === 403) {
        window.location.reload();
      }
    });
    jquery__WEBPACK_IMPORTED_MODULE_2___default()('#formModalLabel').html("<i class='fas fa-pencil-alt'></i> " + title);
  }
}); // Déclenchement des requêtes AJAX de soumission de formulaires modal (hors fichiers)

jquery__WEBPACK_IMPORTED_MODULE_2___default()(document).on('submit', 'form[data-async]', function (event) {
  var $form = jquery__WEBPACK_IMPORTED_MODULE_2___default()(this);
  jquery__WEBPACK_IMPORTED_MODULE_2___default.a.ajax(jquery__WEBPACK_IMPORTED_MODULE_2___default.a.extend({
    url: $form.attr('action'),
    data: $form.serialize(),
    method: $form.attr('method'),
    success: function success(data) {
      if (data == 'ok') {
        jquery__WEBPACK_IMPORTED_MODULE_2___default()('#formModalLabel').html('');
        jquery__WEBPACK_IMPORTED_MODULE_2___default()('#formModalBody').html('');
        jquery__WEBPACK_IMPORTED_MODULE_2___default()('#formModal').modal('hide'); // Raffraîchissement AJAX

        if ($form.data('callback')) {
          jquery__WEBPACK_IMPORTED_MODULE_2___default.a.ajax(jquery__WEBPACK_IMPORTED_MODULE_2___default.a.extend({
            url: $form.data('callback'),
            type: 'GET',
            success: function success(data) {
              jquery__WEBPACK_IMPORTED_MODULE_2___default()("#" + $form.data('list')).html(data);
            }
          }, defaultAjaxOptions()));
        }

        if ($form.data('callback2')) {
          jquery__WEBPACK_IMPORTED_MODULE_2___default.a.ajax(jquery__WEBPACK_IMPORTED_MODULE_2___default.a.extend({
            url: $form.data('callback2'),
            type: 'GET',
            success: function success(data) {
              jquery__WEBPACK_IMPORTED_MODULE_2___default()("#" + $form.data('list2')).html(data);
            }
          }, defaultAjaxOptions()));
        } // Ou redirection de la page entière

      } else if (data.substr(0, 8) == 'redirect') {
        window.location.assign(data.substr(9));
      } else {
        jquery__WEBPACK_IMPORTED_MODULE_2___default()('#formModalBody').html(data);
      }
    }
  }, defaultAjaxOptions()));
  event.preventDefault();
}); // Propriétés par défaut pour les requêtes AJAX

function defaultAjaxOptions() {
  return {
    cache: false,
    beforeSend: function beforeSend() {
      jquery__WEBPACK_IMPORTED_MODULE_2___default()('#loader').show();
    },
    complete: function complete() {
      jquery__WEBPACK_IMPORTED_MODULE_2___default()('#loader').hide();
    },
    error: function error(xhr, ajaxOptions, thrownError) {
      alert(xhr.status);

      if (xhr.status === 403) {//window.location.href = '/';
      } else {
        alert('Une erreur est survenue.');
      }
    }
  };
}

global.defaultAjaxOptions = defaultAjaxOptions;

function getRadioButtonValue(name) {
  var value = '';
  var selected = jquery__WEBPACK_IMPORTED_MODULE_2___default()("input[type='radio'][name='" + name + "']:checked");

  if (selected.length > 0) {
    value = selected.val();
  }

  return value;
}

window.getRadioButtonValue = getRadioButtonValue;
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../node_modules/webpack/buildin/global.js */ "./node_modules/webpack/buildin/global.js")))

/***/ })

},[["./assets/js/app.js","runtime","vendors~app~form~fullcalendar~typeahead","vendors~app~fullcalendar~typeahead","vendors~app~typeahead","vendors~app"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvY3NzL2FwcC5zY3NzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy9hcHAuanMiXSwibmFtZXMiOlsiZ2xvYmFsIiwiJCIsImpRdWVyeSIsInJlcXVpcmUiLCJkb2N1bWVudCIsInJlYWR5IiwidG9vbHRpcCIsImNoYW5nZSIsImZvcm0iLCJhamF4IiwiZXh0ZW5kIiwidXJsIiwiZGF0YSIsInR5cGUiLCJzZXJpYWxpemUiLCJzdWNjZXNzIiwiaHRtbCIsImRlZmF1bHRBamF4T3B0aW9ucyIsInN1Ym1pdCIsIm9uIiwiZXZlbnQiLCIkZm9ybSIsIiRjb25maXJtIiwicHJldmVudERlZmF1bHQiLCJvZmYiLCJmaW5kIiwiYXR0ciIsIm1vZGFsIiwiYWxlcnQiLCJlIiwidGl0bGUiLCJyZWxhdGVkVGFyZ2V0IiwiaHJlZiIsImxvYWQiLCJyZXNwb25zZSIsInN0YXR1cyIsInhociIsIndpbmRvdyIsImxvY2F0aW9uIiwicmVsb2FkIiwibWV0aG9kIiwic3Vic3RyIiwiYXNzaWduIiwiY2FjaGUiLCJiZWZvcmVTZW5kIiwic2hvdyIsImNvbXBsZXRlIiwiaGlkZSIsImVycm9yIiwiYWpheE9wdGlvbnMiLCJ0aHJvd25FcnJvciIsImdldFJhZGlvQnV0dG9uVmFsdWUiLCJuYW1lIiwidmFsdWUiLCJzZWxlY3RlZCIsImxlbmd0aCIsInZhbCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBQUEsdUM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ0FBOzs7Ozs7QUFPQTtDQUdBOztBQUNBO0FBQ0FBLE1BQU0sQ0FBQ0MsQ0FBUCxHQUFXRCxNQUFNLENBQUNFLE1BQVAsR0FBZ0JELDZDQUEzQixDLENBRUE7O0FBRUFFLG1CQUFPLENBQUMsZ0VBQUQsQ0FBUDs7QUFDQUEsbUJBQU8sQ0FBQyxtSEFBRCxDQUFQOztBQUNBQSxtQkFBTyxDQUFDLHVHQUFELENBQVA7O0FBRUFGLDZDQUFDLENBQUNHLFFBQUQsQ0FBRCxDQUFZQyxLQUFaLENBQWtCLFlBQVc7QUFDekJKLCtDQUFDLENBQUMseUJBQUQsQ0FBRCxDQUE2QkssT0FBN0IsR0FEeUIsQ0FHekI7O0FBQ0FMLCtDQUFDLENBQUMsbUJBQUQsQ0FBRCxDQUF1Qk0sTUFBdkIsQ0FBOEIsWUFBWTtBQUN0QyxRQUFJQyxJQUFJLEdBQUdQLDZDQUFDLENBQUMsSUFBRCxDQUFaO0FBRUFBLGlEQUFDLENBQUNRLElBQUYsQ0FBT1IsNkNBQUMsQ0FBQ1MsTUFBRixDQUFTO0FBQ1pDLFNBQUcsRUFBR0gsSUFBSSxDQUFDSSxJQUFMLENBQVUsUUFBVixDQURNO0FBRVpDLFVBQUksRUFBRSxNQUZNO0FBR1pELFVBQUksRUFBR0osSUFBSSxDQUFDTSxTQUFMLEVBSEs7QUFJWkMsYUFBTyxFQUFFLGlCQUFTSCxJQUFULEVBQWU7QUFDcEJYLHFEQUFDLENBQUMsTUFBTU8sSUFBSSxDQUFDSSxJQUFMLENBQVUsTUFBVixDQUFQLENBQUQsQ0FBMkJJLElBQTNCLENBQWdDSixJQUFoQztBQUNIO0FBTlcsS0FBVCxFQU9KSyxrQkFBa0IsRUFQZCxDQUFQO0FBUUgsR0FYRCxFQVdHVixNQVhIO0FBYUFOLCtDQUFDLENBQUMsbUJBQUQsQ0FBRCxDQUF1QmlCLE1BQXZCLENBQThCLFlBQVk7QUFDdEMsV0FBTyxLQUFQO0FBQ0gsR0FGRDtBQUdILENBcEJELEUsQ0FzQkE7O0FBQ0FqQiw2Q0FBQyxDQUFDRyxRQUFELENBQUQsQ0FBWWUsRUFBWixDQUFlLFFBQWYsRUFBeUIseUJBQXpCLEVBQW9ELFVBQVVDLEtBQVYsRUFBaUI7QUFDakUsTUFBSUMsS0FBSyxHQUFHcEIsNkNBQUMsQ0FBQyxJQUFELENBQWI7QUFBQSxNQUNJcUIsUUFBUSxHQUFHckIsNkNBQUMsQ0FBQyxvQkFBRCxDQURoQjs7QUFHQSxNQUFJcUIsUUFBUSxDQUFDVixJQUFULENBQWMsUUFBZCxNQUE0QixLQUFoQyxFQUF1QztBQUNuQztBQUNBUSxTQUFLLENBQUNHLGNBQU47QUFFQUQsWUFBUSxDQUNIRSxHQURMLENBQ1MsT0FEVCxFQUNrQixTQURsQixFQUVLTCxFQUZMLENBRVEsT0FGUixFQUVpQixTQUZqQixFQUU0QixZQUFZO0FBQ2hDRyxjQUFRLENBQUNWLElBQVQsQ0FBYyxRQUFkLEVBQXdCLEtBQXhCO0FBQ0FTLFdBQUssQ0FBQ0ksSUFBTixDQUFXLHNCQUFYLEVBQW1DQyxJQUFuQyxDQUF3QyxVQUF4QyxFQUFvRCxVQUFwRDtBQUNBTCxXQUFLLENBQUNILE1BQU47QUFDSCxLQU5MLEVBT0tTLEtBUEwsQ0FPVyxNQVBYO0FBUUg7QUFDSixDQWpCRCxFLENBbUJBOztBQUNBMUIsNkNBQUMsQ0FBQ0csUUFBRCxDQUFELENBQVllLEVBQVosQ0FBZSxRQUFmLEVBQXlCLDZCQUF6QixFQUF3RCxVQUFVQyxLQUFWLEVBQWlCO0FBQ3JFLE1BQUlDLEtBQUssR0FBR3BCLDZDQUFDLENBQUMsSUFBRCxDQUFiO0FBQUEsTUFDSXFCLFFBQVEsR0FBR3JCLDZDQUFDLENBQUMsb0JBQUQsQ0FEaEI7O0FBR0EsTUFBSXFCLFFBQVEsQ0FBQ1YsSUFBVCxDQUFjLFFBQWQsTUFBNEIsS0FBaEMsRUFBdUM7QUFDbkM7QUFDQVEsU0FBSyxDQUFDRyxjQUFOO0FBRUFELFlBQVEsQ0FDSEUsR0FETCxDQUNTLE9BRFQsRUFDa0IsU0FEbEIsRUFFS0wsRUFGTCxDQUVRLE9BRlIsRUFFaUIsU0FGakIsRUFFNEIsWUFBWTtBQUNoQ0csY0FBUSxDQUFDVixJQUFULENBQWMsUUFBZCxFQUF3QixLQUF4QjtBQUNBUyxXQUFLLENBQUNJLElBQU4sQ0FBVyxzQkFBWCxFQUFtQ0MsSUFBbkMsQ0FBd0MsVUFBeEMsRUFBb0QsVUFBcEQsRUFGZ0MsQ0FJaEM7O0FBQ0F6QixtREFBQyxDQUFDUSxJQUFGLENBQU9SLDZDQUFDLENBQUNTLE1BQUYsQ0FBUztBQUNaQyxXQUFHLEVBQUVVLEtBQUssQ0FBQ0ssSUFBTixDQUFXLFFBQVgsQ0FETztBQUVaYixZQUFJLEVBQUVRLEtBQUssQ0FBQ0ssSUFBTixDQUFXLFFBQVgsQ0FGTTtBQUdaZCxZQUFJLEVBQUdTLEtBQUssQ0FBQ1AsU0FBTixFQUhLO0FBSVpDLGVBQU8sRUFBRSxpQkFBU0gsSUFBVCxFQUFlO0FBQ3BCLGNBQUlBLElBQUksSUFBSSxJQUFaLEVBQWtCO0FBQ2RYLHlEQUFDLENBQUNRLElBQUYsQ0FBT1IsNkNBQUMsQ0FBQ1MsTUFBRixDQUFTO0FBQ1pDLGlCQUFHLEVBQUVVLEtBQUssQ0FBQ1QsSUFBTixDQUFXLFVBQVgsQ0FETztBQUVaQyxrQkFBSSxFQUFFLEtBRk07QUFHWkUscUJBQU8sRUFBRSxpQkFBVUgsSUFBVixFQUFnQjtBQUNyQlgsNkRBQUMsQ0FBQyxNQUFNb0IsS0FBSyxDQUFDVCxJQUFOLENBQVcsTUFBWCxDQUFQLENBQUQsQ0FBNEJJLElBQTVCLENBQWlDSixJQUFqQztBQUNIO0FBTFcsYUFBVCxFQU1KSyxrQkFBa0IsRUFOZCxDQUFQO0FBT0gsV0FSRCxNQVFPO0FBQ0hXLGlCQUFLLENBQUNoQixJQUFELENBQUw7QUFDSDtBQUNKO0FBaEJXLE9BQVQsRUFpQkpLLGtCQUFrQixFQWpCZCxDQUFQO0FBa0JILEtBekJMLEVBMEJLVSxLQTFCTCxDQTBCVyxNQTFCWDtBQTJCSDtBQUNKLENBcENELEUsQ0FzQ0E7O0FBQ0ExQiw2Q0FBQyxDQUFDLFlBQUQsQ0FBRCxDQUFnQmtCLEVBQWhCLENBQW1CLGVBQW5CLEVBQW9DLFVBQVNVLENBQVQsRUFBWTtBQUM1QyxNQUFJQyxLQUFLLEdBQUc3Qiw2Q0FBQyxDQUFDNEIsQ0FBQyxDQUFDRSxhQUFILENBQUQsQ0FBbUJMLElBQW5CLENBQXdCLFlBQXhCLENBQVo7QUFDQSxNQUFJTSxJQUFJLEdBQUcvQiw2Q0FBQyxDQUFDNEIsQ0FBQyxDQUFDRSxhQUFILENBQUQsQ0FBbUJMLElBQW5CLENBQXdCLFdBQXhCLENBQVg7O0FBRUEsTUFBSUksS0FBSyxJQUFJRSxJQUFiLEVBQW1CO0FBQ2YvQixpREFBQyxDQUFDLGdCQUFELENBQUQsQ0FBb0JnQyxJQUFwQixDQUF5QkQsSUFBekIsRUFBK0IsVUFBU0UsUUFBVCxFQUFtQkMsTUFBbkIsRUFBMkJDLEdBQTNCLEVBQWdDO0FBQzNELFVBQUlBLEdBQUcsQ0FBQ0QsTUFBSixLQUFlLEdBQW5CLEVBQXdCO0FBQ3BCRSxjQUFNLENBQUNDLFFBQVAsQ0FBZ0JDLE1BQWhCO0FBQ0g7QUFDSixLQUpEO0FBTUF0QyxpREFBQyxDQUFDLGlCQUFELENBQUQsQ0FBcUJlLElBQXJCLENBQTBCLHVDQUF1Q2MsS0FBakU7QUFDSDtBQUNKLENBYkQsRSxDQWVBOztBQUNBN0IsNkNBQUMsQ0FBQ0csUUFBRCxDQUFELENBQVllLEVBQVosQ0FBZSxRQUFmLEVBQXlCLGtCQUF6QixFQUE2QyxVQUFVQyxLQUFWLEVBQWlCO0FBQzFELE1BQUlDLEtBQUssR0FBR3BCLDZDQUFDLENBQUMsSUFBRCxDQUFiO0FBRUFBLCtDQUFDLENBQUNRLElBQUYsQ0FBT1IsNkNBQUMsQ0FBQ1MsTUFBRixDQUFTO0FBQ1pDLE9BQUcsRUFBRVUsS0FBSyxDQUFDSyxJQUFOLENBQVcsUUFBWCxDQURPO0FBRVpkLFFBQUksRUFBRVMsS0FBSyxDQUFDUCxTQUFOLEVBRk07QUFHWjBCLFVBQU0sRUFBRW5CLEtBQUssQ0FBQ0ssSUFBTixDQUFXLFFBQVgsQ0FISTtBQUlaWCxXQUFPLEVBQUUsaUJBQVVILElBQVYsRUFBZ0I7QUFDckIsVUFBSUEsSUFBSSxJQUFJLElBQVosRUFBa0I7QUFDZFgscURBQUMsQ0FBQyxpQkFBRCxDQUFELENBQXFCZSxJQUFyQixDQUEwQixFQUExQjtBQUNBZixxREFBQyxDQUFDLGdCQUFELENBQUQsQ0FBb0JlLElBQXBCLENBQXlCLEVBQXpCO0FBQ0FmLHFEQUFDLENBQUMsWUFBRCxDQUFELENBQWdCMEIsS0FBaEIsQ0FBc0IsTUFBdEIsRUFIYyxDQUtkOztBQUNBLFlBQUlOLEtBQUssQ0FBQ1QsSUFBTixDQUFXLFVBQVgsQ0FBSixFQUE0QjtBQUN4QlgsdURBQUMsQ0FBQ1EsSUFBRixDQUFPUiw2Q0FBQyxDQUFDUyxNQUFGLENBQVM7QUFDWkMsZUFBRyxFQUFFVSxLQUFLLENBQUNULElBQU4sQ0FBVyxVQUFYLENBRE87QUFFWkMsZ0JBQUksRUFBRSxLQUZNO0FBR1pFLG1CQUFPLEVBQUUsaUJBQVVILElBQVYsRUFBZ0I7QUFDckJYLDJEQUFDLENBQUMsTUFBTW9CLEtBQUssQ0FBQ1QsSUFBTixDQUFXLE1BQVgsQ0FBUCxDQUFELENBQTRCSSxJQUE1QixDQUFpQ0osSUFBakM7QUFDSDtBQUxXLFdBQVQsRUFNSkssa0JBQWtCLEVBTmQsQ0FBUDtBQU9IOztBQUVELFlBQUlJLEtBQUssQ0FBQ1QsSUFBTixDQUFXLFdBQVgsQ0FBSixFQUE2QjtBQUN6QlgsdURBQUMsQ0FBQ1EsSUFBRixDQUFPUiw2Q0FBQyxDQUFDUyxNQUFGLENBQVM7QUFDWkMsZUFBRyxFQUFFVSxLQUFLLENBQUNULElBQU4sQ0FBVyxXQUFYLENBRE87QUFFWkMsZ0JBQUksRUFBRSxLQUZNO0FBR1pFLG1CQUFPLEVBQUUsaUJBQVVILElBQVYsRUFBZ0I7QUFDckJYLDJEQUFDLENBQUMsTUFBTW9CLEtBQUssQ0FBQ1QsSUFBTixDQUFXLE9BQVgsQ0FBUCxDQUFELENBQTZCSSxJQUE3QixDQUFrQ0osSUFBbEM7QUFDSDtBQUxXLFdBQVQsRUFNSkssa0JBQWtCLEVBTmQsQ0FBUDtBQU9ILFNBeEJhLENBeUJkOztBQUNILE9BMUJELE1BMEJPLElBQUlMLElBQUksQ0FBQzZCLE1BQUwsQ0FBWSxDQUFaLEVBQWUsQ0FBZixLQUFxQixVQUF6QixFQUFxQztBQUN4Q0osY0FBTSxDQUFDQyxRQUFQLENBQWdCSSxNQUFoQixDQUF1QjlCLElBQUksQ0FBQzZCLE1BQUwsQ0FBWSxDQUFaLENBQXZCO0FBQ0gsT0FGTSxNQUVBO0FBQ0h4QyxxREFBQyxDQUFDLGdCQUFELENBQUQsQ0FBb0JlLElBQXBCLENBQXlCSixJQUF6QjtBQUNIO0FBQ0o7QUFwQ1csR0FBVCxFQXFDSkssa0JBQWtCLEVBckNkLENBQVA7QUF1Q0FHLE9BQUssQ0FBQ0csY0FBTjtBQUNILENBM0NELEUsQ0E2Q0E7O0FBQ0EsU0FBU04sa0JBQVQsR0FBOEI7QUFDMUIsU0FBTztBQUNIMEIsU0FBSyxFQUFFLEtBREo7QUFFSEMsY0FBVSxFQUFFLHNCQUFZO0FBQ3BCM0MsbURBQUMsQ0FBQyxTQUFELENBQUQsQ0FBYTRDLElBQWI7QUFDSCxLQUpFO0FBS0hDLFlBQVEsRUFBRSxvQkFBWTtBQUNsQjdDLG1EQUFDLENBQUMsU0FBRCxDQUFELENBQWE4QyxJQUFiO0FBQ0gsS0FQRTtBQVFIQyxTQUFLLEVBQUUsZUFBVVosR0FBVixFQUFlYSxXQUFmLEVBQTRCQyxXQUE1QixFQUF5QztBQUM1Q3RCLFdBQUssQ0FBQ1EsR0FBRyxDQUFDRCxNQUFMLENBQUw7O0FBQ0EsVUFBSUMsR0FBRyxDQUFDRCxNQUFKLEtBQWUsR0FBbkIsRUFBd0IsQ0FDcEI7QUFDSCxPQUZELE1BRU87QUFDSFAsYUFBSyxDQUFDLDBCQUFELENBQUw7QUFDSDtBQUNKO0FBZkUsR0FBUDtBQWlCSDs7QUFDRDVCLE1BQU0sQ0FBQ2lCLGtCQUFQLEdBQTRCQSxrQkFBNUI7O0FBRUEsU0FBU2tDLG1CQUFULENBQTZCQyxJQUE3QixFQUFtQztBQUMvQixNQUFJQyxLQUFLLEdBQUcsRUFBWjtBQUNBLE1BQUlDLFFBQVEsR0FBR3JELDZDQUFDLENBQUMsK0JBQStCbUQsSUFBL0IsR0FBc0MsWUFBdkMsQ0FBaEI7O0FBQ0EsTUFBSUUsUUFBUSxDQUFDQyxNQUFULEdBQWtCLENBQXRCLEVBQXlCO0FBQ3JCRixTQUFLLEdBQUdDLFFBQVEsQ0FBQ0UsR0FBVCxFQUFSO0FBQ0g7O0FBRUQsU0FBT0gsS0FBUDtBQUNIOztBQUNEaEIsTUFBTSxDQUFDYyxtQkFBUCxHQUE2QkEsbUJBQTdCLEMiLCJmaWxlIjoiYXBwLmpzIiwic291cmNlc0NvbnRlbnQiOlsiLy8gZXh0cmFjdGVkIGJ5IG1pbmktY3NzLWV4dHJhY3QtcGx1Z2luIiwiLypcbiAqIFdlbGNvbWUgdG8geW91ciBhcHAncyBtYWluIEphdmFTY3JpcHQgZmlsZSFcbiAqXG4gKiBXZSByZWNvbW1lbmQgaW5jbHVkaW5nIHRoZSBidWlsdCB2ZXJzaW9uIG9mIHRoaXMgSmF2YVNjcmlwdCBmaWxlXG4gKiAoYW5kIGl0cyBDU1MgZmlsZSkgaW4geW91ciBiYXNlIGxheW91dCAoYmFzZS5odG1sLnR3aWcpLlxuICovXG5cbi8vIGFueSBDU1MgeW91IGltcG9ydCB3aWxsIG91dHB1dCBpbnRvIGEgc2luZ2xlIGNzcyBmaWxlIChhcHAuY3NzIGluIHRoaXMgY2FzZSlcbmltcG9ydCAnLi4vY3NzL2FwcC5zY3NzJztcblxuLy8gTmVlZCBqUXVlcnk/IEluc3RhbGwgaXQgd2l0aCBcInlhcm4gYWRkIGpxdWVyeVwiLCB0aGVuIHVuY29tbWVudCB0byBpbXBvcnQgaXQuXG5pbXBvcnQgJCBmcm9tICdqcXVlcnknO1xuZ2xvYmFsLiQgPSBnbG9iYWwualF1ZXJ5ID0gJDtcblxuLy9jb25zb2xlLmxvZygnSGVsbG8gV2VicGFjayBFbmNvcmUhIEVkaXQgbWUgaW4gYXNzZXRzL2pzL2FwcC5qcycpO1xuXG5yZXF1aXJlKCdib290c3RyYXAnKTtcbnJlcXVpcmUoJ0Bmb3J0YXdlc29tZS9mb250YXdlc29tZS1mcmVlL2Nzcy9hbGwubWluLmNzcycpO1xucmVxdWlyZSgnQGZvcnRhd2Vzb21lL2ZvbnRhd2Vzb21lLWZyZWUvanMvYWxsLmpzJyk7XG5cbiQoZG9jdW1lbnQpLnJlYWR5KGZ1bmN0aW9uKCkge1xuICAgICQoJ1tkYXRhLXRvZ2dsZT1cInRvb2x0aXBcIl0nKS50b29sdGlwKCk7XG5cbiAgICAvLyBBSkFYIHN1ciBsaXN0aW5nIGQnZW50aXTDqXNcbiAgICAkKCdmb3JtW2RhdGEtZmlsdHJlXScpLmNoYW5nZShmdW5jdGlvbiAoKSB7XG4gICAgICAgIHZhciBmb3JtID0gJCh0aGlzKTtcblxuICAgICAgICAkLmFqYXgoJC5leHRlbmQoe1xuICAgICAgICAgICAgdXJsIDogZm9ybS5kYXRhKCdhY3Rpb24nKSxcbiAgICAgICAgICAgIHR5cGU6ICdQT1NUJyxcbiAgICAgICAgICAgIGRhdGEgOiBmb3JtLnNlcmlhbGl6ZSgpLFxuICAgICAgICAgICAgc3VjY2VzczogZnVuY3Rpb24oZGF0YSkge1xuICAgICAgICAgICAgICAgICQoXCIjXCIgKyBmb3JtLmRhdGEoJ2xpc3QnKSkuaHRtbChkYXRhKTtcbiAgICAgICAgICAgIH1cbiAgICAgICAgfSwgZGVmYXVsdEFqYXhPcHRpb25zKCkpKTtcbiAgICB9KS5jaGFuZ2UoKTtcblxuICAgICQoJ2Zvcm1bZGF0YS1maWx0cmVdJykuc3VibWl0KGZ1bmN0aW9uICgpIHtcbiAgICAgICAgcmV0dXJuIGZhbHNlO1xuICAgIH0pO1xufSk7XG5cbi8vIE1vZGFsIGRlIGNvbmZpcm1hdGlvbiBkZSBzdXBwcmVzc2lvblxuJChkb2N1bWVudCkub24oJ3N1Ym1pdCcsICdmb3JtW2RhdGEtY29uZmlybWF0aW9uXScsIGZ1bmN0aW9uIChldmVudCkge1xuICAgIHZhciAkZm9ybSA9ICQodGhpcyksXG4gICAgICAgICRjb25maXJtID0gJCgnI2NvbmZpcm1hdGlvbk1vZGFsJyk7XG5cbiAgICBpZiAoJGNvbmZpcm0uZGF0YSgncmVzdWx0JykgIT09ICd5ZXMnKSB7XG4gICAgICAgIC8vY2FuY2VsIHN1Ym1pdCBldmVudFxuICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgICAgICRjb25maXJtXG4gICAgICAgICAgICAub2ZmKCdjbGljaycsICcjYnRuWWVzJylcbiAgICAgICAgICAgIC5vbignY2xpY2snLCAnI2J0blllcycsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAkY29uZmlybS5kYXRhKCdyZXN1bHQnLCAneWVzJyk7XG4gICAgICAgICAgICAgICAgJGZvcm0uZmluZCgnaW5wdXRbdHlwZT1cInN1Ym1pdFwiXScpLmF0dHIoJ2Rpc2FibGVkJywgJ2Rpc2FibGVkJyk7XG4gICAgICAgICAgICAgICAgJGZvcm0uc3VibWl0KCk7XG4gICAgICAgICAgICB9KVxuICAgICAgICAgICAgLm1vZGFsKCdzaG93Jyk7XG4gICAgfVxufSk7XG5cbi8vIE1vZGFsIGRlIGNvbmZpcm1hdGlvbiBkZSBzdXBwcmVzc2lvbiBlbiBBSkFYXG4kKGRvY3VtZW50KS5vbignc3VibWl0JywgJ2Zvcm1bZGF0YS1hamF4Y29uZmlybWF0aW9uXScsIGZ1bmN0aW9uIChldmVudCkge1xuICAgIHZhciAkZm9ybSA9ICQodGhpcyksXG4gICAgICAgICRjb25maXJtID0gJCgnI2NvbmZpcm1hdGlvbk1vZGFsJyk7XG5cbiAgICBpZiAoJGNvbmZpcm0uZGF0YSgncmVzdWx0JykgIT09ICd5ZXMnKSB7XG4gICAgICAgIC8vY2FuY2VsIHN1Ym1pdCBldmVudFxuICAgICAgICBldmVudC5wcmV2ZW50RGVmYXVsdCgpO1xuXG4gICAgICAgICRjb25maXJtXG4gICAgICAgICAgICAub2ZmKCdjbGljaycsICcjYnRuWWVzJylcbiAgICAgICAgICAgIC5vbignY2xpY2snLCAnI2J0blllcycsIGZ1bmN0aW9uICgpIHtcbiAgICAgICAgICAgICAgICAkY29uZmlybS5kYXRhKCdyZXN1bHQnLCAneWVzJyk7XG4gICAgICAgICAgICAgICAgJGZvcm0uZmluZCgnaW5wdXRbdHlwZT1cInN1Ym1pdFwiXScpLmF0dHIoJ2Rpc2FibGVkJywgJ2Rpc2FibGVkJyk7XG5cbiAgICAgICAgICAgICAgICAvLyBTdXBwcmVzc2lvbiB2aWEgQUpBWFxuICAgICAgICAgICAgICAgICQuYWpheCgkLmV4dGVuZCh7XG4gICAgICAgICAgICAgICAgICAgIHVybDogJGZvcm0uYXR0cignYWN0aW9uJyksXG4gICAgICAgICAgICAgICAgICAgIHR5cGU6ICRmb3JtLmF0dHIoJ21ldGhvZCcpLFxuICAgICAgICAgICAgICAgICAgICBkYXRhIDogJGZvcm0uc2VyaWFsaXplKCksXG4gICAgICAgICAgICAgICAgICAgIHN1Y2Nlc3M6IGZ1bmN0aW9uKGRhdGEpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgIGlmIChkYXRhID09ICdvaycpIHtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAkLmFqYXgoJC5leHRlbmQoe1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICB1cmw6ICRmb3JtLmRhdGEoJ2NhbGxiYWNrJyksXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgIHR5cGU6ICdHRVQnLFxuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICBzdWNjZXNzOiBmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgJChcIiNcIiArICRmb3JtLmRhdGEoJ2xpc3QnKSkuaHRtbChkYXRhKTtcbiAgICAgICAgICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIH0sIGRlZmF1bHRBamF4T3B0aW9ucygpKSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGFsZXJ0KGRhdGEpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgfSwgZGVmYXVsdEFqYXhPcHRpb25zKCkpKTtcbiAgICAgICAgICAgIH0pXG4gICAgICAgICAgICAubW9kYWwoJ3Nob3cnKTtcbiAgICB9XG59KTtcblxuLy8gSW5pdGlhbGlzYXRpb24gZGVzIHZhciBldCBjaGFyZ2VtZW50IGR1IG1vZGFsIHBvdXIgZm9ybXVsYWlyZSBBSkFYXG4kKFwiI2Zvcm1Nb2RhbFwiKS5vbihcInNob3cuYnMubW9kYWxcIiwgZnVuY3Rpb24oZSkge1xuICAgIHZhciB0aXRsZSA9ICQoZS5yZWxhdGVkVGFyZ2V0KS5hdHRyKFwiZGF0YS10aXRsZVwiKTtcbiAgICB2YXIgaHJlZiA9ICQoZS5yZWxhdGVkVGFyZ2V0KS5hdHRyKFwiZGF0YS1ocmVmXCIpO1xuXG4gICAgaWYgKHRpdGxlICYmIGhyZWYpIHtcbiAgICAgICAgJChcIiNmb3JtTW9kYWxCb2R5XCIpLmxvYWQoaHJlZiwgZnVuY3Rpb24ocmVzcG9uc2UsIHN0YXR1cywgeGhyKSB7XG4gICAgICAgICAgICBpZiAoeGhyLnN0YXR1cyA9PT0gNDAzKSB7XG4gICAgICAgICAgICAgICAgd2luZG93LmxvY2F0aW9uLnJlbG9hZCgpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9KTtcblxuICAgICAgICAkKCcjZm9ybU1vZGFsTGFiZWwnKS5odG1sKFwiPGkgY2xhc3M9J2ZhcyBmYS1wZW5jaWwtYWx0Jz48L2k+IFwiICsgdGl0bGUpO1xuICAgIH1cbn0pO1xuXG4vLyBEw6ljbGVuY2hlbWVudCBkZXMgcmVxdcOqdGVzIEFKQVggZGUgc291bWlzc2lvbiBkZSBmb3JtdWxhaXJlcyBtb2RhbCAoaG9ycyBmaWNoaWVycylcbiQoZG9jdW1lbnQpLm9uKCdzdWJtaXQnLCAnZm9ybVtkYXRhLWFzeW5jXScsIGZ1bmN0aW9uIChldmVudCkge1xuICAgIHZhciAkZm9ybSA9ICQodGhpcyk7XG5cbiAgICAkLmFqYXgoJC5leHRlbmQoe1xuICAgICAgICB1cmw6ICRmb3JtLmF0dHIoJ2FjdGlvbicpLFxuICAgICAgICBkYXRhOiAkZm9ybS5zZXJpYWxpemUoKSxcbiAgICAgICAgbWV0aG9kOiAkZm9ybS5hdHRyKCdtZXRob2QnKSxcbiAgICAgICAgc3VjY2VzczogZnVuY3Rpb24gKGRhdGEpIHtcbiAgICAgICAgICAgIGlmIChkYXRhID09ICdvaycpIHtcbiAgICAgICAgICAgICAgICAkKCcjZm9ybU1vZGFsTGFiZWwnKS5odG1sKCcnKTtcbiAgICAgICAgICAgICAgICAkKCcjZm9ybU1vZGFsQm9keScpLmh0bWwoJycpO1xuICAgICAgICAgICAgICAgICQoJyNmb3JtTW9kYWwnKS5tb2RhbCgnaGlkZScpO1xuXG4gICAgICAgICAgICAgICAgLy8gUmFmZnJhw65jaGlzc2VtZW50IEFKQVhcbiAgICAgICAgICAgICAgICBpZiAoJGZvcm0uZGF0YSgnY2FsbGJhY2snKSkge1xuICAgICAgICAgICAgICAgICAgICAkLmFqYXgoJC5leHRlbmQoe1xuICAgICAgICAgICAgICAgICAgICAgICAgdXJsOiAkZm9ybS5kYXRhKCdjYWxsYmFjaycpLFxuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogJ0dFVCcsXG4gICAgICAgICAgICAgICAgICAgICAgICBzdWNjZXNzOiBmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoXCIjXCIgKyAkZm9ybS5kYXRhKCdsaXN0JykpLmh0bWwoZGF0YSk7XG4gICAgICAgICAgICAgICAgICAgICAgICB9XG4gICAgICAgICAgICAgICAgICAgIH0sIGRlZmF1bHRBamF4T3B0aW9ucygpKSk7XG4gICAgICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICAgICAgaWYgKCRmb3JtLmRhdGEoJ2NhbGxiYWNrMicpKSB7XG4gICAgICAgICAgICAgICAgICAgICQuYWpheCgkLmV4dGVuZCh7XG4gICAgICAgICAgICAgICAgICAgICAgICB1cmw6ICRmb3JtLmRhdGEoJ2NhbGxiYWNrMicpLFxuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogJ0dFVCcsXG4gICAgICAgICAgICAgICAgICAgICAgICBzdWNjZXNzOiBmdW5jdGlvbiAoZGF0YSkge1xuICAgICAgICAgICAgICAgICAgICAgICAgICAgICQoXCIjXCIgKyAkZm9ybS5kYXRhKCdsaXN0MicpKS5odG1sKGRhdGEpO1xuICAgICAgICAgICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgICAgICAgICB9LCBkZWZhdWx0QWpheE9wdGlvbnMoKSkpO1xuICAgICAgICAgICAgICAgIH1cbiAgICAgICAgICAgICAgICAvLyBPdSByZWRpcmVjdGlvbiBkZSBsYSBwYWdlIGVudGnDqHJlXG4gICAgICAgICAgICB9IGVsc2UgaWYgKGRhdGEuc3Vic3RyKDAsIDgpID09ICdyZWRpcmVjdCcpIHtcbiAgICAgICAgICAgICAgICB3aW5kb3cubG9jYXRpb24uYXNzaWduKGRhdGEuc3Vic3RyKDkpKTtcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgJCgnI2Zvcm1Nb2RhbEJvZHknKS5odG1sKGRhdGEpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfSwgZGVmYXVsdEFqYXhPcHRpb25zKCkpKTtcblxuICAgIGV2ZW50LnByZXZlbnREZWZhdWx0KCk7XG59KTtcblxuLy8gUHJvcHJpw6l0w6lzIHBhciBkw6lmYXV0IHBvdXIgbGVzIHJlcXXDqnRlcyBBSkFYXG5mdW5jdGlvbiBkZWZhdWx0QWpheE9wdGlvbnMoKSB7XG4gICAgcmV0dXJuIHtcbiAgICAgICAgY2FjaGU6IGZhbHNlLFxuICAgICAgICBiZWZvcmVTZW5kOiBmdW5jdGlvbiAoKSB7XG4gICAgICAgICAgICAkKCcjbG9hZGVyJykuc2hvdygpO1xuICAgICAgICB9LFxuICAgICAgICBjb21wbGV0ZTogZnVuY3Rpb24gKCkge1xuICAgICAgICAgICAgJCgnI2xvYWRlcicpLmhpZGUoKTtcbiAgICAgICAgfSxcbiAgICAgICAgZXJyb3I6IGZ1bmN0aW9uICh4aHIsIGFqYXhPcHRpb25zLCB0aHJvd25FcnJvcikge1xuICAgICAgICAgICAgYWxlcnQoeGhyLnN0YXR1cyk7XG4gICAgICAgICAgICBpZiAoeGhyLnN0YXR1cyA9PT0gNDAzKSB7XG4gICAgICAgICAgICAgICAgLy93aW5kb3cubG9jYXRpb24uaHJlZiA9ICcvJztcbiAgICAgICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICAgICAgYWxlcnQoJ1VuZSBlcnJldXIgZXN0IHN1cnZlbnVlLicpO1xuICAgICAgICAgICAgfVxuICAgICAgICB9XG4gICAgfTtcbn1cbmdsb2JhbC5kZWZhdWx0QWpheE9wdGlvbnMgPSBkZWZhdWx0QWpheE9wdGlvbnM7XG5cbmZ1bmN0aW9uIGdldFJhZGlvQnV0dG9uVmFsdWUobmFtZSkge1xuICAgIHZhciB2YWx1ZSA9ICcnO1xuICAgIHZhciBzZWxlY3RlZCA9ICQoXCJpbnB1dFt0eXBlPSdyYWRpbyddW25hbWU9J1wiICsgbmFtZSArIFwiJ106Y2hlY2tlZFwiKTtcbiAgICBpZiAoc2VsZWN0ZWQubGVuZ3RoID4gMCkge1xuICAgICAgICB2YWx1ZSA9IHNlbGVjdGVkLnZhbCgpO1xuICAgIH1cblxuICAgIHJldHVybiB2YWx1ZTtcbn1cbndpbmRvdy5nZXRSYWRpb0J1dHRvblZhbHVlID0gZ2V0UmFkaW9CdXR0b25WYWx1ZTtcbiJdLCJzb3VyY2VSb290IjoiIn0=