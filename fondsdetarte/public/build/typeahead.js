(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["typeahead"],{

/***/ "./assets/css/typeahead.scss":
/*!***********************************!*\
  !*** ./assets/css/typeahead.scss ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

// extracted by mini-css-extract-plugin

/***/ }),

/***/ "./assets/js/typeahead.js":
/*!********************************!*\
  !*** ./assets/js/typeahead.js ***!
  \********************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(global) {/* harmony import */ var _typeahead_ad__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./typeahead_ad */ "./assets/js/typeahead_ad.js");
/* harmony import */ var _typeahead_commune__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./typeahead_commune */ "./assets/js/typeahead_commune.js");
/* harmony import */ var _typeahead_adresse__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./typeahead_adresse */ "./assets/js/typeahead_adresse.js");
__webpack_require__(/*! ../css/typeahead.scss */ "./assets/css/typeahead.scss"); // Ne charger que les modules nécessaires. Commenter les autres.



global.typeahead_ad = _typeahead_ad__WEBPACK_IMPORTED_MODULE_0__["default"];

global.typeahead_commune = _typeahead_commune__WEBPACK_IMPORTED_MODULE_1__["default"];

global.typeahead_adresse = _typeahead_adresse__WEBPACK_IMPORTED_MODULE_2__["default"];
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../../node_modules/webpack/buildin/global.js */ "./node_modules/webpack/buildin/global.js")))

/***/ }),

/***/ "./assets/js/typeahead_ad.js":
/*!***********************************!*\
  !*** ./assets/js/typeahead_ad.js ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _vendor_friendsofsymfony_jsrouting_bundle_Resources_public_js_router_min_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js */ "./vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js");
/* harmony import */ var _vendor_friendsofsymfony_jsrouting_bundle_Resources_public_js_router_min_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_vendor_friendsofsymfony_jsrouting_bundle_Resources_public_js_router_min_js__WEBPACK_IMPORTED_MODULE_0__);
var Bloodhound = __webpack_require__(/*! typeahead.js/dist/bloodhound */ "./node_modules/typeahead.js/dist/bloodhound.js");

var typeahead = __webpack_require__(/*! typeahead.js/dist/typeahead.jquery */ "./node_modules/typeahead.js/dist/typeahead.jquery.js");

var routes = __webpack_require__(/*! ../../public/js/fos_js_routes.json */ "./public/js/fos_js_routes.json");


_vendor_friendsofsymfony_jsrouting_bundle_Resources_public_js_router_min_js__WEBPACK_IMPORTED_MODULE_0___default.a.setRoutingData(routes);
/* harmony default export */ __webpack_exports__["default"] = (function (input) {
  var users = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
      url: _vendor_friendsofsymfony_jsrouting_bundle_Resources_public_js_router_min_js__WEBPACK_IMPORTED_MODULE_0___default.a.generate('ldap_search'),
      replace: function replace(url, q) {
        return url + '/' + q;
      }
    }
  });
  input.typeahead({
    hint: false,
    highlight: true,
    minLength: 3
  }, {
    name: 'utilisateur',
    display: 'value',
    limit: 100,
    source: users,
    templates: {
      suggestion: function suggestion(data) {
        return '<div><strong>&bull; ' + data.value + ' (' + data.login + ')</strong><br /><em>' + data.fonction + '</em></div>';
      },
      notFound: function notFound() {
        return '&nbsp;<i class="fas fa-exclamation-triangle text-danger"></i> <em>Aucun résultat</em>&nbsp;';
      },
      pending: function pending() {
        return '&nbsp;<i class="fas fa-spin fa-sync"></i> <em>Recherche en cours ...</em>&nbsp;';
      }
    }
  });
});
;

/***/ }),

/***/ "./assets/js/typeahead_adresse.js":
/*!****************************************!*\
  !*** ./assets/js/typeahead_adresse.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _vendor_friendsofsymfony_jsrouting_bundle_Resources_public_js_router_min_js__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js */ "./vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js");
/* harmony import */ var _vendor_friendsofsymfony_jsrouting_bundle_Resources_public_js_router_min_js__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(_vendor_friendsofsymfony_jsrouting_bundle_Resources_public_js_router_min_js__WEBPACK_IMPORTED_MODULE_0__);
/**
 * Recherche d'adresse postale via l'API IGN
 * https://geo.api.gouv.fr/adresse
 */
var Bloodhound = __webpack_require__(/*! typeahead.js/dist/bloodhound */ "./node_modules/typeahead.js/dist/bloodhound.js");

var typeahead = __webpack_require__(/*! typeahead.js/dist/typeahead.jquery */ "./node_modules/typeahead.js/dist/typeahead.jquery.js");

var routes = __webpack_require__(/*! ../../public/js/fos_js_routes.json */ "./public/js/fos_js_routes.json");


_vendor_friendsofsymfony_jsrouting_bundle_Resources_public_js_router_min_js__WEBPACK_IMPORTED_MODULE_0___default.a.setRoutingData(routes);
/* harmony default export */ __webpack_exports__["default"] = (function (input, input_commune_code) {
  var adresses = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
      url: _vendor_friendsofsymfony_jsrouting_bundle_Resources_public_js_router_min_js__WEBPACK_IMPORTED_MODULE_0___default.a.generate('adresse_postale'),
      replace: function replace(url, q) {
        return url + '/' + q + '/' + input_commune_code.val();
      }
    }
  });
  input.typeahead({
    hint: false,
    highlight: true,
    minLength: 3
  }, {
    name: 'adresse',
    display: 'value',
    limit: 30,
    source: adresses,
    templates: {
      suggestion: function suggestion(data) {
        return '<div>&bull; ' + data.value + ' - ' + data.city + ' (' + data.postcode + ')</div><hr />';
      },
      notFound: function notFound() {
        return '&nbsp;<i class="fas fa-exclamation-triangle text-danger"></i> <em>Aucun résultat</em>&nbsp;';
      },
      pending: function pending() {
        return '&nbsp;<i class="fas fa-spin fa-sync"></i> <em>Recherche en cours ...</em>&nbsp;';
      }
    }
  });
});
;

/***/ }),

/***/ "./assets/js/typeahead_commune.js":
/*!****************************************!*\
  !*** ./assets/js/typeahead_commune.js ***!
  \****************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/**
 * Recherche de commune via l'API geo
 * https://geo.api.gouv.fr/decoupage-administratif/communes
 */
var Bloodhound = __webpack_require__(/*! typeahead.js/dist/bloodhound */ "./node_modules/typeahead.js/dist/bloodhound.js");

var typeahead = __webpack_require__(/*! typeahead.js/dist/typeahead.jquery */ "./node_modules/typeahead.js/dist/typeahead.jquery.js");

/* harmony default export */ __webpack_exports__["default"] = (function (input) {
  var communes = new Bloodhound({
    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    remote: {
      url: 'https://geo.api.gouv.fr/communes?nom=%QUERY',
      wildcard: '%QUERY'
    }
  });
  input.typeahead({
    hint: false,
    highlight: true,
    minLength: 3
  }, {
    name: 'commune',
    display: 'nom',
    limit: 30,
    source: communes,
    templates: {
      suggestion: function suggestion(data) {
        return '<div>&bull; ' + data.nom + ' (' + data.codeDepartement + ')</div>';
      },
      notFound: function notFound() {
        return '&nbsp;<i class="fas fa-exclamation-triangle text-danger"></i> <em>Aucun résultat</em>&nbsp;';
      },
      pending: function pending() {
        return '&nbsp;<i class="fas fa-spin fa-sync"></i> <em>Recherche en cours ...</em>&nbsp;';
      }
    }
  });
});
;

/***/ })

},[["./assets/js/typeahead.js","runtime","vendors~app~form~fullcalendar~typeahead","vendors~app~fullcalendar~typeahead","vendors~fullcalendar~typeahead","vendors~app~typeahead","vendors~typeahead","fullcalendar~typeahead"]]]);
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vLi9hc3NldHMvY3NzL3R5cGVhaGVhZC5zY3NzIiwid2VicGFjazovLy8uL2Fzc2V0cy9qcy90eXBlYWhlYWQuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL3R5cGVhaGVhZF9hZC5qcyIsIndlYnBhY2s6Ly8vLi9hc3NldHMvanMvdHlwZWFoZWFkX2FkcmVzc2UuanMiLCJ3ZWJwYWNrOi8vLy4vYXNzZXRzL2pzL3R5cGVhaGVhZF9jb21tdW5lLmpzIl0sIm5hbWVzIjpbInJlcXVpcmUiLCJnbG9iYWwiLCJ0eXBlYWhlYWRfYWQiLCJ0eXBlYWhlYWRfY29tbXVuZSIsInR5cGVhaGVhZF9hZHJlc3NlIiwiQmxvb2Rob3VuZCIsInR5cGVhaGVhZCIsInJvdXRlcyIsIlJvdXRpbmciLCJzZXRSb3V0aW5nRGF0YSIsImlucHV0IiwidXNlcnMiLCJkYXR1bVRva2VuaXplciIsInRva2VuaXplcnMiLCJvYmoiLCJ3aGl0ZXNwYWNlIiwicXVlcnlUb2tlbml6ZXIiLCJyZW1vdGUiLCJ1cmwiLCJnZW5lcmF0ZSIsInJlcGxhY2UiLCJxIiwiaGludCIsImhpZ2hsaWdodCIsIm1pbkxlbmd0aCIsIm5hbWUiLCJkaXNwbGF5IiwibGltaXQiLCJzb3VyY2UiLCJ0ZW1wbGF0ZXMiLCJzdWdnZXN0aW9uIiwiZGF0YSIsInZhbHVlIiwibG9naW4iLCJmb25jdGlvbiIsIm5vdEZvdW5kIiwicGVuZGluZyIsImlucHV0X2NvbW11bmVfY29kZSIsImFkcmVzc2VzIiwidmFsIiwiY2l0eSIsInBvc3Rjb2RlIiwiY29tbXVuZXMiLCJ3aWxkY2FyZCIsIm5vbSIsImNvZGVEZXBhcnRlbWVudCJdLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBQUEsdUM7Ozs7Ozs7Ozs7OztBQ0FBQTtBQUFBQTtBQUFBQTtBQUFBQTtBQUFBQSxtQkFBTyxDQUFDLDBEQUFELENBQVAsQyxDQUVBOzs7QUFFQTtBQUNBQyxNQUFNLENBQUNDLFlBQVAsR0FBc0JBLHFEQUF0QjtBQUVBO0FBQ0FELE1BQU0sQ0FBQ0UsaUJBQVAsR0FBMkJBLDBEQUEzQjtBQUVBO0FBQ0FGLE1BQU0sQ0FBQ0csaUJBQVAsR0FBMkJBLDBEQUEzQixDOzs7Ozs7Ozs7Ozs7O0FDWEE7QUFBQTtBQUFBO0FBQUEsSUFBSUMsVUFBVSxHQUFHTCxtQkFBTyxDQUFDLG9GQUFELENBQXhCOztBQUNBLElBQUlNLFNBQVMsR0FBR04sbUJBQU8sQ0FBQyxnR0FBRCxDQUF2Qjs7QUFDQSxJQUFNTyxNQUFNLEdBQUdQLG1CQUFPLENBQUMsMEVBQUQsQ0FBdEI7O0FBQ0E7QUFDQVEsa0hBQU8sQ0FBQ0MsY0FBUixDQUF1QkYsTUFBdkI7QUFFZSx5RUFBU0csS0FBVCxFQUFnQjtBQUMzQixNQUFJQyxLQUFLLEdBQUcsSUFBSU4sVUFBSixDQUFlO0FBQ3ZCTyxrQkFBYyxFQUFFUCxVQUFVLENBQUNRLFVBQVgsQ0FBc0JDLEdBQXRCLENBQTBCQyxVQUExQixDQUFxQyxPQUFyQyxDQURPO0FBRXZCQyxrQkFBYyxFQUFFWCxVQUFVLENBQUNRLFVBQVgsQ0FBc0JFLFVBRmY7QUFHdkJFLFVBQU0sRUFBRTtBQUNKQyxTQUFHLEVBQUVWLGtIQUFPLENBQUNXLFFBQVIsQ0FBaUIsYUFBakIsQ0FERDtBQUVKQyxhQUFPLEVBQUUsaUJBQVNGLEdBQVQsRUFBY0csQ0FBZCxFQUFnQjtBQUNyQixlQUFPSCxHQUFHLEdBQUksR0FBUCxHQUFhRyxDQUFwQjtBQUNIO0FBSkc7QUFIZSxHQUFmLENBQVo7QUFXQVgsT0FBSyxDQUFDSixTQUFOLENBQWdCO0FBQ1pnQixRQUFJLEVBQUUsS0FETTtBQUVaQyxhQUFTLEVBQUUsSUFGQztBQUdaQyxhQUFTLEVBQUU7QUFIQyxHQUFoQixFQUlHO0FBQ0NDLFFBQUksRUFBRSxhQURQO0FBRUNDLFdBQU8sRUFBRSxPQUZWO0FBR0NDLFNBQUssRUFBRSxHQUhSO0FBSUNDLFVBQU0sRUFBRWpCLEtBSlQ7QUFLQ2tCLGFBQVMsRUFBRTtBQUNQQyxnQkFBVSxFQUFFLG9CQUFVQyxJQUFWLEVBQWdCO0FBQ3hCLGVBQU8seUJBQXlCQSxJQUFJLENBQUNDLEtBQTlCLEdBQXNDLElBQXRDLEdBQTZDRCxJQUFJLENBQUNFLEtBQWxELEdBQTBELHNCQUExRCxHQUFtRkYsSUFBSSxDQUFDRyxRQUF4RixHQUFtRyxhQUExRztBQUNILE9BSE07QUFJUEMsY0FBUSxFQUFFLG9CQUFZO0FBQ2xCLGVBQU8sNkZBQVA7QUFDSCxPQU5NO0FBT1BDLGFBQU8sRUFBRyxtQkFBWTtBQUNsQixlQUFPLGlGQUFQO0FBQ0g7QUFUTTtBQUxaLEdBSkg7QUFxQkg7QUFBQSxDOzs7Ozs7Ozs7Ozs7QUN2Q0Q7QUFBQTtBQUFBO0FBQUE7Ozs7QUFLQSxJQUFJL0IsVUFBVSxHQUFHTCxtQkFBTyxDQUFDLG9GQUFELENBQXhCOztBQUNBLElBQUlNLFNBQVMsR0FBR04sbUJBQU8sQ0FBQyxnR0FBRCxDQUF2Qjs7QUFDQSxJQUFNTyxNQUFNLEdBQUdQLG1CQUFPLENBQUMsMEVBQUQsQ0FBdEI7O0FBQ0E7QUFDQVEsa0hBQU8sQ0FBQ0MsY0FBUixDQUF1QkYsTUFBdkI7QUFFZSx5RUFBU0csS0FBVCxFQUFnQjJCLGtCQUFoQixFQUFvQztBQUMvQyxNQUFJQyxRQUFRLEdBQUcsSUFBSWpDLFVBQUosQ0FBZTtBQUMxQk8sa0JBQWMsRUFBRVAsVUFBVSxDQUFDUSxVQUFYLENBQXNCQyxHQUF0QixDQUEwQkMsVUFBMUIsQ0FBcUMsT0FBckMsQ0FEVTtBQUUxQkMsa0JBQWMsRUFBRVgsVUFBVSxDQUFDUSxVQUFYLENBQXNCRSxVQUZaO0FBRzFCRSxVQUFNLEVBQUU7QUFDSkMsU0FBRyxFQUFFVixrSEFBTyxDQUFDVyxRQUFSLENBQWlCLGlCQUFqQixDQUREO0FBRUpDLGFBQU8sRUFBRSxpQkFBU0YsR0FBVCxFQUFjRyxDQUFkLEVBQWdCO0FBQ3JCLGVBQU9ILEdBQUcsR0FBSSxHQUFQLEdBQWFHLENBQWIsR0FBaUIsR0FBakIsR0FBdUJnQixrQkFBa0IsQ0FBQ0UsR0FBbkIsRUFBOUI7QUFDSDtBQUpHO0FBSGtCLEdBQWYsQ0FBZjtBQVdBN0IsT0FBSyxDQUFDSixTQUFOLENBQWdCO0FBQ1pnQixRQUFJLEVBQUUsS0FETTtBQUVaQyxhQUFTLEVBQUUsSUFGQztBQUdaQyxhQUFTLEVBQUU7QUFIQyxHQUFoQixFQUlHO0FBQ0NDLFFBQUksRUFBRSxTQURQO0FBRUNDLFdBQU8sRUFBRSxPQUZWO0FBR0NDLFNBQUssRUFBRSxFQUhSO0FBSUNDLFVBQU0sRUFBRVUsUUFKVDtBQUtDVCxhQUFTLEVBQUU7QUFDUEMsZ0JBQVUsRUFBRSxvQkFBVUMsSUFBVixFQUFnQjtBQUN4QixlQUFPLGlCQUFpQkEsSUFBSSxDQUFDQyxLQUF0QixHQUE4QixLQUE5QixHQUFzQ0QsSUFBSSxDQUFDUyxJQUEzQyxHQUFrRCxJQUFsRCxHQUF5RFQsSUFBSSxDQUFDVSxRQUE5RCxHQUF5RSxlQUFoRjtBQUNILE9BSE07QUFJUE4sY0FBUSxFQUFFLG9CQUFZO0FBQ2xCLGVBQU8sNkZBQVA7QUFDSCxPQU5NO0FBT1BDLGFBQU8sRUFBRSxtQkFBWTtBQUNqQixlQUFPLGlGQUFQO0FBQ0g7QUFUTTtBQUxaLEdBSkg7QUFxQkg7QUFBQSxDOzs7Ozs7Ozs7Ozs7QUM1Q0Q7QUFBQTs7OztBQUtBLElBQUkvQixVQUFVLEdBQUdMLG1CQUFPLENBQUMsb0ZBQUQsQ0FBeEI7O0FBQ0EsSUFBSU0sU0FBUyxHQUFHTixtQkFBTyxDQUFDLGdHQUFELENBQXZCOztBQUVlLHlFQUFTVSxLQUFULEVBQWdCO0FBQzNCLE1BQUlnQyxRQUFRLEdBQUcsSUFBSXJDLFVBQUosQ0FBZTtBQUMxQk8sa0JBQWMsRUFBRVAsVUFBVSxDQUFDUSxVQUFYLENBQXNCQyxHQUF0QixDQUEwQkMsVUFBMUIsQ0FBcUMsT0FBckMsQ0FEVTtBQUUxQkMsa0JBQWMsRUFBRVgsVUFBVSxDQUFDUSxVQUFYLENBQXNCRSxVQUZaO0FBRzFCRSxVQUFNLEVBQUU7QUFDSkMsU0FBRyxFQUFFLDZDQUREO0FBRUp5QixjQUFRLEVBQUU7QUFGTjtBQUhrQixHQUFmLENBQWY7QUFTQWpDLE9BQUssQ0FBQ0osU0FBTixDQUFnQjtBQUNaZ0IsUUFBSSxFQUFFLEtBRE07QUFFWkMsYUFBUyxFQUFFLElBRkM7QUFHWkMsYUFBUyxFQUFFO0FBSEMsR0FBaEIsRUFJRztBQUNDQyxRQUFJLEVBQUUsU0FEUDtBQUVDQyxXQUFPLEVBQUUsS0FGVjtBQUdDQyxTQUFLLEVBQUUsRUFIUjtBQUlDQyxVQUFNLEVBQUVjLFFBSlQ7QUFLQ2IsYUFBUyxFQUFFO0FBQ1BDLGdCQUFVLEVBQUUsb0JBQVVDLElBQVYsRUFBZ0I7QUFDeEIsZUFBTyxpQkFBaUJBLElBQUksQ0FBQ2EsR0FBdEIsR0FBNEIsSUFBNUIsR0FBbUNiLElBQUksQ0FBQ2MsZUFBeEMsR0FBMEQsU0FBakU7QUFDSCxPQUhNO0FBSVBWLGNBQVEsRUFBRSxvQkFBWTtBQUNsQixlQUFPLDZGQUFQO0FBQ0gsT0FOTTtBQU9QQyxhQUFPLEVBQUUsbUJBQVk7QUFDakIsZUFBTyxpRkFBUDtBQUNIO0FBVE07QUFMWixHQUpIO0FBcUJIO0FBQUEsQyIsImZpbGUiOiJ0eXBlYWhlYWQuanMiLCJzb3VyY2VzQ29udGVudCI6WyIvLyBleHRyYWN0ZWQgYnkgbWluaS1jc3MtZXh0cmFjdC1wbHVnaW4iLCJyZXF1aXJlKCcuLi9jc3MvdHlwZWFoZWFkLnNjc3MnKTtcclxuXHJcbi8vIE5lIGNoYXJnZXIgcXVlIGxlcyBtb2R1bGVzIG7DqWNlc3NhaXJlcy4gQ29tbWVudGVyIGxlcyBhdXRyZXMuXHJcblxyXG5pbXBvcnQgdHlwZWFoZWFkX2FkIGZyb20gJy4vdHlwZWFoZWFkX2FkJztcclxuZ2xvYmFsLnR5cGVhaGVhZF9hZCA9IHR5cGVhaGVhZF9hZDtcclxuXHJcbmltcG9ydCB0eXBlYWhlYWRfY29tbXVuZSBmcm9tICcuL3R5cGVhaGVhZF9jb21tdW5lJztcclxuZ2xvYmFsLnR5cGVhaGVhZF9jb21tdW5lID0gdHlwZWFoZWFkX2NvbW11bmU7XHJcblxyXG5pbXBvcnQgdHlwZWFoZWFkX2FkcmVzc2UgZnJvbSAnLi90eXBlYWhlYWRfYWRyZXNzZSc7XHJcbmdsb2JhbC50eXBlYWhlYWRfYWRyZXNzZSA9IHR5cGVhaGVhZF9hZHJlc3NlO1xyXG4iLCJ2YXIgQmxvb2Rob3VuZCA9IHJlcXVpcmUoJ3R5cGVhaGVhZC5qcy9kaXN0L2Jsb29kaG91bmQnKTtcclxudmFyIHR5cGVhaGVhZCA9IHJlcXVpcmUoJ3R5cGVhaGVhZC5qcy9kaXN0L3R5cGVhaGVhZC5qcXVlcnknKTtcclxuY29uc3Qgcm91dGVzID0gcmVxdWlyZSgnLi4vLi4vcHVibGljL2pzL2Zvc19qc19yb3V0ZXMuanNvbicpO1xyXG5pbXBvcnQgUm91dGluZyBmcm9tICcuLi8uLi92ZW5kb3IvZnJpZW5kc29mc3ltZm9ueS9qc3JvdXRpbmctYnVuZGxlL1Jlc291cmNlcy9wdWJsaWMvanMvcm91dGVyLm1pbi5qcyc7XHJcblJvdXRpbmcuc2V0Um91dGluZ0RhdGEocm91dGVzKTtcclxuXHJcbmV4cG9ydCBkZWZhdWx0IGZ1bmN0aW9uKGlucHV0KSB7XHJcbiAgICB2YXIgdXNlcnMgPSBuZXcgQmxvb2Rob3VuZCh7XHJcbiAgICAgICAgZGF0dW1Ub2tlbml6ZXI6IEJsb29kaG91bmQudG9rZW5pemVycy5vYmoud2hpdGVzcGFjZSgndmFsdWUnKSxcclxuICAgICAgICBxdWVyeVRva2VuaXplcjogQmxvb2Rob3VuZC50b2tlbml6ZXJzLndoaXRlc3BhY2UsXHJcbiAgICAgICAgcmVtb3RlOiB7XHJcbiAgICAgICAgICAgIHVybDogUm91dGluZy5nZW5lcmF0ZSgnbGRhcF9zZWFyY2gnKSxcclxuICAgICAgICAgICAgcmVwbGFjZTogZnVuY3Rpb24odXJsLCBxKXtcclxuICAgICAgICAgICAgICAgIHJldHVybiB1cmwgICsgJy8nICsgcTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH0pO1xyXG5cclxuICAgIGlucHV0LnR5cGVhaGVhZCh7XHJcbiAgICAgICAgaGludDogZmFsc2UsXHJcbiAgICAgICAgaGlnaGxpZ2h0OiB0cnVlLFxyXG4gICAgICAgIG1pbkxlbmd0aDogM1xyXG4gICAgfSwge1xyXG4gICAgICAgIG5hbWU6ICd1dGlsaXNhdGV1cicsXHJcbiAgICAgICAgZGlzcGxheTogJ3ZhbHVlJyxcclxuICAgICAgICBsaW1pdDogMTAwLFxyXG4gICAgICAgIHNvdXJjZTogdXNlcnMsXHJcbiAgICAgICAgdGVtcGxhdGVzOiB7XHJcbiAgICAgICAgICAgIHN1Z2dlc3Rpb246IGZ1bmN0aW9uIChkYXRhKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gJzxkaXY+PHN0cm9uZz4mYnVsbDsgJyArIGRhdGEudmFsdWUgKyAnICgnICsgZGF0YS5sb2dpbiArICcpPC9zdHJvbmc+PGJyIC8+PGVtPicgKyBkYXRhLmZvbmN0aW9uICsgJzwvZW0+PC9kaXY+JztcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgbm90Rm91bmQ6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiAnJm5ic3A7PGkgY2xhc3M9XCJmYXMgZmEtZXhjbGFtYXRpb24tdHJpYW5nbGUgdGV4dC1kYW5nZXJcIj48L2k+IDxlbT5BdWN1biByw6lzdWx0YXQ8L2VtPiZuYnNwOyc7XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHBlbmRpbmcgOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gJyZuYnNwOzxpIGNsYXNzPVwiZmFzIGZhLXNwaW4gZmEtc3luY1wiPjwvaT4gPGVtPlJlY2hlcmNoZSBlbiBjb3VycyAuLi48L2VtPiZuYnNwOyc7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9KTtcclxufTsiLCIvKipcclxuICogUmVjaGVyY2hlIGQnYWRyZXNzZSBwb3N0YWxlIHZpYSBsJ0FQSSBJR05cclxuICogaHR0cHM6Ly9nZW8uYXBpLmdvdXYuZnIvYWRyZXNzZVxyXG4gKi9cclxuXHJcbnZhciBCbG9vZGhvdW5kID0gcmVxdWlyZSgndHlwZWFoZWFkLmpzL2Rpc3QvYmxvb2Rob3VuZCcpO1xyXG52YXIgdHlwZWFoZWFkID0gcmVxdWlyZSgndHlwZWFoZWFkLmpzL2Rpc3QvdHlwZWFoZWFkLmpxdWVyeScpO1xyXG5jb25zdCByb3V0ZXMgPSByZXF1aXJlKCcuLi8uLi9wdWJsaWMvanMvZm9zX2pzX3JvdXRlcy5qc29uJyk7XHJcbmltcG9ydCBSb3V0aW5nIGZyb20gJy4uLy4uL3ZlbmRvci9mcmllbmRzb2ZzeW1mb255L2pzcm91dGluZy1idW5kbGUvUmVzb3VyY2VzL3B1YmxpYy9qcy9yb3V0ZXIubWluLmpzJztcclxuUm91dGluZy5zZXRSb3V0aW5nRGF0YShyb3V0ZXMpO1xyXG5cclxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24oaW5wdXQsIGlucHV0X2NvbW11bmVfY29kZSkge1xyXG4gICAgdmFyIGFkcmVzc2VzID0gbmV3IEJsb29kaG91bmQoe1xyXG4gICAgICAgIGRhdHVtVG9rZW5pemVyOiBCbG9vZGhvdW5kLnRva2VuaXplcnMub2JqLndoaXRlc3BhY2UoJ3ZhbHVlJyksXHJcbiAgICAgICAgcXVlcnlUb2tlbml6ZXI6IEJsb29kaG91bmQudG9rZW5pemVycy53aGl0ZXNwYWNlLFxyXG4gICAgICAgIHJlbW90ZToge1xyXG4gICAgICAgICAgICB1cmw6IFJvdXRpbmcuZ2VuZXJhdGUoJ2FkcmVzc2VfcG9zdGFsZScpLFxyXG4gICAgICAgICAgICByZXBsYWNlOiBmdW5jdGlvbih1cmwsIHEpe1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIHVybCAgKyAnLycgKyBxICsgJy8nICsgaW5wdXRfY29tbXVuZV9jb2RlLnZhbCgpO1xyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfVxyXG4gICAgfSk7XHJcblxyXG4gICAgaW5wdXQudHlwZWFoZWFkKHtcclxuICAgICAgICBoaW50OiBmYWxzZSxcclxuICAgICAgICBoaWdobGlnaHQ6IHRydWUsXHJcbiAgICAgICAgbWluTGVuZ3RoOiAzXHJcbiAgICB9LCB7XHJcbiAgICAgICAgbmFtZTogJ2FkcmVzc2UnLFxyXG4gICAgICAgIGRpc3BsYXk6ICd2YWx1ZScsXHJcbiAgICAgICAgbGltaXQ6IDMwLFxyXG4gICAgICAgIHNvdXJjZTogYWRyZXNzZXMsXHJcbiAgICAgICAgdGVtcGxhdGVzOiB7XHJcbiAgICAgICAgICAgIHN1Z2dlc3Rpb246IGZ1bmN0aW9uIChkYXRhKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gJzxkaXY+JmJ1bGw7ICcgKyBkYXRhLnZhbHVlICsgJyAtICcgKyBkYXRhLmNpdHkgKyAnICgnICsgZGF0YS5wb3N0Y29kZSArICcpPC9kaXY+PGhyIC8+JztcclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgbm90Rm91bmQ6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiAnJm5ic3A7PGkgY2xhc3M9XCJmYXMgZmEtZXhjbGFtYXRpb24tdHJpYW5nbGUgdGV4dC1kYW5nZXJcIj48L2k+IDxlbT5BdWN1biByw6lzdWx0YXQ8L2VtPiZuYnNwOyc7XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHBlbmRpbmc6IGZ1bmN0aW9uICgpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiAnJm5ic3A7PGkgY2xhc3M9XCJmYXMgZmEtc3BpbiBmYS1zeW5jXCI+PC9pPiA8ZW0+UmVjaGVyY2hlIGVuIGNvdXJzIC4uLjwvZW0+Jm5ic3A7JztcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgIH1cclxuICAgIH0pO1xyXG59OyIsIi8qKlxyXG4gKiBSZWNoZXJjaGUgZGUgY29tbXVuZSB2aWEgbCdBUEkgZ2VvXHJcbiAqIGh0dHBzOi8vZ2VvLmFwaS5nb3V2LmZyL2RlY291cGFnZS1hZG1pbmlzdHJhdGlmL2NvbW11bmVzXHJcbiAqL1xyXG5cclxudmFyIEJsb29kaG91bmQgPSByZXF1aXJlKCd0eXBlYWhlYWQuanMvZGlzdC9ibG9vZGhvdW5kJyk7XHJcbnZhciB0eXBlYWhlYWQgPSByZXF1aXJlKCd0eXBlYWhlYWQuanMvZGlzdC90eXBlYWhlYWQuanF1ZXJ5Jyk7XHJcblxyXG5leHBvcnQgZGVmYXVsdCBmdW5jdGlvbihpbnB1dCkge1xyXG4gICAgdmFyIGNvbW11bmVzID0gbmV3IEJsb29kaG91bmQoe1xyXG4gICAgICAgIGRhdHVtVG9rZW5pemVyOiBCbG9vZGhvdW5kLnRva2VuaXplcnMub2JqLndoaXRlc3BhY2UoJ3ZhbHVlJyksXHJcbiAgICAgICAgcXVlcnlUb2tlbml6ZXI6IEJsb29kaG91bmQudG9rZW5pemVycy53aGl0ZXNwYWNlLFxyXG4gICAgICAgIHJlbW90ZToge1xyXG4gICAgICAgICAgICB1cmw6ICdodHRwczovL2dlby5hcGkuZ291di5mci9jb21tdW5lcz9ub209JVFVRVJZJyxcclxuICAgICAgICAgICAgd2lsZGNhcmQ6ICclUVVFUlknXHJcbiAgICAgICAgfVxyXG4gICAgfSk7XHJcblxyXG4gICAgaW5wdXQudHlwZWFoZWFkKHtcclxuICAgICAgICBoaW50OiBmYWxzZSxcclxuICAgICAgICBoaWdobGlnaHQ6IHRydWUsXHJcbiAgICAgICAgbWluTGVuZ3RoOiAzXHJcbiAgICB9LCB7XHJcbiAgICAgICAgbmFtZTogJ2NvbW11bmUnLFxyXG4gICAgICAgIGRpc3BsYXk6ICdub20nLFxyXG4gICAgICAgIGxpbWl0OiAzMCxcclxuICAgICAgICBzb3VyY2U6IGNvbW11bmVzLFxyXG4gICAgICAgIHRlbXBsYXRlczoge1xyXG4gICAgICAgICAgICBzdWdnZXN0aW9uOiBmdW5jdGlvbiAoZGF0YSkge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuICc8ZGl2PiZidWxsOyAnICsgZGF0YS5ub20gKyAnICgnICsgZGF0YS5jb2RlRGVwYXJ0ZW1lbnQgKyAnKTwvZGl2Pic7XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIG5vdEZvdW5kOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gJyZuYnNwOzxpIGNsYXNzPVwiZmFzIGZhLWV4Y2xhbWF0aW9uLXRyaWFuZ2xlIHRleHQtZGFuZ2VyXCI+PC9pPiA8ZW0+QXVjdW4gcsOpc3VsdGF0PC9lbT4mbmJzcDsnO1xyXG4gICAgICAgICAgICB9LFxyXG4gICAgICAgICAgICBwZW5kaW5nOiBmdW5jdGlvbiAoKSB7XHJcbiAgICAgICAgICAgICAgICByZXR1cm4gJyZuYnNwOzxpIGNsYXNzPVwiZmFzIGZhLXNwaW4gZmEtc3luY1wiPjwvaT4gPGVtPlJlY2hlcmNoZSBlbiBjb3VycyAuLi48L2VtPiZuYnNwOyc7XHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9XHJcbiAgICB9KTtcclxufTsiXSwic291cmNlUm9vdCI6IiJ9