# Fonds de tarte

Fonds de tarte Symfony 5.0.*

Voir le fichier docs/installation.txt pour mise en place

# Fonctionnalités disponibles et services configurés

- Design Bootstrap V4.4.1
- SSO kerberos
- Gestion des utilisateurs
- Autocomplétion Typeahead (ex. page accueil)
- Envoi d'emails (ex. nouvel utilisateur)
- Export Excel (ex. liste utilisateurs)
- Chiffrement de données (ex. page accueil)
- Recherche AD (ex. ajout utilisateur)
- Recherche de commune via API (ex. page accueil)
- Recherche d'adresse postale via API (ex. page accueil)
- Fullcalendar (ex. page accueil)

# Chiffrement
Chiffrement et déchiffrement de données grâce à la librairie defuse. Voir exemple dans DefaultController::index

Documentation : https://github.com/defuse/php-encryption/blob/HEAD/docs/Tutorial.md

**Installer defuse :**<br />
`composer require defuse/php-encryption`

**Installer et configurer Secret<br />**
`php bin/console secrets:generate-keys`<br />
`php bin/console secrets:generate-keys --env=prod`<br />

Créer une clef de chiffrement unique et la stocker dans Secret :<br />
`vendor/bin/generate-defuse-key`<br />
`php bin/console secrets:set ENCRYPTION_KEY`

**Ajouter le service dans config\services.yaml :<br />**
`App\Services\Chiffre:`<br />
`        arguments: ["%env(ENCRYPTION_KEY)%"]`
