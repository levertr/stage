/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import '../css/app.scss';

// Need jQuery? Install it with "yarn add jquery", then uncomment to import it.
import $ from 'jquery';
global.$ = global.jQuery = $;

//console.log('Hello Webpack Encore! Edit me in assets/js/app.js');

require('bootstrap');
require('@fortawesome/fontawesome-free/css/all.min.css');
require('@fortawesome/fontawesome-free/js/all.js');

$(document).ready(function() {
    $('[data-toggle="tooltip"]').tooltip();

    // AJAX sur listing d'entités
    $('form[data-filtre]').change(function () {
        var form = $(this);

        $.ajax($.extend({
            url : form.data('action'),
            type: 'POST',
            data : form.serialize(),
            success: function(data) {
                $("#" + form.data('list')).html(data);
            }
        }, defaultAjaxOptions()));
    }).change();

    $('form[data-filtre]').submit(function () {
        return false;
    });
});

// Modal de confirmation de suppression
$(document).on('submit', 'form[data-confirmation]', function (event) {
    var $form = $(this),
        $confirm = $('#confirmationModal');

    if ($confirm.data('result') !== 'yes') {
        //cancel submit event
        event.preventDefault();

        $confirm
            .off('click', '#btnYes')
            .on('click', '#btnYes', function () {
                $confirm.data('result', 'yes');
                $form.find('input[type="submit"]').attr('disabled', 'disabled');
                $form.submit();
            })
            .modal('show');
    }
});

// Modal de confirmation de suppression en AJAX
$(document).on('submit', 'form[data-ajaxconfirmation]', function (event) {
    var $form = $(this),
        $confirm = $('#confirmationModal');

    if ($confirm.data('result') !== 'yes') {
        //cancel submit event
        event.preventDefault();

        $confirm
            .off('click', '#btnYes')
            .on('click', '#btnYes', function () {
                $confirm.data('result', 'yes');
                $form.find('input[type="submit"]').attr('disabled', 'disabled');

                // Suppression via AJAX
                $.ajax($.extend({
                    url: $form.attr('action'),
                    type: $form.attr('method'),
                    data : $form.serialize(),
                    success: function(data) {
                        if (data == 'ok') {
                            $.ajax($.extend({
                                url: $form.data('callback'),
                                type: 'GET',
                                success: function (data) {
                                    $("#" + $form.data('list')).html(data);
                                }
                            }, defaultAjaxOptions()));
                        } else {
                            alert(data);
                        }
                    }
                }, defaultAjaxOptions()));
            })
            .modal('show');
    }
});

// Initialisation des var et chargement du modal pour formulaire AJAX
$("#formModal").on("show.bs.modal", function(e) {
    var title = $(e.relatedTarget).attr("data-title");
    var href = $(e.relatedTarget).attr("data-href");

    if (title && href) {
        $("#formModalBody").load(href, function(response, status, xhr) {
            if (xhr.status === 403) {
                window.location.reload();
            }
        });

        $('#formModalLabel').html("<i class='fas fa-pencil-alt'></i> " + title);
    }
});

// Déclenchement des requêtes AJAX de soumission de formulaires modal (hors fichiers)
$(document).on('submit', 'form[data-async]', function (event) {
    var $form = $(this);

    $.ajax($.extend({
        url: $form.attr('action'),
        data: $form.serialize(),
        method: $form.attr('method'),
        success: function (data) {
            if (data == 'ok') {
                $('#formModalLabel').html('');
                $('#formModalBody').html('');
                $('#formModal').modal('hide');

                // Raffraîchissement AJAX
                if ($form.data('callback')) {
                    $.ajax($.extend({
                        url: $form.data('callback'),
                        type: 'GET',
                        success: function (data) {
                            $("#" + $form.data('list')).html(data);
                        }
                    }, defaultAjaxOptions()));
                }

                if ($form.data('callback2')) {
                    $.ajax($.extend({
                        url: $form.data('callback2'),
                        type: 'GET',
                        success: function (data) {
                            $("#" + $form.data('list2')).html(data);
                        }
                    }, defaultAjaxOptions()));
                }
                // Ou redirection de la page entière
            } else if (data.substr(0, 8) == 'redirect') {
                window.location.assign(data.substr(9));
            } else {
                $('#formModalBody').html(data);
            }
        }
    }, defaultAjaxOptions()));

    event.preventDefault();
});

// Propriétés par défaut pour les requêtes AJAX
function defaultAjaxOptions() {
    return {
        cache: false,
        beforeSend: function () {
            $('#loader').show();
        },
        complete: function () {
            $('#loader').hide();
        },
        error: function (xhr, ajaxOptions, thrownError) {
            alert(xhr.status);
            if (xhr.status === 403) {
                //window.location.href = '/';
            } else {
                alert('Une erreur est survenue.');
            }
        }
    };
}
global.defaultAjaxOptions = defaultAjaxOptions;

function getRadioButtonValue(name) {
    var value = '';
    var selected = $("input[type='radio'][name='" + name + "']:checked");
    if (selected.length > 0) {
        value = selected.val();
    }

    return value;
}
window.getRadioButtonValue = getRadioButtonValue;
