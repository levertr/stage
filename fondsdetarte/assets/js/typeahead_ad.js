var Bloodhound = require('typeahead.js/dist/bloodhound');
var typeahead = require('typeahead.js/dist/typeahead.jquery');
const routes = require('../../public/js/fos_js_routes.json');
import Routing from '../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js';
Routing.setRoutingData(routes);

export default function(input) {
    var users = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: Routing.generate('ldap_search'),
            replace: function(url, q){
                return url  + '/' + q;
            }
        }
    });

    input.typeahead({
        hint: false,
        highlight: true,
        minLength: 3
    }, {
        name: 'utilisateur',
        display: 'value',
        limit: 100,
        source: users,
        templates: {
            suggestion: function (data) {
                return '<div><strong>&bull; ' + data.value + ' (' + data.login + ')</strong><br /><em>' + data.fonction + '</em></div>';
            },
            notFound: function () {
                return '&nbsp;<i class="fas fa-exclamation-triangle text-danger"></i> <em>Aucun résultat</em>&nbsp;';
            },
            pending : function () {
                return '&nbsp;<i class="fas fa-spin fa-sync"></i> <em>Recherche en cours ...</em>&nbsp;';
            }
        }
    });
};