/**
 * Recherche de commune via l'API geo
 * https://geo.api.gouv.fr/decoupage-administratif/communes
 */

var Bloodhound = require('typeahead.js/dist/bloodhound');
var typeahead = require('typeahead.js/dist/typeahead.jquery');

export default function(input) {
    var communes = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: 'https://geo.api.gouv.fr/communes?nom=%QUERY',
            wildcard: '%QUERY'
        }
    });

    input.typeahead({
        hint: false,
        highlight: true,
        minLength: 3
    }, {
        name: 'commune',
        display: 'nom',
        limit: 30,
        source: communes,
        templates: {
            suggestion: function (data) {
                return '<div>&bull; ' + data.nom + ' (' + data.codeDepartement + ')</div>';
            },
            notFound: function () {
                return '&nbsp;<i class="fas fa-exclamation-triangle text-danger"></i> <em>Aucun résultat</em>&nbsp;';
            },
            pending: function () {
                return '&nbsp;<i class="fas fa-spin fa-sync"></i> <em>Recherche en cours ...</em>&nbsp;';
            }
        }
    });
};