require('fullcalendar/dist/fullcalendar.css');
require('fullcalendar');
require('fullcalendar/dist/locale/fr');

const routes = require('../../public/js/fos_js_routes.json');
import Routing from '../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js';
Routing.setRoutingData(routes);

export default function(calendar) {
    calendar.fullCalendar({
        header: {
            left: 'month,listMonth',
            center: 'title',
            right: 'prev,next today'
        },
        locale: 'fr',
        selectable: true,
        minTime: '07:00:00',
        maxTime: '20:00:00',
        events: {
            url: Routing.generate('fullcalendar'),
            cache: true,
            type: 'POST',
            data: {},
            error: function() {
                alert('Une erreur est survenue.');
            }
        },
        eventClick:  function(event) {
            $('#formModalLabel').html("<i class='fas fa-lg fa-glass-cheers'></i> Résumé d'un événement");
            //$("#formModalBody").load(Routing.generate('evenement_show', {'id': event.id}));
            $("#formModalBody").html("Contenu de l'événement " + event.title);
            $('#formModal').modal('show');
        }
    })
}