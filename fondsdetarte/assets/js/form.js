require('bootstrap-select');
datepicker = require('bootstrap-datepicker');
global.datepicker = datepicker;
require('bootstrap-datepicker/dist/locales/bootstrap-datepicker.fr.min');
require('bootstrap-checkbox');

$(document).ready(function() {
    // Bootstrap-select
    $('.selectpicker-hdf').selectpicker({
        liveSearch: true,
        noneSelectedText: '--',
        showTick: true
    });

    $('[data-toggle="datepicker"]').datepicker({
        format: "dd/mm/yyyy",
        language: "fr",
        todayHighlight: true,
        autoclose: true,
        clearBtn: true
    });

    // $('[data-toggle="datetimepicker"]').datetimepicker({
    //     locale: 'fr',
    //     stepping: 15,
    //     icons: {
    //         time: 'fas fa-clock',
    //         date: 'fas fa-calendar',
    //         up: 'fas fa-arrow-up',
    //         down: 'fas fa-arrow-down',
    //         previous: 'fas fa-chevron-left',
    //         next: 'fas fa-chevron-right',
    //         today: 'fas fa-calendar-check',
    //         clear: 'far fa-trash-alt',
    //         close: 'far fa-times-circle'
    //     }
    // });
});
