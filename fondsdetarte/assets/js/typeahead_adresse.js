/**
 * Recherche d'adresse postale via l'API IGN
 * https://geo.api.gouv.fr/adresse
 */

var Bloodhound = require('typeahead.js/dist/bloodhound');
var typeahead = require('typeahead.js/dist/typeahead.jquery');
const routes = require('../../public/js/fos_js_routes.json');
import Routing from '../../vendor/friendsofsymfony/jsrouting-bundle/Resources/public/js/router.min.js';
Routing.setRoutingData(routes);

export default function(input, input_commune_code) {
    var adresses = new Bloodhound({
        datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        remote: {
            url: Routing.generate('adresse_postale'),
            replace: function(url, q){
                return url  + '/' + q + '/' + input_commune_code.val();
            }
        }
    });

    input.typeahead({
        hint: false,
        highlight: true,
        minLength: 3
    }, {
        name: 'adresse',
        display: 'value',
        limit: 30,
        source: adresses,
        templates: {
            suggestion: function (data) {
                return '<div>&bull; ' + data.value + ' - ' + data.city + ' (' + data.postcode + ')</div><hr />';
            },
            notFound: function () {
                return '&nbsp;<i class="fas fa-exclamation-triangle text-danger"></i> <em>Aucun résultat</em>&nbsp;';
            },
            pending: function () {
                return '&nbsp;<i class="fas fa-spin fa-sync"></i> <em>Recherche en cours ...</em>&nbsp;';
            }
        }
    });
};