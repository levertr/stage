require('../css/typeahead.scss');

// Ne charger que les modules nécessaires. Commenter les autres.

import typeahead_ad from './typeahead_ad';
global.typeahead_ad = typeahead_ad;

import typeahead_commune from './typeahead_commune';
global.typeahead_commune = typeahead_commune;

import typeahead_adresse from './typeahead_adresse';
global.typeahead_adresse = typeahead_adresse;
