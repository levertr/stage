<?php

namespace App\Services;

use App\Entity\Utilisateur;
use Doctrine\ORM\EntityManagerInterface;

class Ldap
{
    private $ds;
    private $ldapHost;
    private $ldapPort;
    private $ldapDn;
    private $ldapPass;
    private $em;
    private $dn;
    private $rs;

    /**
     * Constructeur : Init des variables et connexion au serveur AD
     *
     * @param string $ldapHost
     * @param integer $ldapPort
     * @param string $ldapDn
     * @param string $ldapPass
     * @param EntityManagerInterface $em
     */
    public function __construct($ldapHost, $ldapPort, $ldapDn, $ldapPass, EntityManagerInterface $em)
    {
        $this->ldapHost = $ldapHost;
        $this->ldapPort = $ldapPort;
        $this->ldapDn = $ldapDn;
        $this->ldapPass = $ldapPass;
        $this->em = $em;

        $this->dn = "DC=meta,DC=cr-picardie,DC=net";
        $this->rs = array("cn", "sn", "givenName", "SAMAccountName", "mail", "physicalDeliveryOfficeName", "department", "extensionAttribute1",
            "telephoneNumber", "distinguishedName", "title", "extensionAttribute5");

        $this->connect();
    }

    public function __destruct()
    {
        if ($this->ds) {
            ldap_close($this->ds);
        }
    }

    /**
     * Connexion au serveur AD
     */
    private function connect()
    {
        $this->ds = ldap_connect($this->ldapHost, $this->ldapPort);

        if ($this->ds) {
            ldap_set_option($this->ds, LDAP_OPT_PROTOCOL_VERSION, 3);
            ldap_set_option($this->ds, LDAP_OPT_REFERRALS, 0);
            ldap_bind($this->ds, $this->ldapDn, $this->ldapPass);
        }
    }

    /**
     * Retourne les utilisateurs AD par le nom
     *
     * @param string $str
     * @return array|string
     */
    public function findUsersByName($str)
    {
        if ($this->ds && $str) {
            $filtre = "(&(sn=*$str*)(objectCategory=organizationalPerson)(extensionAttribute5=*))";
            $srdir = ldap_search($this->ds, $this->dn, $filtre, $this->rs);
            $info = ldap_get_entries($this->ds, $srdir);

            // si distinguishedName, on est chez nous (AD), sinon on est en NPDC (openldap)
           // print_r($info);

            return $info;
        } else {
            return ldap_error($this->ds);
        }
    }

    /**
     * Retourne un utilisateur AD par son login
     *
     * @param string $login
     * @return array|string
     */
    public function findUserByLogin($login)
    {
        if ($this->ds && $login) {
            $filtre = "(&(extensionAttribute5=$login)(objectCategory=organizationalPerson)(extensionAttribute5=*))";
            $srdir = ldap_search($this->ds, $this->dn, $filtre, $this->rs);
            $info = ldap_get_entries($this->ds, $srdir);

            return $info;
        } else {
            return ldap_error($this->ds);
        }
    }

    /**
     * Vérifie si un user a été retourné
     * @param $user
     * @return bool
     */
    public function checkUser($user)
    {
        return ($user != 'Success' && $user['count'] != 0) ? true : false;
    }

    /**
     * @param $user
     * @return string|null
     */
    public function getMatricule($user)
    {
        if (isset($user[0]['extensionattribute1'][0])) {
            return $user[0]['extensionattribute1'][0];
        } else if (isset($user['extensionattribute1'][0])) {
            return $user['extensionattribute1'][0];
        } else {
            return null;
        }
    }

    /**
     * @param $user
     * @return string|null
     */
    public function getNom($user)
    {
        if (isset($user[0]['sn'][0])) {
            return $user[0]['sn'][0];
        } else if (isset($user['sn'][0])) {
            return $user['sn'][0];
        } else {
            return '';
        }
    }

    /**
     * @param $user
     * @return string|null
     */
    public function getPrenom($user)
    {
        if (isset($user[0]['givenname'][0])) {
            return $user[0]['givenname'][0];
        } else if (isset($user['givenname'][0])) {
            return $user['givenname'][0];
        } else {
            return null;
        }
    }

    /**
     * @param $user
     * @return string|null
     */
    public function getEmail($user)
    {
        if (isset($user[0]['mail'][0])) {
            return $user[0]['mail'][0];
        } else if (isset($user['mail'][0])) {
            return $user['mail'][0];
        } else {
            return null;
        }
    }

    /**
     * @param $user
     * @return string|null
     */
    public function getTelephone($user)
    {
        if (isset($user[0]['telephonenumber'][0])) {
            return $user[0]['telephonenumber'][0];
        } else if (isset($user['telephonenumber'][0])) {
            return $user['telephonenumber'][0];
        } else {
            return null;
        }
    }

    /**
     * @param $user
     * @return string|null
     */
    public function getLogin($user)
    {
        if (isset($user[0]['extensionattribute5'][0])) {
            return strtolower($user[0]['extensionattribute5'][0]);
        } else if (isset($user['extensionattribute5'][0])) {
            return strtolower($user['extensionattribute5'][0]);
        } else if (isset($user[0]['samaccountname'][0])) {
            return strtolower($user[0]['samaccountname'][0]);
        } else if (isset($user['samaccountname'][0])) {
            return strtolower($user['samaccountname'][0]);
        } else {
            return '';
        }
    }

    /**
     * @param $user
     * @return string|null
     */
    public function getFonction($user)
    {
        if (isset($user[0]['title'][0])) {
            return $user[0]['title'][0];
        } else if (isset($user['title'][0])) {
            return $user['title'][0];
        } else {
            return '';
        }
    }

    /**
     * Enregistrement d'un nouvel utilisateur
     * @param string $login
     * @return Utilisateur|bool
     */
    public function persistEntity($login)
    {
        $user = $this->findUserByLogin($login);

        if (false === $this->checkUser($user)) {
            return false;
        }

        $utilisateur = new Utilisateur();
        $utilisateur->setCreatedAt(new \DateTime());
        $utilisateur->setUpdatedAt(new \DateTime());
        $utilisateur->setLogin($login);
        $utilisateur->setCivilite(' ');
        $utilisateur->setNom($this->getNom($user));
        $utilisateur->setPrenom($this->getPrenom($user));
        $utilisateur->setEmail($this->getEmail($user));
        $utilisateur->setTelephone($this->getTelephone($user));
        $utilisateur->setRole('Lecture seule');

        $this->em->persist($utilisateur);
        $this->em->flush();

        return $utilisateur;
    }
}