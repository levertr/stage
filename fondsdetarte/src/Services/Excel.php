<?php

namespace App\Services;

use App\Entity\Utilisateur;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Excel
{
    /**
     * @param Utilisateur[] $utilisateurs
     * @return string
     * @throws \Exception
     */
    public function utilisateurs(array $utilisateurs)
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle("Utilisateurs de l'application");

        // Tailles des colonnes
        $sheet->getColumnDimension('A')->setWidth(40);
        $sheet->getColumnDimension('B')->setWidth(40);
        $sheet->getColumnDimension('C')->setWidth(20);
        $sheet->getColumnDimension('D')->setWidth(50);
        $sheet->getColumnDimension('E')->setWidth(30);
        $sheet->getColumnDimension('F')->setWidth(15);
        $sheet->freezePane('A2');

        // En-têtes
        $sheet
            ->setCellValue('A1', 'Nom')
            ->setCellValue('B1', 'Prénom')
            ->setCellValue('C1', 'Login')
            ->setCellValue('D1', 'Email')
            ->setCellValue('E1', 'Rôle')
            ->setCellValue('F1', 'Actif')
        ;

        // Lignes du tableau
        $m = 2;
        foreach ($utilisateurs as $utilisateur) {
            $sheet
                ->setCellValueByColumnAndRow(1, $m, $utilisateur->getNom())
                ->setCellValueByColumnAndRow(2, $m, $utilisateur->getPrenom())
                ->setCellValueByColumnAndRow(3, $m, $utilisateur->getLogin())
                ->setCellValueByColumnAndRow(4, $m, $utilisateur->getEmail())
                ->setCellValueByColumnAndRow(5, $m, $utilisateur->getRole())
                ->setCellValueByColumnAndRow(6, $m, $utilisateur->getActif() ? 'Oui' : 'Non')
            ;

            $m++;
        }

        // Bordures sur les cellules
        for ($i = 1 ; $i <= 6 ; $i++) {
            $sheet->getStyleByColumnAndRow($i, 1)->getFont()->setBold(true);

            for ($j = 1; $j < $m; $j++) {
                $sheet->getStyleByColumnAndRow($i, $j)->applyFromArray(['borders' => ['outline' => ['borderStyle' => Border::BORDER_THIN, 'color' => ['rgb' => '808080']]]]);
            }
        }

        $writer = new Xlsx($spreadsheet);
        $writer->save('php://output');
    }

    /**
     * @param Dispositif[] $dispositifs
     * @return string
     * @throws \Exception
     */
    public function dispositifs(array $dispositifs)
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle("Dispositifs de l'application");

        // Tailles des colonnes
        $sheet->getColumnDimension('A')->setWidth(30);
        $sheet->getColumnDimension('B')->setWidth(40);
        $sheet->getColumnDimension('C')->setWidth(15);
        $sheet->getColumnDimension('D')->setWidth(10);
        $sheet->getColumnDimension('E')->setWidth(20);
        $sheet->getColumnDimension('F')->setWidth(20);
        $sheet->getColumnDimension('G')->setWidth(30);
        $sheet->getColumnDimension('H')->setWidth(20);
        $sheet->getColumnDimension('I')->setWidth(35);
        $sheet->getColumnDimension('J')->setWidth(20);
        $sheet->getColumnDimension('K')->setWidth(20);
        $sheet->getColumnDimension('L')->setWidth(30);
        $sheet->getColumnDimension('M')->setWidth(30);
        $sheet->getColumnDimension('N')->setWidth(30);
        $sheet->getColumnDimension('O')->setWidth(30);
        $sheet->getColumnDimension('P')->setWidth(30);
        $sheet->freezePane('A2');

        // En-têtes
        $sheet
            ->setCellValue('A1', 'Libelle')
            ->setCellValue('B1', 'Direction propriétaire')
            ->setCellValue('C1', 'Etat')
            ->setCellValue('D1', 'Plateforme')
            ->setCellValue('E1', 'Criticite')
            ->setCellValue('F1', 'Complexité')
            ->setCellValue('G1', 'Référent SROD')
            ->setCellValue('H1', 'Lien vers cèdre')
            ->setCellValue('I1', 'Version de production')
            ->setCellValue('J1', 'Eligibilité reversibilitée')
            ->setCellValue('K1', 'Référentiels')
            ->setCellValue('L1', 'Domaines des bindings')
            ->setCellValue('M1', 'Règles métiers')
            ->setCellValue('N1', 'JavaScript partagés')
            ->setCellValue('O1', 'Styles partagés')
            ->setCellValue('P1', 'Formulaires modèles')

        ;

        // Lignes du tableau
        $m = 2;
        foreach ($dispositifs as $dispositif) {
            $sheet
                ->setCellValueByColumnAndRow(1, $m, $dispositif->getLibelle())
                ->setCellValueByColumnAndRow(2, $m, $dispositif->getStructure())
                ->setCellValueByColumnAndRow(3, $m, $dispositif->getEtat() ? 'Oui' : 'Non')
                ->setCellValueByColumnAndRow(4, $m, $dispositif->getPlateforme())
                ->setCellValueByColumnAndRow(5, $m, $dispositif->getCriticite())
                ->setCellValueByColumnAndRow(6, $m, $dispositif->getComplexite())
                ->setCellValueByColumnAndRow(7, $m, $dispositif->getSrodUtilisateur())
                ->setCellValueByColumnAndRow(8, $m, $dispositif->getCedre())
                ->setCellValueByColumnAndRow(9, $m, $dispositif->getVersionProd())
                ->setCellValueByColumnAndRow(10, $m, $dispositif->getEligibleReversibilite() ? 'Oui' : 'Non')
                ->setCellValueByColumnAndRow(11, $m, $dispositif->getReferentiels())
                ->setCellValueByColumnAndRow(12, $m, $dispositif->getDomaineBindings())
                ->setCellValueByColumnAndRow(13, $m, $dispositif->getReglesMetier())
                ->setCellValueByColumnAndRow(13, $m, $dispositif->getJavascriptPartages())
                ->setCellValueByColumnAndRow(13, $m, $dispositif->getStylesPartages())
                ->setCellValueByColumnAndRow(13, $m, $dispositif->getFormulairesModeles())
            ;
            $m++;
        }

        // Bordures sur les cellules
        for ($i = 1 ; $i <= 6 ; $i++) {
            $sheet->getStyleByColumnAndRow($i, 1)->getFont()->setBold(true);

            for ($j = 1; $j < $m; $j++) {
                $sheet->getStyleByColumnAndRow($i, $j)->applyFromArray(['borders' => ['outline' => ['borderStyle' => Border::BORDER_THIN, 'color' => ['rgb' => '808080']]]]);
            }
        }
        $writer = new Xlsx($spreadsheet);
        $writer->save('php://output');
    }

    /**
     * @param Dispositif[] $dispositifs
     * @return string
     * @throws \Exception
     */
    public function evolutions(array $evolutions, $dispo = false)
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        if ($dispo) {
            $sheet->setTitle("Evolutions du dispositif " . $evolutions[0]->getDispositif());
        } else {
            $sheet->setTitle("Toutes les évolutions");
        }

        // Tailles des colonnes
        $sheet->getColumnDimension('A')->setWidth(30);
        $sheet->getColumnDimension('B')->setWidth(30);
        $sheet->getColumnDimension('C')->setWidth(30);
        $sheet->getColumnDimension('D')->setWidth(15);
        $sheet->getColumnDimension('E')->setWidth(15);
        $sheet->getColumnDimension('F')->setWidth(15);
        $sheet->getColumnDimension('G')->setWidth(15);
        $sheet->getColumnDimension('H')->setWidth(20);
        $sheet->getColumnDimension('I')->setWidth(20);
        $sheet->getColumnDimension('J')->setWidth(20);
        $sheet->getColumnDimension('K')->setWidth(30);
        $sheet->getColumnDimension('L')->setWidth(40);
        $sheet->getColumnDimension('M')->setWidth(40);
        $sheet->getColumnDimension('N')->setWidth(40);
        $sheet->getColumnDimension('O')->setWidth(40);
        $sheet->getColumnDimension('P')->setWidth(40);
        $sheet->getColumnDimension('Q')->setWidth(40);
        $sheet->getColumnDimension('R')->setWidth(40);
        $sheet->getColumnDimension('S')->setWidth(40);
        $sheet->getColumnDimension('T')->setWidth(40);
        $sheet->freezePane('A2');

        // En-têtes
        $sheet
            ->setCellValue('A1', 'Libelle')
            ->setCellValue('B1', 'Dispositif')
            ->setCellValue('C1', 'Statut actuel')
            ->setCellValue('D1', 'Plateforme')
            ->setCellValue('E1', 'Version')
            ->setCellValue('F1', 'Priorité')
            ->setCellValue('G1', 'Complexité')
            ->setCellValue('H1', 'Référent SROD')
            ->setCellValue('I1', 'Référent DSI')
            ->setCellValue('J1', 'Référent BOWI')
            ->setCellValue('K1', 'Lien vers cèdre')
            ->setCellValue('L1', 'Livraison en recette SROD souhaitée')
            ->setCellValue('M1', 'Livraison en recette Direction souhaitée')
            ->setCellValue('N1', 'Ouverture en production souhaitée')
            ->setCellValue('O1', 'Livraison en recette SROD prévisionnelle')
            ->setCellValue('P1', 'Livraison en recette Direction prévisionnelle')
            ->setCellValue('Q1', 'Ouverture en production prévisionnelle')
            ->setCellValue('R1', 'Livraison en recette SROD réelle')
            ->setCellValue('S1', 'Livraison en recette Direction réelle')
            ->setCellValue('T1', 'Ouverture en production réelle')

        ;

        // Lignes du tableau
        $m = 2;
        foreach ($evolutions as $evolution) {
            $sheet
                ->setCellValueByColumnAndRow(1, $m, $evolution->getLibelle())
                ->setCellValueByColumnAndRow(2, $m, $evolution->getDispositif())
                ->setCellValueByColumnAndRow(3, $m, $evolution->getEvolutionStatuts()[0]->getStatut())
                ->setCellValueByColumnAndRow(4, $m, $evolution->getDispositif()->getPlateforme())
                ->setCellValueByColumnAndRow(5, $m, $evolution->getVersion())
                ->setCellValueByColumnAndRow(6, $m, $evolution->getPriorite())
                ->setCellValueByColumnAndRow(7, $m, $evolution->getComplexite())
                ->setCellValueByColumnAndRow(8, $m, $evolution->getSrodUtilisateur())
                ->setCellValueByColumnAndRow(9, $m, $evolution->getDsiUtilisateur())
                ->setCellValueByColumnAndRow(10, $m, $evolution->getBowiUtilisateur())
                ->setCellValueByColumnAndRow(11, $m, $evolution->getDispositif()->getCedre())
                ->setCellValueByColumnAndRow(12, $m, $evolution->getLivraisonRecetteSrodSouhaitee())
                ->setCellValueByColumnAndRow(13, $m, $evolution->getLivraisonRecetteDirectionSouhaitee())
                ->setCellValueByColumnAndRow(14, $m, $evolution->getOuvertureProductionSouhaitee())
                ->setCellValueByColumnAndRow(15, $m, $evolution->getLivraisonRecetteSrodPrevisionnelle())
                ->setCellValueByColumnAndRow(16, $m, $evolution->getLivraisonRecetteDirectionPrevisionnelle())
                ->setCellValueByColumnAndRow(17, $m, $evolution->getOuvertureProductionPrevisionnelle())
                ->setCellValueByColumnAndRow(18, $m, $evolution->getLivraisonRecetteSrod())
                ->setCellValueByColumnAndRow(19, $m, $evolution->getLivraisonRecetteDirection())
                ->setCellValueByColumnAndRow(20, $m, $evolution->getOuvertureProduction())
            ;
            $m++;
        }

        // Bordures sur les cellules
        for ($i = 1 ; $i <= 6 ; $i++) {
            $sheet->getStyleByColumnAndRow($i, 1)->getFont()->setBold(true);

            for ($j = 1; $j < $m; $j++) {
                $sheet->getStyleByColumnAndRow($i, $j)->applyFromArray(['borders' => ['outline' => ['borderStyle' => Border::BORDER_THIN, 'color' => ['rgb' => '808080']]]]);
            }
        }
        $writer = new Xlsx($spreadsheet);
        $writer->save('php://output');
    }
}