<?php

namespace App\Services;

use App\Entity\Statut;
use App\Entity\Structure;
use Doctrine\ORM\EntityManagerInterface;
use phpDocumentor\Reflection\Types\Null_;

/**
 * Class Statistique
 * @package App\Services
 */
class Statistique
{
    public function nbJoursStatut($evolution, $statut, int $nbJours)
    {
        
        $idStatut = $statut->getId();
        // dd($this->arrayStatuts);
        // dd($evolution->getArrayStatuts());
        
        $evolution->setArrayStatuts($idStatut, $nbJours);
        

    }
}