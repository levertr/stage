<?php

namespace App\Services;

use App\Entity\Structure;
use Doctrine\ORM\EntityManagerInterface;
use phpDocumentor\Reflection\Types\Null_;

class Organigramme
{
    private $em;
    private $branches = [];
    private $i = 0;
    private $niveau = 0;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }
    
    /**
     * Retourne un tableau de structures ordonnées en arbre à partir d'un niveau fourni
     * @param Structure $structure
     * @param int $i
     * @return array
     */
    public function getTree(Structure $structure, $i = 0)
    {
        $branches = [];
        foreach ($structure->getEnfants() as $enfant) {
            $branches[$i]['id'] = $enfant->getId();
            $branches[$i]['code'] = $enfant->getCode();
            $branches[$i]['libelle'] = $enfant->getLibelle();
            $branches[$i]['enfant'] = $this->getTree($enfant, $i);

            $i++;

        }

        return $branches;

    }

    public function getFlatTree(Structure $structure, $niveau = 0)
    {
        foreach ($structure->getEnfants() as $enfant) {
            // dd($enfant);
            $this->branches[$this->i]['id'] = $enfant->getId();
            $this->branches[$this->i]['code'] = $enfant->getCode();
            $this->branches[$this->i]['libelle'] = $enfant->getLibelle();
            $this->branches[$this->i]['niveau'] = $niveau;

            $this->i++;
            
            $this->getFlatTree($enfant, $niveau+1);
        }

        return $this->branches;

    }

    public function getObjectFlatTree(Structure $structure)
    {
        foreach ($structure->getEnfants() as $enfant) {
            $this->branches[] = $enfant;

            $this->getObjectFlatTree($enfant);
        }

        return $this->branches;
    }

    /**
     * @param array $branches
     */
    public function displayTree($branches)
    { 
        echo '<ul>';
        foreach($branches as $noeud) {
            echo '<li>'.$noeud['libelle'];
            $this->displayTree($noeud['enfant']);
            echo '</li>';
        }
        echo '</ul>';
    }
}