<?php

namespace App\Services;

use App\Entity\Evolution;
use App\Entity\EvolutionStatut;
use App\Entity\Utilisateur;
use Twig\Environment;

class Email
{
    private $mailer;
    private $environment;
    private $appName;

    /**
     * Email constructor.
     * @param \Swift_Mailer $mailer
     * @param Environment $environment
     */
    public function __construct(\Swift_Mailer $mailer, Environment $environment, string $appName)
    {
        $this->mailer = $mailer;
        $this->environment = $environment;
        $this->appName = $appName;
    }

    public function sendUtilisateur(Utilisateur $utilisateur, $editor, $string)
    {
        $mot = '';
        switch ($string) {
            case "new":
                $mot = 'Création';
                break;
            case "edit":
                $mot = 'Modification';
                break;
            case "delete":
                $mot = "Suppression";
                break;
        }

        $message = (new \Swift_Message('[' . $this->appName . ']' . ' ' . $mot . ' ' . 'de votre compte'))
            ->setFrom('nepasrepondre@hautsdefrance.fr')
            ->setTo($utilisateur->getEmail())
            ->setBody(
                $this->environment->render(
                    'email/utilisateur.html.twig',
                    [
                        'utilisateur' => $utilisateur,
                        'string' => $string,
                        'editor' => $editor
                    ]
                ),
                'text/html'
            );
        return $this->mailer->send($message) ? true : false;
    }

    public function sendNewEvolution(Evolution $evolution)
    {
        $message = (new \Swift_Message('[' . $this->appName . ']' . ' ' . 'Création de l\'évolution ' . $evolution . ' du dispositif ' . $evolution->getDispositif()))
            ->setFrom('nepasrepondre@hautsdefrance.fr')
            ->setTo($evolution->getSrodUtilisateur()->getEmail())
            ->setBody(
                $this->environment->render(
                    'email/evolution_new.html.twig',
                    ['evolution' => $evolution]
                ),
                'text/html'
            );
        return $this->mailer->send($message) ? true : false;
    }

    public function sendEditEvolution(EvolutionStatut $evolutionStatut, $user)
    {

        $message = (new \Swift_Message('[' . $this->appName . ']' . ' ' . 'Modification de l\'évolution ' . $evolutionStatut->getEvolution() . ' du dispositif ' . $evolutionStatut->getEvolution()->getDispositif()))
            ->setFrom('nepasrepondre@hautsdefrance.fr')
            ->setTo($user->getEmail())
            ->setBody(
                $this->environment->render(
                    'email/evolution_edit.html.twig',
                    [
                        'evolutionStatut' => $evolutionStatut,
                        'user' => $user
                    ]
                ),
                'text/html'
            );
        return $this->mailer->send($message) ? true : false;
    }

    public function sendMailReporting($corps, $mailreporting, $userMail)
    {
        setlocale(LC_TIME, 'fr_FR.UTF8', 'fr.UTF8', 'fr_FR.UTF-8', 'fr.UTF-8', 'fra');

        $emails = explode(', ', $mailreporting->getDestinatairesEnPlus());
        foreach ($mailreporting->getDestinataires() as $destinataire) {
            $emails[] = $destinataire->getEmail();
        }
        $emails[] = $userMail;
        
        $message = (new \Swift_Message('[' . $this->appName . ']' . ' ' . $mailreporting->getObjet() . ' - ' . ucfirst(strftime('%A %d %B %G', strtotime($mailreporting->getDateEtatLieux()->format('Y-m-d'))))))
            ->setFrom('nepasrepondre@hautsdefrance.fr')
            ->setTo($emails)
            ->setBody($corps, 'text/html');
        return $this->mailer->send($message) ? true : false;
    }
}
