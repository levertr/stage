<?php

namespace App\Services;

class IgnAdressePostale
{
    /**
     * Requêtes l'API IGN pour récupération des adresses postales
     *
     * @param string $q
     * @param string $code_insee
     * @return mixed
     */
    public function findAdresses(string $q, string $code_insee = '')
    {
        $q = iconv('UTF-8', 'ASCII//TRANSLIT', $q);
        $q = trim(preg_replace("#[^A-Za-z0-9]#", " ", $q));
        $q = urlencode($q);

        if ($code_insee) {
            $url = "https://api-adresse.data.gouv.fr/search/?limit=100&citycode=$code_insee&q=$q";
        } else {
            $url = "https://api-adresse.data.gouv.fr/search/?limit=100&q=$q";
        }

        $curl = curl_init($url);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        $curl_response = curl_exec($curl);
        curl_close($curl);

        return json_decode($curl_response, true);
    }

    /**
     * Récupère les infos essentielles
     *
     * @param array $response
     * @return json
     */
    public function formatResponse(array $response)
    {
        $adresses = [];

        if ($response['features']) {
            foreach ($response['features'] as $item) {
                $adresses[] = array(
                    'citycode' => $item['properties']['citycode'],
                    'postcode' => $item['properties']['postcode'],
                    'city' => $item['properties']['city'],
                    'value' => $item['properties']['name'],
                    'id' => $item['properties']['id'],
                    'score' => $item['properties']['score'],
                    'label' => $item['properties']['label'],
                    'context' => $item['properties']['context'],
                    'longitude' => $item['geometry']['coordinates'][0],
                    'latitude' => $item['geometry']['coordinates'][1],
                );
            }
        }

        return json_encode($adresses);
    }
}