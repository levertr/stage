<?php

namespace App\Controller;

use App\Entity\Commentaire;
use App\Entity\Dispositif;
use App\Entity\Evolution;
use App\Entity\Structure;
use App\Form\CommentaireFormType;
use App\Form\DispositifType;
use App\Form\StructureDefaultType;
use App\Services\Excel;
use App\Services\Organigramme;
use Egulias\EmailValidator\Warning\Comment;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Doctrine\Common\Persistence\ManagerRegistry as Doctrine;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\HttpFoundation\StreamedResponse;

/**
 * @Route("/dispositif", name="dispositif")
 */
class DispositifController extends AbstractController
{
    private $dispositif;

    private $service;
    private $em;

    public function __construct(Organigramme $service, Doctrine $doctrine)
    {
        $this->service = $service;
        $this->em = $doctrine->getManager();
    }

    /**
     * @Route("/", name="_index")
     */
    public function index(Request $request)
    {
        $form = $this->createForm(StructureDefaultType::class);
        $form->handleRequest($request);
        
        return $this->render('dispositif/index.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/list/{page<\d+>}", name="_list", methods={"GET", "POST"}, defaults={"page"=1})
     * @param int|null $page
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function list(int $page, Request $request, PaginatorInterface $paginator): Response
    {
        $q = $request->get('q', '');
        $vue = $request->get('vue', '');
        $input = $request->get('input', '');
        $form = $request->get('structure_default', '');

        if (isset($form['structure'])) {
            $structure = $form['structure'];
        } else $structure = null;
        
        $entities = $this->getDoctrine()
            ->getRepository(Dispositif::class)
            ->findByFilter($q, $page, $vue, $input, $structure);

        $dispositifs = $paginator->paginate($entities, $page, Dispositif::NUM_ITEMS);

        return $this->render('dispositif/_list.html.twig', [
            'dispositifs' => $dispositifs,
        ]);
    }

    /**
     * @Route("/new", methods={"GET", "POST"}, name="_new")
     * @IsGranted("ROLE_CONTRIBUTEUR")
     * @param Request $request
     * @return Response
     */
    public function new(Request $request): Response
    {
        $dispositif = new Dispositif();
        $dispositif->setCreatedAt(new \DateTime());
        $dispositif->setUpdatedAt(new \DateTime('now'));

        $form = $this->createForm(DispositifType::class, $dispositif);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($dispositif);
            $em->flush();

            return new Response('ok');
        }

        return $this->render('dispositif/_new.html.twig', [
            'post' => $dispositif,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/edit/{id<\d+>}", methods={"GET", "POST"}, name="_edit")
     * @IsGranted("ROLE_CONTRIBUTEUR")
     * @param Request $request
     * @param Dispositif $dispositif
     * @return Response
     */
    public function edit(Request $request, Dispositif $dispositif): Response
    {
        $form = $this->createForm(DispositifType::class, $dispositif);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $dispositif->setUpdatedAt(new \DateTime());

            $this->getDoctrine()->getManager()->flush();

            return new Response('ok');
        }

        return $this->render('dispositif/_edit.html.twig', [
            'dispositif' => $dispositif,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/excel", name="_excel")
     * @param Excel $excel
     * @return Response
     */
    public function excel(Excel $excel): Response
    {
        $dispositifs = $this->getDoctrine()
            ->getRepository(Dispositif::class)
            ->findBy([], ['libelle' => 'asc']);

        $response =  new StreamedResponse(function () use ($excel, $dispositifs) {
            $excel->dispositifs($dispositifs);
        });
        $response->headers->set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        $response->headers->set('Content-Disposition', 'attachment;filename="dispositifs.xlsx"');
        $response->headers->set('Cache-Control', 'max-age=0');

        return $response;
    }

    /**
     * @Route("/delete/{id<\d+>}", methods={"DELETE"}, name="_delete")
     * @IsGranted("ROLE_ADMIN")
     * @param Request $request
     * @param Dispositif $dispositif
     * @return Response
     */
    public function delete(Request $request, Dispositif $dispositif): Response
    {
        if (!$this->isCsrfTokenValid('delete', $request->request->get('token'))) {
            $this->addFlash('danger', 'Le dispositif n\'a pas été supprimé : validation incorrect');

            return $this->redirectToRoute('dispositif_index');
        }

        try {
            $em = $this->getDoctrine()->getManager();
            $em->remove($dispositif);
            $em->flush();

            $this->addFlash('success', 'Le dispositif a été supprimé.');

            return $this->redirectToRoute('dispositif_index');
        } catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add('danger', 'Suppression impossible : ce dispositif est lié à des actions.');

            return $this->redirectToRoute('dispositif_edit', ['id' => $dispositif->getId()]);
        }
    }

    /**
     * @Route("/{id}", name="_show")
     * @param int $id
     */
    public function show($id)
    {
        $this->dispositif = $this->getDoctrine()->getRepository(Dispositif::class)->findOneBy(['id' => $id]);

        if (!$this->dispositif) {
            throw $this->createNotFoundException('Le dispositif n\'existe pas');
        }

        return $this->renderTwig('dispositif/show.html.twig');
    }

    /**
     * @Route("/{id}/listeDetails", name="_listeDetails")
     */
    public function listeDetails($id)
    {
        $this->show($id);

        return $this->renderTwig('dispositif/_details.html.twig');
    }

    public function renderTwig($string): Response
    {
        return $this->render($string, [
            'dispositif' => $this->dispositif,
        ]);
    }
}
