<?php

namespace App\Controller;

use App\Entity\Evolution;
use App\Entity\EvolutionStatut;
use App\Form\EvolutionStatutType;
use App\Services\Email;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/evolutionStatut", name="evolutionStatut")
 */
class EvolutionStatutController extends AbstractController
{
    /**
     * @Route("/new/{evolution_id}",  methods={"GET", "POST"}, name="_new")
     * @IsGranted("ROLE_CONTRIBUTEUR")
     * @param int $evolution_id
     * @param Request $request
     */
    public function new($evolution_id, Request $request, Email $email): Response
    {
        $evolution = $this->getDoctrine()->getRepository(Evolution::class)->findOneBy(['id' => $evolution_id]);

        $evolutionStatut = new EvolutionStatut();
        $evolutionStatut->setEvolution($evolution);
        $evolutionStatut->setUtilisateur($this->getUser());

        $mostRecentEvolutionStatut = $this->getDoctrine()->getRepository(EvolutionStatut::class)->findOneBy(
            ['evolution' => $evolution],
            ['createdAt' => 'desc']
        );

        $statutBefore = $mostRecentEvolutionStatut->getStatut();

        $form = $this->createForm(EvolutionStatutType::class, $evolutionStatut, [
            'statutEnCours' => $mostRecentEvolutionStatut->getStatut(),
            'dateDuStatutEnCours' => $mostRecentEvolutionStatut->getCreatedAt(),
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $statutAfter = $form["statut"]->getData();

            if ($statutAfter->getId() != $statutBefore->getId() && $mostRecentEvolutionStatut->getCreatedAt() < $form["createdAt"]->getData()) {

                if (substr($statutAfter, 0, 3) == 'DSI') {
                    $user = $evolution->getDsiUtilisateur();
                } elseif (substr($statutAfter, 0, 4) == 'SROD') {
                    $user = $evolution->getSrodUtilisateur();
                } else {
                    $user = $evolution->getDsiUtilisateur();
                }

                $email->sendEditEvolution($evolutionStatut, $user);

                $doctrine = $this->getDoctrine()->getManager();
                $doctrine->persist($evolutionStatut);
                $doctrine->flush();
                return new Response('ok');

            } else if ($mostRecentEvolutionStatut->getCreatedAt() >= $form["createdAt"]->getData()) {
                $this->addFlash('warning', 'Veuillez choisir une date postérieure au précédent statut.');
            } else $this->addFlash('warning', 'Vous devez choisir un nouveau statut.');
        }

        return $this->render('evolution_statut/_new.html.twig', [
            'evolutionStatut' => $evolutionStatut,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @return Response
     * @Route("/show/{evolution_id}", name="_list")
     * @param int $evolution_id
     * @param Request $request
     */
    public function listNbJours($evolution_id, Request $request): Response
    {
        $evolution = $this->getDoctrine()->getRepository(Evolution::class)->findOneBy(['id' => $evolution_id]);
        $evolutionStatuts = $evolution->getEvolutionStatuts();
        $nbJours = [];
        $compteur = 0;
        $i = 0;

        foreach ($evolutionStatuts as $evolutionStatut) {
            $i++;
            if ($i > 1) {
                $nbJours[$compteur] = $evolutionStatut->getCreatedAt()
                    ->diff($evolutionStatuts[$i - 2]->getCreatedAt())->format("%a");
                $compteur++;
            }
        }

        return $this->render('evolution_statut/_listNbJours.html.twig', [
            'evolutionStatuts' => $evolutionStatuts,
            'nbJours' => $nbJours
        ]);
    }
}
