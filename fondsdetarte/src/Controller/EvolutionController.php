<?php

namespace App\Controller;

use App\Entity\Commentaire;
use App\Entity\Dispositif;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Evolution;
use App\Entity\EvolutionStatut;
use App\Form\CommentaireFormType;
use App\Form\EvolutionType;
use App\Services\Email;
use App\Services\Excel;
use App\Services\Statistique;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Finder\Exception\AccessDeniedException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\StreamedResponse;

/**
 * @Route("/evolution", name="evolution")
 */
class EvolutionController extends AbstractController
{
    private $evolution;
    private $dispositif;
    private $evolutionStatut;

    /**
     * @Route("/index", name="_index")
     * @return Response
     */
    public function index(): Response
    {
        return $this->render('evolution/index.html.twig', []);
    }

    /**
     * @Route("/listAll/{page<\d+>}", name="_listAll", methods={"GET", "POST"}, defaults={"page": 1})
     * @param int|null $page
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function listAll(int $page, Request $request, PaginatorInterface $paginator): Response
    {
        $q = $request->get('q', '');
        $vue = $request->get('vue', '');
        $input = $request->get('input', '');

        $entities = $this->getDoctrine()
            ->getRepository(Evolution::class)
            ->findByAllEvolutions($q, $page, $vue, $input);

        $evolutions = $paginator->paginate($entities, $page, Evolution::NUM_ITEMS);

        return $this->render('evolution/_list.html.twig', [
            'evolutions' => $evolutions,
            'vue' => $vue,
            'input' => $input,
            'dispo' => true,
        ]);
    }

    /**
     * @Route("/list/{dispositif_id}/{page<\d+>}", name="_list", methods={"GET", "POST"}, defaults={"page": 1})
     * @param int|null $page
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @param int $dispositif_id
     * @return Response
     */
    public function list(int $page, Request $request, PaginatorInterface $paginator, $dispositif_id): Response
    {
        $q = $request->get('q', '');
        $vue = $request->get('vue', '');
        $input = $request->get('input', '');

        $dispositif = $this->getDoctrine()->getRepository(Dispositif::class)->findOneBy(['id' => $dispositif_id]);

        $evolutionStatuts = $this->getDoctrine()->getRepository(EvolutionStatut::class)->findBy(['statut' => 9], ['createdAt' => 'desc']);
        $arrayEvo = [];
        $time = new \DateTime('now');
        $delai = $input;

        $entities = $this->getDoctrine()
            ->getRepository(Evolution::class)
            ->findByFilter($dispositif, $q, $page, $vue, $input);

        $evolutions = $paginator->paginate($entities, $page, Evolution::NUM_ITEMS);

        return $this->render('evolution/_list.html.twig', [
            'evolutions' => $evolutions,
            'vue' => $vue,
            'input' => $input,
            'dispo' => false,
        ]);
    }

    /**
     * @Route("/excelAll/", name="_excelAll")
     * @param Excel $excel
     * @return Response
     */
    public function excel(Excel $excel): Response
    {
        $evolutions = $this->getDoctrine()
            ->getRepository(Evolution::class)
            ->findBy([], ['libelle' => 'asc']);

        $response =  new StreamedResponse(function () use ($excel, $evolutions) {
            $dispo = false;
            $excel->evolutions($evolutions, $dispo);
        });
        $response->headers->set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        $response->headers->set('Content-Disposition', 'attachment;filename="evolutions.xlsx"');
        $response->headers->set('Cache-Control', 'max-age=0');

        return $response;
    }

    /**
     * @Route("/excelDispo/{dispositif_id}", name="_excelDispo")
     * @param Excel $excel
     * @return Response
     */
    public function excelDispo(Excel $excel, $dispositif_id): Response
    {
        $dispositif = $this->getDoctrine()->getRepository(Dispositif::class)->findOneBy(['id' => $dispositif_id]);

        $evolutions = $this->getDoctrine()
            ->getRepository(Evolution::class)
            ->findBy(['dispositif' => $dispositif], ['libelle' => 'asc']);

        $response =  new StreamedResponse(function () use ($excel, $evolutions) {
            $dispo = true;
            $excel->evolutions($evolutions, $dispo);
        });
        $response->headers->set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        $response->headers->set('Content-Disposition', 'attachment;filename="evolutions.xlsx"');
        $response->headers->set('Cache-Control', 'max-age=0');

        return $response;
    }

    /**
     * @Route("/new/{dispositif_id}", methods={"GET", "POST"}, name="_new", defaults={"dispositif_id": 0})
     * @IsGranted("ROLE_CONTRIBUTEUR")
     * @param Request $request
     * @param $dispositif
     * @return Response
     */
    public function new(Request $request, $dispositif_id, Email $email): Response
    {
        $evolution = new Evolution();
        $evolution->setUpdatedAt(new \DateTime('now'));
        $evolution->addEvolutionStatut(new EvolutionStatut());
        
        $mostRecentEvolution = $this->getDoctrine()->getRepository(EvolutionStatut::class)->findOneBy(
            ['statut' => [8, 9]],
            ['createdAt' => 'desc']
        );

        if ($dispositif_id != 0) {
            $dispositif = $this->getDoctrine()->getRepository(Dispositif::class)->findOneBy(['id' => $dispositif_id]);

            if (!$dispositif) {
                throw $this->createNotFoundException('Le dispositif n\'existe pas');
            }
            $evolution->setDispositif($dispositif);
        } else {
            $dispositif = null;
        }

        if ($mostRecentEvolution) {
            $form = $this->createForm(EvolutionType::class, $evolution, [
                'mostRecentVersion' => 'Version de la dernière évolution en production : v' . $mostRecentEvolution->getEvolution()->getVersion()
            ]);
        } else {
            $form = $this->createForm(EvolutionType::class, $evolution, [
                'mostRecentVersion' => 'Aucune évolution en prod n\'est encore créée'
            ]);
        }

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($evolution);
            $em->flush();

            $email->sendNewEvolution($evolution);

            return new Response('ok');
        }

        return $this->render('evolution/_new.html.twig', [
            'post' => $evolution,
            'dispositif' => $dispositif,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/edit/{id<\d+>}", methods={"GET", "POST"}, name="_edit")
     * @IsGranted("ROLE_CONTRIBUTEUR")
     * @param Request $request
     * @param Evolution $evolution
     * @return Response
     */
    public function edit(Request $request, Evolution $evolution): Response
    {
        $mostRecentEvolution = $this->getDoctrine()->getRepository(EvolutionStatut::class)->findOneBy(
            ['statut' => [3, 4, 5]],
            ['createdAt' => 'desc']
        );

        if ($mostRecentEvolution) {
            $form = $this->createForm(EvolutionType::class, $evolution, [
                'mostRecentVersion' => 'Version de la dernière évolution en production : v' . $mostRecentEvolution->getEvolution()->getVersion()
            ]);
        } else {
            $form = $this->createForm(EvolutionType::class, $evolution, [
                'mostRecentVersion' => 'Aucune évolution en prod n\'est encore créée'
            ]);
        }

        $form->handleRequest($request);
        $dispositif = $evolution->getDispositif();
        $evolutionStatuts = $evolution->getEvolutionStatuts();

        if ($form->isSubmitted() && $form->isValid()) {
            $evolution->setUpdatedAt(new \DateTime());
            $this->getDoctrine()->getManager()->flush();

            return new Response('ok');
        }

        return $this->render('evolution/_edit.html.twig', [
            'evolution' => $evolution,
            'dispositif' => $dispositif,
            'evoStatuts' => $evolutionStatuts,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/delete/{id<\d+>}", methods={"DELETE"}, name="_delete")
     * @IsGranted("ROLE_ADMIN")
     * @param Request $request
     * @param Evolution $evolution
     * @return Response
     */
    public function delete(Request $request, Evolution $evolution): Response
    {
        $dispositif = $evolution->getDispositif();

        if (!$this->isCsrfTokenValid('delete', $request->request->get('token'))) {
            $this->addFlash('danger', 'L\'évolution n\'a pas été supprimée : validation incorrect');

            return $this->redirectToRoute('dispositif_show', ['id' => $dispositif->getId()]);
        }

        try {
            $em = $this->getDoctrine()->getManager();
            $em->remove($evolution);
            $em->flush();

            $this->addFlash('success', 'L\'évolution a été supprimée.');

            return $this->redirectToRoute('dispositif_show', ['id' => $dispositif->getId()]);
        } catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add('danger', 'Suppression impossible : cette évolution est liée à des actions.');

            return $this->redirectToRoute('dispositif_show', ['id' => $dispositif->getId()]);
        }
    }

    /**
     * @Route("/{id}", name="_show")
     * @param int $id
     */
    public function show($id)
    {
        $this->evolution = $this->getDoctrine()->getRepository(Evolution::class)->findOneBy(['id' => $id]);
        $this->dispositif = $this->evolution->getDispositif();

        $this->evolutionStatut = $this->getDoctrine()->getRepository(EvolutionStatut::class)->findOneBy(['evolution' => $id], [
            'createdAt' => 'desc'
        ]);

        if (!$this->evolution) {
            throw $this->createNotFoundException('L\' évolution n\'existe pas');
        }

        return $this->renderTwig('evolution/show.html.twig');
    }

    /**
     * @Route("/{id}/listeDetails", name="_listeDetails")
     */
    public function listeDetails($id)
    {
        $this->show($id);

        return $this->renderTwig('evolution/_details.html.twig');
    }

    /**
     * @Route("/{id}/listePlanning", name="_listePlanning")
     */
    public function listePlanning($id)
    {
        $this->show($id);

        return $this->renderTwig('evolution/_planning.html.twig');
    }

    public function renderTwig($string): Response
    {
        return $this->render($string, [
            'evolution' => $this->evolution,
            'dispositif' => $this->dispositif,
            'evoStatut' => $this->evolutionStatut
        ]);
    }
}
