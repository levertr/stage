<?php

namespace App\Controller;

use App\Entity\Evolution;
use App\Entity\EvolutionStatut;
use App\Entity\MailReporting;
use App\Entity\MailReportingUtilisateur;
use App\Entity\Utilisateur;
use App\Form\MailReportingType;
use App\Form\VerifMailType;
use App\Services\Date;
use App\Services\Email;
use Doctrine\ORM\EntityRepository;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use KMS\FroalaEditorBundle\Form\Type\FroalaEditorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\DomCrawler\Field\TextareaFormField;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Twig\Environment;

/**
 * @Route("/mailreporting", name="mailreporting")
 */
class MailReportingController extends AbstractController
{
    private $email;
    private $corps;
    private $emails;

    /**
     * Email constructor.
     * @param \Swift_Mailer $mailer
     * @param Environment $environment
     */
    public function __construct(\Swift_Mailer $mailer, Environment $environment, string $appName, Email $email)
    {
        $this->mailer = $mailer;
        $this->environment = $environment;
        $this->appName = $appName;
        $this->email = $email;
    }

    /**
     * @Route("/new", name="_new")
     */
    public function new(Request $request, Email $email): Response
    {
        $mailreporting = new MailReporting();

        $evolutions = $this->getDoctrine()->getRepository(Evolution::class)->findBy([], ['dispositif' => 'asc']);

        $array = [];
        $time = new \DateTime('now');

        $lastMailReporting = $this->getDoctrine()->getRepository(MailReporting::class)->findOneBy([], ['id' => 'desc']);

        if ($lastMailReporting) {
            $form = $this->createForm(MailReportingType::class, $mailreporting, [
                'dataDelai' => $lastMailReporting->getDelaiStatut(),
                'dataDestinataires' => $lastMailReporting->getDestinataires(),
                'dataDestinatairesEnPlus' => $lastMailReporting->getDestinatairesEnPlus(),
                'dataObjet' => $lastMailReporting->getObjet(),
                'dataEtatLieux' => $lastMailReporting->getDateEtatLieux(),
                'dataCommentaires' => $lastMailReporting->getNombreCommentaires()
            ]);
        } else {
            $form = $this->createForm(MailReportingType::class, $mailreporting);
        }

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $destinataires = $mailreporting->getDestinataires();
            $destinatairesEnPlus = $mailreporting->getDestinatairesEnPlus();

            $valid = true;
            if ($destinatairesEnPlus != null) {
                $emails = explode(', ', $destinatairesEnPlus);

                foreach ($emails as $email) {
                    $test = filter_var($email, FILTER_VALIDATE_EMAIL);
                    if ($test == false) {
                        $valid = false;
                    }
                }
            }

            foreach ($destinataires as $destinataire) {
                $emails[] = $destinataire->getEmail();
            }

            $delai = $form["delaiStatut"]->getData();
            date_sub($time, date_interval_create_from_date_string($delai . ' days'));

            if ($valid) {
                if (isset($emails)) {
                    if ($evolutions) {
                        foreach ($evolutions as $evolution) {
                            if ($evolution->getEvolutionStatuts()[0]->getStatut()->getId() == 9) {
                                if ($evolution->getEvolutionStatuts()[0]->getCreatedAt() > $time) {
                                    $array[$evolution->getId()] = $evolution;
                                }
                            } else {
                                $array[$evolution->getId()] = $evolution;
                            }
                        }

                        if (!empty($array)) {
                            $em = $this->getDoctrine()->getManager();
                            $em->persist($mailreporting);
                            $em->flush();

                            $this->corps = $this->render('email/mailreporting_new.html.twig', [
                                'evolutions' => $array,
                                'mailreporting' => $mailreporting,
                            ])->getContent();

                            return $this->verifMail($request);
                        }
                    } else {
                        $this->addFlash('warning', 'Il n\'y a aucune évolution.');
                    }
                } else {
                    $this->addFlash('warning', 'Veuillez selectionner au moins un destinataire.');
                }
            } else {
                $this->addFlash('warning', 'L\' ou les email(s) hors appli n\'est(ne sont) pas correcte(s).');
            }
        }

        return $this->render('mail_reporting/_new.html.twig', [
            'post' => $mailreporting,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/verifMail", name="_verifMail")
     */
    public function verifMail(Request $request): Response
    {
        $mailreporting = $this->getDoctrine()->getRepository(MailReporting::class)->findOneBy([], ['id' => 'desc']);
        $form = $this->createForm(VerifMailType::class, null, [
            'corps' => $this->corps
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) { 
                $this->email->sendMailReporting($form["donnee"]->getData(), $mailreporting, $this->getUser()->getEmail());
                return new Response('ok');
        }

        return $this->render('email/_newVerifMail.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/twigMail", name="mailreporting_twig")
     */
    public function generateTwigMail($evolutions, $mailreporting, $form = null): Response
    {
        return $this->render('email/submit.html.twig', [
            'form' => $form->createView()
        ]);
    }
}
