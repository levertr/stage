<?php

namespace App\Controller;

use App\Entity\Dispositif;
use App\Entity\Evolution;
use App\Entity\EvolutionStatut;
use App\Entity\Structure;
use App\Services\Chiffre;
use App\Services\Date;
use App\Services\IgnAdressePostale;
use App\Services\Ldap;
use App\Services\Organigramme;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * @Route("/", name="default")
 */
class DefaultController extends AbstractController
{
    /**
     * @Route("/", name="_index")
     * @IsGranted("ROLE_USER")
     * @return Response
     */
    public function index(): Response
    {
        return $this->render('default/index.html.twig');
    }

    /**
     * @Route("/indexEvoNonCommencees/{page<\d+>}", name="_evoNonCommencees", defaults={"page"=1})
     */
    public function indexEvoNonCommencees(): Response
    {
        $min = 0;
        $max = 3;

        $entities = $this->getDoctrine()
            ->getRepository(EvolutionStatut::class)
            ->findBy([], ['createdAt' => 'desc']);

        $evoStatutByUniqueID = array_unique($entities);
        $array = [];
        foreach ($evoStatutByUniqueID as $evoStatut) {
            if ($evoStatut->getStatut()->getId() > $min && $evoStatut->getStatut()->getId() < $max) {
                $array[] = $evoStatut;
            }
        }

        return $this->render('default/_listEvolutions.html.twig', [
            'evolutionStatuts' => $array,
            'evo' => 'nonCommencees'
        ]);
    }

    /**
     * @Route("/indexDispo/{page<\d+>}", name="_dispositifs", defaults={"page"=1})
     */
    public function indexDispo(): Response
    {
        $entities = $this->getDoctrine()
            ->getRepository(Dispositif::class)
            ->findBy([
                'etat' => 1
            ], ['updatedAt' => 'desc']);

        return $this->render('default/_listDispositifs.html.twig', [
            'dispositifs' => $entities
        ]);
    }

    /**
     * @Route("/indexEvoActives/{page<\d+>}", name="_evoActives", defaults={"page"=1})
     */
    public function indexEvoActives(PaginatorInterface $paginator, $page): Response
    {
        $min = 2;
        $max = 6;
        $entities = $this->getDoctrine()
            ->getRepository(EvolutionStatut::class)
            ->findBy([], ['createdAt' => 'desc']);

        $evoStatutByUniqueID = array_unique($entities);
        $array = [];
        foreach ($evoStatutByUniqueID as $evoStatut) {
            if ($evoStatut->getStatut()->getId() > $min && $evoStatut->getStatut()->getId() < $max) {
                $array[] = $evoStatut;
            }
        }

        return $this->render('default/_listEvolutions.html.twig', [
            'evolutionStatuts' => $array,
            'evo' => 'actives'
        ]);
    }

    /**
     * @Route("/version", name="_version")
     * @IsGranted("ROLE_USER")
     * @return Response
     */
    public function version(): Response
    {
        return $this->render('default/version.html.twig');
    }

    /**
     * @Route("/failed", name="_failed")
     * @return Response
     */
    public function failed(): Response
    {
        return $this->render('default/failed.html.twig');
    }

    /**
     * @Route("/logout", name="_logout")
     * @param Request $request
     * @return Response
     */
    public function logout(Request $request): Response
    {
        $this->get('security.token_storage')->setToken(null);
        $request->getSession()->invalidate();

        $response = new Response();
        $response->headers->clearCookie('PHPSESSID');

        return $this->render('default/logout.html.twig', [], $response);
    }

    /**
     * Retourne un tableau d'utilisateurs AD avec nom et login en fonction du nom
     *
     * @Route("/ldapsearch/{q}", name="ldap_search", options={"expose" = true})
     * @IsGranted("ROLE_USER")
     * @param Ldap $ldap
     * @param string $q
     * @return Response
     */
    public function ldapSearch(Ldap $ldap, string $q = '')
    {
        $users = $ldap->findUsersByName($q);

        $return = [];
        if (count($users) != 0) {
            $i = 0;
            foreach ($users as $user) {
                if ($ldap->getNom($user)) {
                    $return[$i]['value'] = $ldap->getNom($user) . ' ' . $ldap->getPrenom($user);
                    $return[$i]['login'] = $ldap->getLogin($user);
                    $return[$i]['fonction'] = $ldap->getFonction($user);

                    $i++;
                }
            }

            foreach ($return as $key => $row) {
                $value[$key] = $row['value'];
                $login[$key] = $row['login'];
                $fonction[$key] = $row['fonction'];
            }
            array_multisort($value, SORT_ASC, $login, SORT_ASC, $return);
        }

        return new Response(json_encode($return));
    }

    /**
     * Retourne un json pour autocomplétion d'adresse postale
     * @Route("/adressepostale/{q}/{code_insee?}", name="adresse_postale", options={"expose" = true}, defaults={"code_insee": ""})
     * @param Request $request
     * @param IgnAdressePostale $ignAdressePostale
     * @param string $q
     * @param string $code_insee
     * @return Response
     */
    public function adressePostale(Request $request, IgnAdressePostale $ignAdressePostale, string $q, string $code_insee = '')
    {
        if (!$request->isXmlHttpRequest()) {
            throw new AccessDeniedException();
        }

        $adresses = $ignAdressePostale->findAdresses($q, $code_insee);

        return $adresses ? new Response($ignAdressePostale->formatResponse($adresses)) : new Response('');
    }

    /**
     * Retourne un json d'événements
     *
     * @Route("/calendar", name="fullcalendar", methods={"POST"}, options={"expose" = true})
     * @param Request $request
     * @return Response
     */
    public function calendar(Request $request): Response
    {
        if (!$request->isXmlHttpRequest()) {
            throw new AccessDeniedException();
        }

        $today = new \DateTime();

        $events = [[
            'id' => 1,
            'title' => 'TOTO',
            'start' => $today->format('Y-m-d'),
            'textColor' => '#FFFFFF',
        ], [
            'id' => 2,
            'title' => 'TATA',
            'start' => $today->modify('+1 day')->format('Y-m-d'),
            'textColor' => '#FFFFFF',
        ]];

        return new Response(json_encode($events));
    }
}
