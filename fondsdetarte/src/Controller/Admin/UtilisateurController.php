<?php

namespace App\Controller\Admin;

use App\Entity\Utilisateur;
use App\Form\UtilisateurAdType;
use App\Form\UtilisateurType;
use App\Form\StructureType;
use App\Repository\UtilisateurRepository;
use App\Services\Email;
use App\Services\Excel;
use App\Services\Ldap;
use Doctrine\ORM\UnitOfWork;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;


/**
 * @Route("/admin/utilisateur")
 * @IsGranted("ROLE_ADMIN")
 */
class UtilisateurController extends AbstractController
{
    /**
     * @Route("/", methods={"GET"}, name="utilisateur_index")
     * @return Response
     */
    public function index(): Response
    {
        return $this->render('admin/utilisateur/index.html.twig');
    }

    /**
     * @Route("/list/p/{page<\d+>}", name="utilisateur_list", methods={"GET", "POST"}, defaults={"page": 1})
     * @param int|null $page
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @return Response
     */
    public function list(int $page, Request $request, PaginatorInterface $paginator): Response
    {
        // if (!$request->isXmlHttpRequest()) {
        //     throw new AccessDeniedException();
        // }

        $q = $request->get('q', '');

        $entities = $this->getDoctrine()
            ->getRepository(Utilisateur::class)
            ->findByFilter($q, $page);

        $utilisateurs = $paginator->paginate($entities, $page, Utilisateur::NUM_ITEMS);
        return $this->render('admin/utilisateur/_list.html.twig', [
            'utilisateurs' => $utilisateurs,
        ]);
    }

    /**
     * @Route("/new", methods={"GET", "POST"}, name="utilisateur_new")
     * @param Request $request
     * @return Response
     */
    public function new(Request $request, Email $email): Response
    {
        $utilisateur = new Utilisateur();
        $utilisateur->setCreatedAt(new \DateTime());
        $utilisateur->setUpdatedAt(new \DateTime('now'));

        $form = $this->createForm(UtilisateurType::class, $utilisateur);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($utilisateur);
            $em->flush();

            $email->sendUtilisateur($utilisateur, $this->getUser(), "new");

            return new Response('ok');
        }

        return $this->render('admin/utilisateur/_new.html.twig', [
            'post' => $utilisateur,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/newad", methods={"GET", "POST"}, name="utilisateur_newad")
     * @param Request $request
     * @param Ldap $ldap
     * @return RedirectResponse|Response
     */
    public function newAdAction(Request $request, Ldap $ldap, Email $email)
    {
        if (!$request->isXmlHttpRequest()) {
            throw new AccessDeniedException();
        }

        $utilisateur = new Utilisateur();
        $form = $this->createForm(UtilisateurAdType::class, $utilisateur);

        if ($request->getMethod() == 'POST') {
            $form->handleRequest($request);

            // On récupère le login de l'autocomplete
            $login = $form->get('adUserHidden')->getData();

            // On ne crée que s'il n'existe pas déjà en base
            $em = $this->getDoctrine()->getManager();
            $utilisateur = $em->getRepository(Utilisateur::class)->findOneByLogin($login);

            if (!$utilisateur) {
                $user = $ldap->persistEntity($login);

                if ($user) {
                    $email->sendCreationCompte($user);

                    return new Response('ok');
                } else {
                    return new Response("Utilisateur inconnu.");
                }
            } else {
                return new Response('Cet utilisateur est déjà dans la liste.');
            }
        }


        return $this->render('admin/utilisateur/_formAd.html.twig', [
            'utilisateur' => $utilisateur,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Affichage d'un select
     * @Route("/select/{prefixe}", name="utilisateur_select", methods={"GET"})
     * @param Request $request
     * @param string $prefixe
     * @return Response
     */
    public function select(Request $request, string $prefixe): Response
    {
        if (!$request->isXmlHttpRequest()) {
            throw new AccessDeniedException();
        }

        $utilisateurs = $this->getDoctrine()
            ->getRepository(Utilisateur::class)
            ->findBy([], ['nom' => 'ASC']);

        return $this->render('admin/utilisateur/_select.html.twig', [
            'utilisateurs' => $utilisateurs,
            'prefixe' => $prefixe,
        ]);
    }

    /**
     * @Route("/edit/{id<\d+>}", methods={"GET", "POST"}, name="utilisateur_edit")
     * @param Request $request
     * @param Utilisateur $utilisateur
     * @return Response
     */
    public function edit(Request $request, Utilisateur $utilisateur, Email $email): Response
    {
        $form = $this->createForm(UtilisateurType::class, $utilisateur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $utilisateur->setUpdatedAt(new \DateTime());

            $this->getDoctrine()->getManager()->flush();

            $email->sendUtilisateur($utilisateur, $this->getUser(), "edit");

            return new Response('ok');
        }

        return $this->render('admin/utilisateur/_edit.html.twig', [
            'utilisateur' => $utilisateur,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/excel", name="utilisateur_excel")
     * @param Excel $excel
     * @return Response
     */
    public function excel(Excel $excel): Response
    {
        $utilisateurs = $this->getDoctrine()
            ->getRepository(Utilisateur::class)
            ->findBy([], ['nom' => 'ASC']);

        $response =  new StreamedResponse(function () use ($excel, $utilisateurs) {
            $excel->utilisateurs($utilisateurs);
        });
        $response->headers->set('Content-Type', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        $response->headers->set('Content-Disposition', 'attachment;filename="utilisateurs.xlsx"');
        $response->headers->set('Cache-Control', 'max-age=0');
       
        return $response;
    }

    /**
     * @Route("/delete/{id<\d+>}", methods={"DELETE"}, name="utilisateur_delete")
     * @param Request $request
     * @param Utilisateur $utilisateur
     * @return Response
     */
    public function delete(Request $request, Utilisateur $utilisateur, Email $email): Response
    {
        if (!$this->isCsrfTokenValid('delete', $request->request->get('token'))) {
            $this->addFlash('danger', 'Le compte n\'a pas été supprimé : validation incorrect');

            return $this->redirectToRoute('utilisateur_index');
        }

        try {
            $em = $this->getDoctrine()->getManager();
            $em->remove($utilisateur);
            $em->flush();

            $email->sendUtilisateur($utilisateur, $this->getUser(), "delete");

            $this->addFlash('success', 'Le compte utilisateur a été supprimé.');

            return $this->redirectToRoute('utilisateur_index');
        } catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add('danger', 'Suppression impossible : cet utilisateur est lié à des actions.');

            return $this->redirectToRoute('utilisateur_edit', ['id' => $utilisateur->getId()]);
        }
    }
}
