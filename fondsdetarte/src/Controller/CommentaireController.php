<?php

namespace App\Controller;

use App\Entity\Commentaire;
use App\Entity\Dispositif;
use App\Entity\Evolution;
use App\Form\CommentaireFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/commentaire", name="commentaire")
 */
class CommentaireController extends AbstractController
{
    /**
     * @return Response
     * @Route("/show/{dispositif_id}/{evolution_id}", name="_list")
     * @param int $dispositif_id, $evolution_id
     * @param Request $request
     */
    public function list($dispositif_id, $evolution_id, Request $request): Response
    {
        $dispositif = $evolution = null;

        $dispositif = $this->getDoctrine()->getRepository(Dispositif::class)->findOneBy(['id' => $dispositif_id]);
        $evolution = $this->getDoctrine()->getRepository(Evolution::class)->findOneBy(['id' => $evolution_id]);

        if (!$dispositif) {
            $commentaires = $evolution->getCommentaires();
        } else {
            $commentaires = $dispositif->getCommentaires();
        }

        return $this->render('commentaire/_list.html.twig', [
            'commentaires' => $commentaires,
        ]);
    }

    /**
     * @return Response
     * @Route("/add/{dispositif_id}/{evolution_id}", name="_new", defaults={"dispositif_id":0, "evolution_id":0})
     * @param int $dispositif_id, $evolution_id
     * @param Request $request
     */
    public function new($dispositif_id, $evolution_id, Request $request): Response
    {
        $evolution = $dispositif = null;

        $dispositif = $this->getDoctrine()->getRepository(Dispositif::class)->findOneBy(['id' => $dispositif_id]);
        $evolution = $this->getDoctrine()->getRepository(Evolution::class)->findOneBy(['id' => $evolution_id]);

        $commentaire = new Commentaire();
        $commentaire->setUtilisateur($this->getUser());
        $commentaire->setCreatedAt(new \DateTime());
        $commentaire->setUpdatedAt(new \DateTime('now'));

        $form = $this->createForm(CommentaireFormType::class, $commentaire);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            if (!$dispositif) {
                $commentaire->setEvolution($evolution);
            } else {
                $commentaire->setDispositif($dispositif);
            }
            $doctrine = $this->getDoctrine()->getManager();
            $doctrine->persist($commentaire);
            $doctrine->flush();

            return new Response('ok');
        }

        return $this->render('commentaire/_new.html.twig', [
            'form' => $form->createView(),
            'dispositif' => $dispositif,
            'evolution' => $evolution
        ]);
    }

    /**
     * @Route("/edit/{id<\d+>}", methods={"GET", "POST"}, name="_edit")
     * @param Request $request
     * @param Commentaire $commentaire
     * @return Response
     */
    public function edit(Request $request, Commentaire $commentaire): Response
    {
        $form = $this->createForm(CommentaireFormType::class, $commentaire);
        $commentaire->setUpdatedAt(new  \DateTime('now'));
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $commentaire->setUpdatedAt(new \DateTime());

            $this->getDoctrine()->getManager()->flush();

            return new Response('ok');
        }

        return $this->render('commentaire/_edit.html.twig', [
            'commentaire' => $commentaire,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/delete/{id<\d+>}", methods={"DELETE"}, name="_delete")
     * @param Request $request
     * @param Commentaire $commentaire
     * @return Response
     */
    public function delete(Request $request, Commentaire $commentaire): Response
    {
        if (!$this->isCsrfTokenValid('delete', $request->request->get('token'))) {
            $this->addFlash('danger', 'Le commentaire n\'a pas été supprimé : validation incorrect');

            return $this->redirectToRoute('dispositif_index');
        }

        try {
            $em = $this->getDoctrine()->getManager();
            $em->remove($commentaire);
            $em->flush();

            $this->addFlash('success', 'Le commentaire a été supprimé.');

            if ($commentaire->getEvolution() == null) {
                return $this->redirectToRoute('dispositif_show', ['id' => $commentaire->getDispositif()->getId()]);
            } else  return $this->redirectToRoute('evolution_show', ['id' => $commentaire->getEvolution()->getId()]);
        } catch (\Exception $e) {
            $this->get('session')->getFlashBag()->add('danger', 'Suppression impossible : ce commentaire est lié à des actions.');

            if ($commentaire->getEvolution() == null) {
                return $this->redirectToRoute('dispositif_show', ['id' => $commentaire->getDispositif()->getId()]);
            } else  return $this->redirectToRoute('evolution_show', ['id' => $commentaire->getEvolution()->getId()]);
        }
    }
}
