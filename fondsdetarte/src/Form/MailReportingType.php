<?php

namespace App\Form;

use App\Entity\MailReporting;
use App\Entity\MailReportingUtilisateur;
use App\Entity\Utilisateur;
use App\Form\Type\DatePickerType;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MailReportingType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            // ->add('vueSource', ChoiceType::class, [
            //     'label' => 'Vue Source* :',
            //     'required' => true,
            //     'choices' => array(
            //         'Par dispositif' => 'par dispositif',
            //         'Par statut' => 'par statut',
            //     )
            // ])
            ->add('delaiStatut', null, [
                'label' => 'Delai du statut "ouverture en production" (jours)* :',
                'required' => true,
                'data' => $options['dataDelai'],
                'help' => 'Les évolutions ouvertes en prod depuis plus longtemps que le délai seront écartées du reporting.'
            ])
            ->add('destinataires', null, [
                'label' => 'Destinataires* :',
                'data' => $options['dataDestinataires'],
                'attr' => ['class' => 'selectpicker-hdf'],
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->select('u')
                        ->orderBy('u.nom', 'asc');
                }
            ])
            ->add('destinatairesEnPlus', null, [
                'label' => 'Destinataire(s) hors application :',
                'data' => $options['dataDestinatairesEnPlus'],
                'required' => false,
                'help' => 'Emails séparés d\'une virgule, ex: x@y.com, y@x.com ...'
            ])
            ->add('objet', null, [
                'label' => 'Objet* :',
                'data' => $options['dataObjet'],
                'help' => 'Objet qui sera suffixé par la date de l\'état des lieux.'
            ])
            ->add('dateEtatLieux', DatePickerType::class, [
                'label' => 'Date d\'état des lieux',
                'data' => $options['dataEtatLieux'],
                'html5' =>  false,
                'help' => 'Date qui sera ajoutée à l\'objet du mail.'
            ])
            ->add('nombreCommentaires', null, [
                'label' => 'Nombre de commentaires* :',
                'data' => $options['dataCommentaires'],
                'help' => 'Liste des X derniers commentaires des évolutions'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MailReporting::class,
            'dataDelai' => null,
            'dataDestinataires' => null,
            'dataCopieA' => null,
            'dataObjet' => null,
            'dataEtatLieux' => null,
            'dataCommentaires' => null,
            'dataDestinatairesEnPlus' => null
        ]);
    }
}
