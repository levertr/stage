<?php

namespace App\Form;

use App\Entity\Utilisateur;
use App\Entity\Structure;
use App\Form\StructureType;
use App\Services\Organigramme;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Choice;
use Doctrine\Common\Persistence\ManagerRegistry as Doctrine;

class UtilisateurType extends AbstractType
{
    private $service;
    private $em;
    private $structure;

    public function __construct(Organigramme $service, Doctrine $doctrine)
    {
        $this->service = $service;
        $this->em = $doctrine->getManager();
    }
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $this->structure = $this->em->getRepository(Structure::class)->find(356);
        
        $choices = $this->service->getObjectFlatTree($this->structure);

        $builder
            ->add('civilite', ChoiceType::class, [
                'label' => 'Civilité* :',
                'choices' => array(
                    'Mme' => 'Mme',
                    'M.' => 'M.',
                ),
                'label_attr' => ['class' => 'radio-custom radio-inline'],
                'expanded' => true,
                'required' => true,
            ])
            ->add('nom', null, [
                'attr' => ['autofocus' => true],
                'label' => 'Nom* :',
                'required' => true,
            ])
            ->add('prenom', null, [
                'label' => 'Prénom* :',
                'required' => true,
            ])
            ->add('login', null, [
                'label' => 'Identifiant* :',
                'required' => true,
            ])
            ->add('referent_dsi', CheckboxType::class, [
                'label' => "Service : DSI",
                'required' => false,
            ])
            ->add('srod', CheckboxType::class, [
                'label' => "Service : SROD",
                'required' => false,
            ])
            ->add('bowi', CheckboxType::class, [
                'label' => "Intervenant BOWI",
                'required' => false,
            ])
            ->add('role', ChoiceType::class, [
                'label' => 'Rôle* : ',
                'choices' => array(
                    'Lecteur' => 'Lecteur',
                    'Contributeur' => 'Contributeur',
                    'Administrateur' => 'Administrateur',
                ),
                'label_attr' => ['class' => 'radio-custom radio-inline'],
                'expanded' => true,
                'multiple' => false,
                'required' => true,
            ])
            ->add('email', EmailType::class, [
                'label' => 'Email* :',
            ])
            ->add('telephone', null, [
                'label' => 'Téléphone :',
                'required' => false,
            ])
            ->add('actif', ChoiceType::class, [
                'label' => 'Actif* ?',
                'choices' => array(
                    'Oui' => 1,
                    'Non' => 0,
                ),
                'label_attr' => ['class' => 'radio-custom radio-inline'],
                'expanded' => true,
                'required' => true,
            ])
            ->add('structure', null, [
                'label' => 'Direction propriétaire* :',
                'attr' => array('class' => 'selectpicker-hdf'),
                'choices' => $choices,
                'choice_label' => 'libelleIndente',
                'choice_name' => 'id',
                'required' => true
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Utilisateur::class,
        ]);
    }
}
