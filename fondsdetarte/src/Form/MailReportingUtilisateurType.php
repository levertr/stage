<?php

namespace App\Form;

use App\Entity\MailReportingUtilisateur;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MailReportingUtilisateurType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('utilisateur', null, [
                'label' => 'Utilisateur* :',
                'required' => true,
            ])
            ->add('mailreporting', null, [
                'label' => 'Mail reporting* :',
                'required' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MailReportingUtilisateur::class,
        ]);
    }
}
