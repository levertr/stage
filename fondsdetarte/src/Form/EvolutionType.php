<?php

namespace App\Form;

use App\Entity\Evolution;
use App\Entity\EvolutionStatut;
use App\Entity\Statut;
use App\Entity\Utilisateur;
use App\Form\Type\DatePickerType;
use Doctrine\DBAL\Types\DateTimeType;
use Doctrine\DBAL\Types\IntegerType;
use Doctrine\DBAL\Types\TextType;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping\Entity;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\BrowserKit\Request;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class EvolutionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $evolution_id = $builder->getData()->getId();
        $builder
            ->add('libelle', null, [
                'attr' => ['autofocus' => true],
                'label' => 'Libellé* :',
                'required' => true,
            ])
            ->add('version', NumberType::class, [
                'attr' => ['autofocus' => true],
                'label' => 'Version* :',
                'invalid_message' => "Veuillez saisir un nombre.",
                'required' => true,
                'help' => $options['mostRecentVersion']
            ])
            ->add('srodUtilisateur', EntityType::class, array(
                'class' => Utilisateur::class,
                'label' => 'Affectation SROD* :',
                'required' => true,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->select('u')
                        ->where('u.srod = 1')
                        ->orderBy('u.nom', 'asc');
                }
            ))
            ->add('priorite', ChoiceType::class, [
                'label' => 'Priorité* :',
                'required' => true,
                'choices' => array(
                    'Urgent' => 'Urgent',
                    'Moyenne' => 'Moyenne',
                    'Faible' => 'Faible'
                )
            ])
            ->add('complexite',  ChoiceType::class, [
                'label' => 'Complexité* :',
                'required' => true,
                'choices' => array(
                    'Complexe' => 'Complexe',
                    'Moyen' => 'Moyen',
                    'Simple' => 'Simple'
                )
            ])
            ->add('dsiUtilisateur', EntityType::class, array(
                'class' => Utilisateur::class,
                'label' => 'Affectation DSI :',
                'required' => false,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->select('u')
                        ->where('u.referent_dsi = 1 AND u.bowi = 0')
                        ->orderBy('u.nom', 'asc');
                }
            ))
            ->add('bowiUtilisateur', null, [
                'label' => "Affectation BOWI :",
                'required' => false,
                'help' => 'Laissez vide si l\'évolution n\'est pas BOWI',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->select('u')
                        ->where('u.bowi = 1')
                        ->orderBy('u.nom', 'asc');
                }
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Description :',
                'required' => false,
                'empty_data' => ''
            ])
            ->add('bowi', ChoiceType::class, [
                'label' => 'Est-elle BOWI ?* :',
                'required' => true,
                'choices' => array(
                    'Oui' => '1',
                    'Non' => '0',
                ),
                'label_attr' => ['class' => 'radio-custom radio-inline'],
                'expanded' => true,
                'required' => true,
            ])
            // DATES REELLES //
            ->add('ouvertureProduction', DatePickerType::class, [
                'label' => 'Ouverture en production :',
                'html5' =>  false,
                'required' => false
            ])
            ->add('livraisonRecetteDirection', DatePickerType::class, [
                'label' => 'Livraison recette Direction :',
                'html5' =>  false,
                'required' => false

            ])
            ->add('livraisonRecetteSrod', DatePickerType::class, [
                'label' => 'Livraison recette SROD :',
                'html5' =>  false,
                'required' => false
            ])
            // DATES PREVISIONNELLES //
            ->add('ouvertureProductionPrevisionnelle', DatePickerType::class, [
                'label' => 'Ouverture en production :',
                'html5' =>  false,
                'required' => false
            ])
            ->add('livraisonRecetteDirectionPrevisionnelle', DatePickerType::class, [
                'label' => 'Livraison recette Direction :',
                'html5' =>  false,
                'required' => false

            ])
            ->add('livraisonRecetteSrodPrevisionnelle', DatePickerType::class, [
                'label' => 'Livraison recette SROD :',
                'html5' =>  false,
                'required' => false
            ])
            // DATES SOUHAITEES //
            ->add('ouvertureProductionSouhaitee', DatePickerType::class, [
                'label' => 'Ouverture en production :',
                'html5' =>  false,
                'required' => false
            ])
            ->add('livraisonRecetteDirectionSouhaitee', DatePickerType::class, [
                'label' => 'Livraison recette Direction :',
                'html5' =>  false,
                'required' => false

            ])
            ->add('livraisonRecetteSrodSouhaitee', DatePickerType::class, [
                'label' => 'Livraison recette SROD :',
                'html5' =>  false,
                'required' => false
            ])
            ->add('dispositif', null, [
                'label' => 'Dispositif* :',
                'required' => true,
            ])
        ;

        if (!$evolution_id) {
            $builder
                ->add('evolutionStatuts', CollectionType::class, [
                    'entry_type' => EvolutionStatutType::class,
                    'required' => true,
                    'by_reference' => false,
                    'label' => false,
                    'entry_options' => ['label' => false]
                ]);
            }
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Evolution::class,
            'mostRecentVersion' => null
        ]);
    }
}
