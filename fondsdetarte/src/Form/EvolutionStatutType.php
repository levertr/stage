<?php

namespace App\Form;

use App\Entity\EvolutionStatut;
use App\Form\Type\DatePickerType;
use DateTime;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EvolutionStatutType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('statut', null, [
                'label' => 'Statut* :',
                'data' => $options['statutEnCours'],
                'required' => true,
            ])
            ->add('createdAt', DatePickerType::class, [
                'label' => 'Date du statut* :',
                'data' => $options['dateDuStatutEnCours'],
                'html5' =>  false,
                'required' => false
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => EvolutionStatut::class,
            'statutEnCours' => null,
            'dateDuStatutEnCours' => new \DateTime('now')
        ]);
    }
}
