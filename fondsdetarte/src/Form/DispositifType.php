<?php

namespace App\Form;

use App\Entity\Dispositif;
use App\Entity\Structure;
use App\Services\Organigramme;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\Common\Persistence\ManagerRegistry as Doctrine;
use Symfony\Component\Form\Extension\Core\Type\NumberType;

class DispositifType extends AbstractType
{
    private $service;
    private $em;

    public function __construct(Organigramme $service, Doctrine $doctrine)
    {
        $this->service = $service;
        $this->em = $doctrine->getManager();
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $structure = $this->em->getRepository(Structure::class)->find(355);
        $choices = $this->service->getObjectFlatTree($structure);

        $builder
            ->add('libelle', null, [
                'attr' => ['autofocus' => true],
                'label' => 'Libellé* :',
                'required' => true,
            ])
            ->add('plateforme', ChoiceType::class, [
                'label' => 'Plateforme* :',
                'required' => true,
                'choices' => array(
                    'AEL' => 'AEL',
                    'AI' => 'AI'
                )
            ])
            ->add('etat', ChoiceType::class, [
                'label' => 'Actif* ?',
                'required' => true,
                'choices' => array(
                    'Oui' => 1,
                    'Non' => 0,
                ),
                'label_attr' => ['class' => 'radio-custom radio-inline'],
                'expanded' => true,
                'required' => true,
            ])
            ->add('cp', ChoiceType::class, [
                'label' => 'CP* ?',
                'required' => true,
                'choices' => array(
                    'Oui' => 1,
                    'Non' => 0,
                ),
                'label_attr' => ['class' => 'radio-custom radio-inline'],
                'expanded' => true,
                'required' => true,
            ])
            ->add('description', TextareaType::class, [
                'label' => 'Description :',
                'required' => false,
                'empty_data' => ''
            ])
            ->add('criticite', ChoiceType::class, [
                'label' => 'Criticite* :',
                'required' => true,
                'choices' => array(
                    'Haute' => 'Haute',
                    'Normale' => 'Normale',
                    'Faible' => 'Faible'
                )
            ])
            ->add('complexite', ChoiceType::class, [
                'label' => 'Complexite* :',
                'required' => true,
                'choices' => array(
                    'Complexe' => 'Complexe',
                    'Moyen' => 'Moyen',
                    'Simple' => 'Simple'
                )
            ])
            ->add('versionProd', NumberType::class, [
                'attr' => ['autofocus' => true],
                'label' => 'Version de production* :',
                'invalid_message' => "Veuillez saisir un nombre.",
                'required' => true
            ])
            ->add('eligibleReversibilite', CheckboxType::class, [
                'label' => "Eligible réversibilité",
                'required' => false,
                'help' => 'Ce dispositif devra être repris en cas de réversibilité si cette case est cochée.',
            ])
            ->add('cedre', UrlType::class, [
                'help' => 'www.example.com',
                'attr' => ['autofocus' => true],
                'label' => 'Lien vers Cèdre* :',
                'required' => true,
            ])
            ->add('referentiels', TextareaType::class, [
                'label' => 'Référentiels :',
                'required' => false,
            ])
            ->add('domaineBindings', TextareaType::class, [
                'attr' => ['autofocus' => true],
                'label' => 'Domaines des bindings :',
                'required' => false,
            ])
            ->add('reglesMetier', TextareaType::class, [
                'label' => 'Règles métier :',
                'required' => false,
            ])
            ->add('javascriptPartages', TextareaType::class, [
                'label' => 'JavaScript partagés :',
                'required' => false,
            ])
            ->add('stylesPartages', TextareaType::class, [
                'label' => 'Styles Partagés :',
                'required' => false,
            ])
            ->add('formulairesModeles', TextareaType::class, [
                'label' => 'Formulaires modèles :',
                'required' => false,
            ])
            ->add('structure', null, [
                'label' => 'Direction propriétaire*',
                'attr' => array('class' => 'selectpicker-hdf'),
                'choices' => $choices,
                'choice_label' => 'libelleIndente',
                'choice_name' => 'id',
                'required' => true
            ])
            ->add('srodUtilisateur', null, array(
                'label' => 'Référent SROD',
                'required' => true,
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->select('u')
                        ->where('u.srod = 1')
                        ->orderBy('u.nom', 'asc');
                }
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Dispositif::class,
        ]);
    }
}
