<?php

namespace App\Form;

use App\Entity\Commentaire;
use App\Entity\Structure;
use App\Services\Organigramme;
use Doctrine\Common\Persistence\ManagerRegistry as Doctrine;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class StructureDefaultType extends AbstractType
{
    public function __construct(Organigramme $service, Doctrine $doctrine)
    {
        $this->service = $service;
        $this->em = $doctrine->getManager();
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $structure = $this->em->getRepository(Structure::class)->find(355);
        $choices = $this->service->getObjectFlatTree($structure);

        $builder
            ->add('structure', ChoiceType::class, [
                'label' => 'Choisissez votre structure : ',
                'attr' => array('class' => 'selectpicker-hdf'),
                'choices' => $choices,
                'choice_label' => 'libelleIndente',
                'choice_name' => 'id',
                'choice_value' => 'libelle',
                'required' => true
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
        ]);
    }
}
