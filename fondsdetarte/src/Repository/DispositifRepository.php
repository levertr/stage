<?php

namespace App\Repository;

use App\Entity\Dispositif;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Dispositif|null find($id, $lockMode = null, $lockVersion = null)
 * @method Dispositif|null findOneBy(array $criteria, array $orderBy = null)
 * @method Dispositif[]    findAll()
 * @method Dispositif[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class DispositifRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Dispositif::class);
    }

    public function findByFilter(string $q, int $page, $vue, $input, $structure)
    {
        
        $queryBuilder = $this->createQueryBuilder('d');
        $queryBuilder->join('d.structure', 'r');
        $choix = "aucun";

        switch ($vue) {
            case "vueDirection":
                $queryBuilder
                    ->andWhere('r.libelle = :structure')
                    ->setParameter('structure', $structure);
                break;

            case "vuePlateforme":
                $choix = "plateforme";
                $structure = null;
                $queryBuilder
                    ->andWhere('d.plateforme = :input')
                    ->setParameter('input', $input);
                break;

            case "vueEtat":
                $choix = "etat";
                $structure = null;
                $queryBuilder
                    ->andWhere('d.etat = :input')
                    ->setParameter('input', $input);

                break;

            case "vueCriticite":
                $choix = "criticite";
                $structure = null;
                $queryBuilder->select('d')
                    ->addSelect("(CASE WHEN d.criticite like 'Faible' THEN 0
                        WHEN d.criticite like 'Normale' THEN 1
                        WHEN d.criticite like 'Haute' THEN 2 ELSE 999 END) AS HIDDEN fixed_order")
                    ->andWhere('d.criticite = :input')
                    ->setParameter('input', $input);
                break;

            case "aucune":
                $structure = null;
                $choix = "aucun";
                $queryBuilder
                    ->orderBy('d.libelle');
        }

        if ($q && $structure == null && $choix == "aucun") {
            $queryBuilder
                ->orWhere('d.libelle LIKE :q')
                ->orWhere('d.versionProd LIKE :q')
                ->orWhere('d.cedre LIKE :q')
                ->orWhere('d.referentiels LIKE :q')
                ->orWhere('d.domaineBindings LIKE :q')
                ->orWhere('d.reglesMetier LIKE :q')
                ->orWhere('d.javascriptPartages LIKE :q')
                ->orWhere('d.stylesPartages LIKE :q')
                ->orWhere('d.formulairesModeles LIKE :q')
                ->setParameter('q', "%$q%");
        }

        if ($q && $structure != null) {
            $queryBuilder
                ->orWhere('r.libelle LIKE :structure AND ( d.libelle LIKE :q
                           OR d.versionProd LIKE :q OR d.cedre LIKE :q
                           OR d.referentiels LIKE :q OR d.domaineBindings LIKE :q
                           OR d.reglesMetier LIKE :q OR d.javascriptPartages LIKE :q
                           OR d.stylesPartages LIKE :q OR d.formulairesModeles LIKE :q )')
                ->setParameter('q', "%". $q. "%")
                ->setParameter('structure', "%". $structure. "%");
        }

        if ($q && $choix != "aucun") {
            switch ($choix) {
                case "plateforme":
                    $queryBuilder
                        ->orWhere('d.plateforme LIKE :input AND ( d.libelle LIKE :q
                           OR d.versionProd LIKE :q OR d.cedre LIKE :q
                           OR d.referentiels LIKE :q OR d.domaineBindings LIKE :q
                           OR d.reglesMetier LIKE :q OR d.javascriptPartages LIKE :q
                           OR d.stylesPartages LIKE :q OR d.formulairesModeles LIKE :q )')
                        ->setParameter('q', "%". $q. "%")
                        ->setParameter('input', "%". $input. "%");
                break;

                case "etat":
                    $queryBuilder
                        ->orWhere('d.etat LIKE :input AND ( d.libelle LIKE :q
                           OR d.versionProd LIKE :q OR d.cedre LIKE :q
                           OR d.referentiels LIKE :q OR d.domaineBindings LIKE :q
                           OR d.reglesMetier LIKE :q OR d.javascriptPartages LIKE :q
                           OR d.stylesPartages LIKE :q OR d.formulairesModeles LIKE :q )')
                        ->setParameter('q', "%". $q. "%")
                        ->setParameter('input', "%$input%");
                break;

                case "criticite":
                    $queryBuilder
                        ->orWhere('d.criticite LIKE :input AND ( d.libelle LIKE :q
                           OR d.versionProd LIKE :q OR d.cedre LIKE :q
                           OR d.referentiels LIKE :q OR d.domaineBindings LIKE :q
                           OR d.reglesMetier LIKE :q OR d.javascriptPartages LIKE :q
                           OR d.stylesPartages LIKE :q OR d.formulairesModeles LIKE :q )')
                        ->setParameter('q', "%". $q. "%")
                        ->setParameter('input', "%$input%");
                break;
            }
        }
        return $queryBuilder->getQuery();
    }
}
