<?php

namespace App\Repository;

use App\Entity\Commentaire;
use App\Entity\Dispositif;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Commentaire|null find($id, $lockMode = null, $lockVersion = null)
 * @method Commentaire|null findOneBy(array $criteria, array $orderBy = null)
 * @method Commentaire[]    findAll()
 * @method Commentaire[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CommentaireRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Commentaire::class);
    }

     /**
      * @return Commentaire[] Returns an array of Commentaire objects
      */
    public function findByField($value, string $q, int $page)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.contenu = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }

    public function findByFilter(Dispositif $dispositif, string $q, int $page = 1)
    {
        $queryBuilder = $this
            ->createQueryBuilder('c')
            ->join('c.utilisateur', 'u')
            ->join('c.evolution', 'e')
            ->leftJoin('c.dispositif', 'd')
            ->where('c.dispositif = :dispositif')
            ->setParameter('dispositif', $dispositif)
            ->orderBy('u.createdAt')
        ;

        if ($q) {
            $queryBuilder
                ->orWhere('c.createdAt LIKE :q')
                ->orWhere('u.prenom LIKE :q')
                ->orWhere('u.login LIKE :q')
                ->orWhere('c.contenu LIKE :q')
                ->setParameter('q', "%$q%");
        }

        return $queryBuilder->getQuery();
    }
    

    /*
    public function findOneBySomeField($value): ?Commentaire
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
