<?php

namespace App\Repository;

use App\Entity\EvolutionStatut;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method EvolutionStatut|null find($id, $lockMode = null, $lockVersion = null)
 * @method EvolutionStatut|null findOneBy(array $criteria, array $orderBy = null)
 * @method EvolutionStatut[]    findAll()
 * @method EvolutionStatut[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EvolutionStatutRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, EvolutionStatut::class);
    }

     /**
      * @return EvolutionStatut[] Returns an array of EvolutionStatut objects
      */
    
    public function findOneByFilter($evolution, string $q, int $page = 1, $vue, $input)
    {
        $queryBuilder = $this
            ->createQueryBuilder('eS')
            ->join('eS.evolution', 'evo')
            ->leftJoin('eS.utilisateur', 'u')
            ->leftJoin('eS.statut', 's')
            ->where('eS.evolution = :evolution')
            ->setParameter('evolution', $evolution)
            ->orderBy('eS.createdAt', 'desc')
        ;

        return $queryBuilder->getQuery();
    }

    

    /*
    public function findOneBySomeField($value): ?EvolutionStatut
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
