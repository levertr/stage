<?php

namespace App\Repository;

use App\Entity\Utilisateur;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use Pagerfanta\Adapter\DoctrineORMAdapter;


/**
 * UtilisateurRepository
 */
class UtilisateurRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Utilisateur::class);
    }

    public function findByFilter(string $q, int $page = 1)
    {
        $queryBuilder = $this
            ->createQueryBuilder('u')
            ->join('u.structure', 'r')
            ->orderBy('u.nom')
            ->addOrderBy('u.prenom')
        ;

        if ($q) {
            $queryBuilder
                ->orWhere('u.nom LIKE :q')
                ->orWhere('u.prenom LIKE :q')
                ->orWhere('u.role LIKE :q')
                ->orWhere('r.libelle LIKE :q')
                ->orWhere('u.login LIKE :q')
                ->orWhere('u.email LIKE :q')
                ->orWhere('u.telephone LIKE :q')
                ->setParameter('q', "%$q%");
        }

        return $queryBuilder->getQuery();
    }
}