<?php

namespace App\Repository;

use App\Entity\Dispositif;
use App\Entity\Evolution;
use App\Entity\EvolutionStatut;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\Tools\Pagination\Paginator;
use Doctrine\Persistence\ManagerRegistry;
use PDO;

/**
 * @method Evolution|null find($id, $lockMode = null, $lockVersion = null)
 * @method Evolution|null findOneBy(array $criteria, array $orderBy = null)
 * @method Evolution[]    findAll()
 * @method Evolution[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EvolutionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Evolution::class);
    }

    public function findByFilter(Dispositif $dispositif, string $q = '', int $page = 1, $vue = '', $input = '')
    {
        $queryBuilder = $this
            ->createQueryBuilder('e')
            ->where('e.dispositif = :dispositif')
            ->setParameter('dispositif', $dispositif)
            ->join('e.evolutionStatuts', 'evoStatuts')
            ->join('evoStatuts.statut', 'statut')
            ->leftJoin('e.dispositif', 'dis')
            ->leftJoin('e.dsiUtilisateur', 'd')
            ->leftJoin('e.srodUtilisateur', 's')
            ->orderBy('e.version', 'desc')
        ;

        if ($vue == "aucune") {
            $queryBuilder;
        }
        
        if ($vue == "vueStatut") {
            $queryBuilder
                ->andWhere('statut.libelle = :input')
                ->setParameter('input', $input)
                ->getQuery();
        }
        
        if ($vue == "vuePriorite") {
        $queryBuilder
            ->getQuery();
        }

        if ($q) {
            $queryBuilder
                ->andWhere('e.libelle LIKE :q OR e.version LIKE :q 
                            OR dis.plateforme LIKE :q OR e.priorite LIKE :q 
                            OR e.priorite LIKE :q OR e.complexite LIKE :q 
                            OR s.nom LIKE :q OR s.prenom LIKE :q 
                            OR d.nom LIKE :q OR d.prenom LIKE :q')
                ->setParameter('q', "%$q%")
                ->getQuery();
        }
        
        return $queryBuilder;
    }

    public function findByAllEvolutions(string $q, int $page = 1, $vue, $input)
    {
        $em = $this->getEntityManager();

        $requeteSQL =  "SELECT e0_.id as id, e0_.libelle as libelle, e0_.version as version, d0_.plateforme as dispositif_plateforme, d0_.id as dispositif_id,
        d0_.libelle as dispositif_libelle, s2_.libelle as statut, e0_.priorite, u0_.nom as srodUtilisateur_nom,  u0_.prenom as srodUtilisateur_prenom, 
        u1_.nom as dsiUtilisateur_nom, u1_.prenom as dsiUtilisateur_prenom

        FROM evolution e0_

        INNER JOIN evolution_statut e1_ ON e0_.id = e1_.evolution_id

        INNER JOIN statut s2_ ON e1_.statut_id = s2_.id

        INNER JOIN dispositif d0_ ON e0_.dispositif_id = d0_.id

        INNER JOIN utilisateur u0_ ON e0_.srod_utilisateur_id = u0_.id

        INNER JOIN utilisateur u1_ ON e0_.dsi_utilisateur_id = u1_.id

        WHERE s2_.libelle = (SELECT s3_.libelle

                            FROM evolution e4_, statut s3_

                            INNER JOIN evolution_statut e5_ ON s3_.id = e5_.statut_id

                            INNER JOIN evolution e6_ ON e5_.evolution_id = e6_.id

                            WHERE e6_.id = e0_.id

                            ORDER BY e5_.created_at DESC, e1_.created_at desc

                            LIMIT 1)

        AND s2_.libelle = :input

        GROUP BY e0_.id, e0_.created_at, e0_.updated_at, e0_.libelle, e0_.version, e0_.priorite, e0_.complexite, 
        e0_.ouverture_production, e0_.livraison_recette_direction, e0_.livraison_recette_srod, 
        e0_.dispositif_id, e0_.srod_utilisateur_id, e0_.dsi_utilisateur_id, e0_.bowi_utilisateur_id;";

        $queryBuilder = $this
            ->createQueryBuilder('e')
            ->leftJoin('e.evolutionStatuts', 'evoStatut')
            ->leftJoin('evoStatut.statut', 'statut')
            ->join('e.dispositif', 'dis')
            ->leftJoin('e.dsiUtilisateur', 'd')
            ->leftJoin('e.srodUtilisateur', 's')
            ->orderBy('e.version', 'desc')
        ;

        switch ($vue) {
            case "aucune":
                $queryBuilder;
            break;

            case "vueStatut":
                $query = $em->getConnection()->prepare($requeteSQL);
                $query->bindValue('input', $input);
                $query->execute();
    
                return $query->fetchAll();
            break;
        }

        if ($q && $input == null) {
            $queryBuilder
                ->orWhere('e.libelle LIKE :q')
                ->orWhere('e.version LIKE :q')
                ->orWhere('dis.libelle LIKE :q')
                ->orWhere('dis.plateforme LIKE :q')
                ->orWhere('e.priorite LIKE :q')
                ->orWhere('e.complexite LIKE :q')
                ->orWhere('s.nom LIKE :q')->orWhere('s.prenom LIKE :q')
                ->orWhere('d.nom LIKE :q')->orWhere('d.prenom LIKE :q')
                ->setParameter('q', "%$q%");
        }

        if ($q && $input != null) {
            $queryBuilder
                ->orWhere('statut.libelle LIKE :input AND ( d.libelle LIKE :q
                           OR d.versionProd LIKE :q OR d.cedre LIKE :q
                           OR d.referentiels LIKE :q OR d.domaineBindings LIKE :q
                           OR d.reglesMetier LIKE :q OR d.javascriptPartages LIKE :q
                           OR d.stylesPartages LIKE :q OR d.formulairesModeles LIKE :q )')
                ->setParameter('q', "%". $q. "%");
        }

        return $queryBuilder->getQuery();
    }
}
