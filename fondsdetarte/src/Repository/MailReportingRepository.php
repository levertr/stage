<?php

namespace App\Repository;

use App\Entity\MailReporting;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method MailReporting|null find($id, $lockMode = null, $lockVersion = null)
 * @method MailReporting|null findOneBy(array $criteria, array $orderBy = null)
 * @method MailReporting[]    findAll()
 * @method MailReporting[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MailReportingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MailReporting::class);
    }

    // /**
    //  * @return MailReporting[] Returns an array of MailReporting objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('m.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?MailReporting
    {
        return $this->createQueryBuilder('m')
            ->andWhere('m.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
