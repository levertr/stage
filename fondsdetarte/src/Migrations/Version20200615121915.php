<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200615121915 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE evolution_statut CHANGE evolution_id evolution_id INT UNSIGNED DEFAULT NULL, CHANGE statut_id statut_id INT UNSIGNED DEFAULT NULL, CHANGE utilisateur_id utilisateur_id INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE acces CHANGE utilisateur_id utilisateur_id INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE structure CHANGE pere_id pere_id INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE commentaire CHANGE utilisateur_id utilisateur_id INT UNSIGNED DEFAULT NULL, CHANGE evolution_id evolution_id INT UNSIGNED DEFAULT NULL, CHANGE dispositif_id dispositif_id INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE utilisateur DROP login, DROP token, DROP token_date, CHANGE updated_at updated_at DATETIME DEFAULT NULL, CHANGE telephone telephone VARCHAR(15) DEFAULT NULL');
        $this->addSql('ALTER TABLE evolution CHANGE dispositif_id dispositif_id INT UNSIGNED DEFAULT NULL, CHANGE srod_utilisateur_id srod_utilisateur_id INT UNSIGNED DEFAULT NULL, CHANGE dsi_utilisateur_id dsi_utilisateur_id INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE dispositif CHANGE structure_id structure_id INT UNSIGNED DEFAULT NULL, CHANGE srod_utilisateur_id srod_utilisateur_id INT UNSIGNED DEFAULT NULL, CHANGE etat etat TINYINT(1) NOT NULL, CHANGE eligible_reversibilite eligible_reversibilite TINYINT(1) DEFAULT \'NULL\'');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE acces CHANGE utilisateur_id utilisateur_id INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE commentaire CHANGE dispositif_id dispositif_id INT UNSIGNED DEFAULT NULL, CHANGE evolution_id evolution_id INT UNSIGNED DEFAULT NULL, CHANGE utilisateur_id utilisateur_id INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE dispositif CHANGE structure_id structure_id INT UNSIGNED NOT NULL, CHANGE srod_utilisateur_id srod_utilisateur_id INT UNSIGNED NOT NULL, CHANGE etat etat TINYINT(1) NOT NULL, CHANGE eligible_reversibilite eligible_reversibilite TINYINT(1) DEFAULT \'0\' NOT NULL');
        $this->addSql('ALTER TABLE evolution CHANGE dispositif_id dispositif_id INT UNSIGNED DEFAULT NULL, CHANGE srod_utilisateur_id srod_utilisateur_id INT UNSIGNED DEFAULT NULL, CHANGE dsi_utilisateur_id dsi_utilisateur_id INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE evolution_statut CHANGE evolution_id evolution_id INT UNSIGNED DEFAULT NULL, CHANGE statut_id statut_id INT UNSIGNED DEFAULT NULL, CHANGE utilisateur_id utilisateur_id INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE structure CHANGE pere_id pere_id INT UNSIGNED DEFAULT NULL');
        $this->addSql('ALTER TABLE utilisateur ADD login VARCHAR(45) CHARACTER SET utf8 NOT NULL COLLATE `utf8_general_ci`, ADD token VARCHAR(255) CHARACTER SET utf8 DEFAULT \'NULL\' COLLATE `utf8_general_ci`, ADD token_date DATE DEFAULT \'NULL\', CHANGE updated_at updated_at DATETIME DEFAULT \'NULL\', CHANGE telephone telephone VARCHAR(15) CHARACTER SET utf8 DEFAULT \'NULL\' COLLATE `utf8_general_ci`');
    }
}
