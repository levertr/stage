<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\EquatableInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use App\Entity\Structure;

/**
 * App\Entity\Utilisateur
 *
 * @ORM\Table(name="utilisateur")
 * @ORM\Entity(repositoryClass="App\Repository\UtilisateurRepository")
 * @UniqueEntity("login", message="Identifiant déjà utilisé")
 */
class Utilisateur implements UserInterface, \Serializable
{
    const NUM_ITEMS = 15;

    /**
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @Assert\NotBlank(message="Champ obligatoire")
     * @ORM\Column(type="string", length=3, nullable=false)
     */
    private $civilite;

    /**
     * @Assert\NotBlank(message="Champ obligatoire")
     * @ORM\Column(name="nom", type="string", length=64, nullable=false)
     */
    private $nom;

    /**
     * @Assert\NotBlank(message="Champ obligatoire")
     * @ORM\Column(name="prenom", type="string", length=32, nullable=false)
     */
    private $prenom;

    /**
     * @Assert\NotBlank(message="Champ obligatoire")
     * @ORM\Column(name="login", type="string", length=16, nullable=false)
     */
    private $login;

    /**
     * @Assert\NotBlank(message="Champ obligatoire")
     * @Assert\Email(
     *     message = "'{{ value }}' n'est pas un email valide."
     * )
     * @ORM\Column(name="email", type="string", length=128, nullable=false)
     */
    private $email;

    /**
     * @Assert\Type(type="digit", message="Numéro non valide")
     * @ORM\Column(name="telephone", type="string", length=15, nullable=true)
     */
    private $telephone;

    /**
     * @Assert\NotBlank(message="Champ obligatoire")
     * @ORM\Column(name="role", type="string", nullable=false)
     */
    private $role = 'Aucun droit';

    /**
     * @var bool|null
     *
     * @ORM\Column(name="actif", type="boolean", nullable=false)
     */
    private $actif = true;

    /**
     * @ORM\Column(name="referent_dsi", type="boolean", nullable=true)
     */
    private $referent_dsi;

    /**
     * @ORM\Column(name="srod", type="boolean", nullable=true)
     * @Assert\Expression(
     *     "not (this.getreferentDsi() && this.getSrod())",
     *     message="Vous ne pouvez pas être du DSI et SROD à la fois."
     * )
     */
    private $srod;

    /**
     * @ORM\Column(name="bowi", type="boolean", nullable=true)
     * @Assert\Expression(
     *     "not (this.getreferentDsi() == 0 && this.getBowi())",
     *     message="Il faut être du DSI pour être intervenant BOWI."
     * )
     */
    private $bowi;

    /**
     * @ORM\OneToMany(targetEntity="Acces", mappedBy="utilisateur")
     * @ORM\OrderBy({"date" = "DESC"})
     */
    private $access;
    
    /**
     * @ORM\ManyToOne(targetEntity="Structure")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="structure_id", referencedColumnName="id")
     * })
     */    
    private $structure;

    public function __construct()
    {
        $this->access = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->nom . ' ' . $this->prenom;
    }

    /**
     * Returns the roles granted to the user.
     *
     * @return Role[] The user roles
     */
    public function getRoles()
    {
        $roles = array();

        if ($this->role == 'Lecteur') {
            $roles[] = 'ROLE_LECTEUR';
            $roles[] = 'ROLE_USER';
        } else if ($this->role == 'Contributeur') {
            $roles[] = 'ROLE_CONTRIBUTEUR';
            $roles[] = 'ROLE_LECTEUR';
            $roles[] = 'ROLE_USER';
        } else if ($this->role == 'Administrateur') {
            $roles[] = 'ROLE_ADMIN';
            $roles[] = 'ROLE_CONTRIBUTEUR';
            $roles[] = 'ROLE_LECTEUR';
            $roles[] = 'ROLE_USER';
        } else if ($this->role == 'Super administrateur') {
            $roles[] = 'ROLE_SUPER_ADMIN';
            $roles[] = 'ROLE_ALLOWED_TO_SWITCH';
            $roles[] = 'ROLE_ADMIN';
            $roles[] = 'ROLE_CONTRIBUTEUR';
            $roles[] = 'ROLE_LECTEUR';
            $roles[] = 'ROLE_USER';
        }

        return $roles;
    }

    /**
     * Returns the password used to authenticate the user.
     *
     * @return string The password
     */
    public function getPassword()
    {}

    /**
     * Returns the salt.
     *
     * @return string The salt
     */
    public function getSalt()
    {}

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername()
    {
        return $this->login;
    }

    /**
     * Removes sensitive data from the user.
     *
     * @return void
     */
    public function eraseCredentials()
    {}

    /**
     * The equality comparison should neither be done by referential equality
     * nor by comparing identities (i.e. getId() === getId()).
     *
     * However, you do not need to compare every attribute, but only those that
     * are relevant for assessing whether re-authentication is required.
     *
     * @param UserInterface $user
     */
    public function isEqualTo(UserInterface $user)
    {}

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->login,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->login,
            ) = unserialize($serialized, array('allowed_classes' => false));
    }

    public function setNom(string $nom): self
    {
        $this->nom = mb_strtoupper($nom);

        return $this;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = ucfirst($prenom);

        return $this;
    }


    /*** SUPPRIMER A PARTIR D'ICI ***/


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getCivilite(): ?string
    {
        return $this->civilite;
    }

    public function setCivilite(string $civilite): self
    {
        $this->civilite = $civilite;

        return $this;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function getLogin(): ?string
    {
        return $this->login;
    }

    public function setLogin(string $login): self
    {
        $this->login = $login;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(?string $telephone): self
    {
        $this->telephone = $telephone;

        return $this;
    }

    public function getRole(): ?string
    {
        return $this->role;
    }

    public function setRole(string $role): self
    {
        $this->role = $role;

        return $this;
    }

    public function getActif(): ?bool
    {
        return $this->actif;
    }

    public function setActif(bool $actif): self
    {
        $this->actif = $actif;

        return $this;
    }

    /**
     * @return Collection|Acces[]
     */
    public function getAccess(): Collection
    {
        return $this->access;
    }

    public function addAccess(Acces $access): self
    {
        if (!$this->access->contains($access)) {
            $this->access[] = $access;
            $access->setUtilisateur($this);
        }

        return $this;
    }

    public function removeAccess(Acces $access): self
    {
        if ($this->access->contains($access)) {
            $this->access->removeElement($access);
            // set the owning side to null (unless already changed)
            if ($access->getUtilisateur() === $this) {
                $access->setUtilisateur(null);
            }
        }

        return $this;
    }

    public function getreferent_dsi()
    {
        return $this->referent_dsi;
    }

    public function getReferentDsi()
    {
        return $this->referent_dsi;
    }

    public function setreferent_dsi(bool $referent_dsi)
    {
        $this->referent_dsi = $referent_dsi;
    }

    public function setReferentDsi(bool $referent_dsi)
    {
        $this->referent_dsi = $referent_dsi;
    }

    public function getSrod()
    {
        return $this->srod;
    }

    public function setSrod(int $srod)
    {
        $this->srod = $srod;
    }

    public function getBowi()
    {
        return $this->bowi;
    }

    public function setBowi(int $bowi)
    {
        $this->bowi = $bowi;
    }

    public function getStructure()
    {
        return $this->structure;
    }

    public function setStructure(?Structure $structure)
    {
        $this->structure = $structure;
    }
}