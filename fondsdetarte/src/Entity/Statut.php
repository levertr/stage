<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Statut
 *
 * @ORM\Table(name="statut")
 * @ORM\Entity(repositoryClass="App\Repository\StatutRepository")
 */
class Statut
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="libelle", type="string", length=64, nullable=true, options={"default"="NULL"})
     */
    public $libelle;

    /**
     * @ORM\OneToMany(targetEntity="EvolutionStatut", mappedBy="statut")
     * @ORM\OrderBy({"createdAt" = "desc"})
     */
    private $evolutionStatuts;

    public function __construct()
    {
        $this->evolutionStatuts = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->libelle;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(?string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getEvolutionStatuts(): Collection
    {
        return $this->evolutionStatuts;
    }

    public function addEvolutionStatut(EvolutionStatut $evolutionStatut): self
    {
        if (!$this->evolutionStatuts->contains($evolutionStatut)) {
            $this->evolutionStatuts[] = $evolutionStatut;
            $evolutionStatut->setStatut($this);
        }

        return $this;
    }

    public function removeEvolutionStatut(EvolutionStatut $evolutionStatut): self
    {
        if ($this->evolutionStatuts->contains($evolutionStatut)) {
            $this->evolutionStatuts->removeElement($evolutionStatut);
            // set the owning side to null (unless already changed)
            if ($evolutionStatut->getStatut() === $this) {
                $evolutionStatut->setStatut(null);
            }
        }

        return $this;
    }

}