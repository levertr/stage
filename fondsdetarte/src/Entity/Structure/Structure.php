<?php

namespace App\Entity\Structure;

use Doctrine\ORM\Mapping as ORM;

/**
 * Structure
 *
 * @ORM\Table(name="structure", indexes={@ORM\Index(name="pere_structure_id", columns={"pere_structure_id"}), @ORM\Index(name="structure_type_id", columns={"structure_type_id"})})
 * @ORM\Entity
 */
class Structure
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="type", type="string", length=0, nullable=true, options={"default"="NULL"})
     */
    private $type = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="departement", type="string", length=2, nullable=true, options={"default"="NULL"})
     */
    private $departement = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="ville", type="string", length=128, nullable=true, options={"default"="NULL"})
     */
    private $ville = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=16, nullable=false)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=128, nullable=false)
     */
    private $libelle;

    /**
     * @var bool
     *
     * @ORM\Column(name="actif", type="boolean", nullable=false, options={"default"="1"})
     */
    private $actif = true;

    /**
     * @var \StructureType
     *
     * @ORM\ManyToOne(targetEntity="StructureType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="structure_type_id", referencedColumnName="id")
     * })
     */
    private $structureType;

    /**
     * @var \Structure
     *
     * @ORM\ManyToOne(targetEntity="Structure")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pere_structure_id", referencedColumnName="id")
     * })
     */
    private $pereStructure;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(?string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getDepartement(): ?string
    {
        return $this->departement;
    }

    public function setDepartement(?string $departement): self
    {
        $this->departement = $departement;

        return $this;
    }

    public function getVille(): ?string
    {
        return $this->ville;
    }

    public function setVille(?string $ville): self
    {
        $this->ville = $ville;

        return $this;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getActif(): ?bool
    {
        return $this->actif;
    }

    public function setActif(bool $actif): self
    {
        $this->actif = $actif;

        return $this;
    }

    public function getStructureType(): ?StructureType
    {
        return $this->structureType;
    }

    public function setStructureType(?StructureType $structureType): self
    {
        $this->structureType = $structureType;

        return $this;
    }

    public function getPereStructure(): ?self
    {
        return $this->pereStructure;
    }

    public function setPereStructure(?self $pereStructure): self
    {
        $this->pereStructure = $pereStructure;

        return $this;
    }


}
