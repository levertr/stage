<?php

namespace App\Entity\Structure;

use Doctrine\ORM\Mapping as ORM;

/**
 * Evolution
 *
 * @ORM\Table(name="evolution", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"})}, indexes={@ORM\Index(name="fk_evolution_utilisateur1_idx", columns={"srod_utilisateur_id"}), @ORM\Index(name="dispositif_UNIQUE", columns={"dispositif_id"}), @ORM\Index(name="fk_statut_bowi_idx", columns={"id"}), @ORM\Index(name="fk_evolution_utilisateur2_idx", columns={"dsi_utilisateur_id"}), @ORM\Index(name="version_UNIQUE", columns={"version"}), @ORM\Index(name="fk_statut_bowi", columns={"statut_bowi_id"}), @ORM\Index(name="statut_id", columns={"statut_id"}), @ORM\Index(name="fk_evolution_utilisateur3_idx", columns={"bowi_utilisateur_id"})})
 * @ORM\Entity
 */
class Evolution
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true, options={"default"="NULL"})
     */
    private $updatedAt = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=128, nullable=false)
     */
    private $libelle;

    /**
     * @var string
     *
     * @ORM\Column(name="version", type="string", length=255, nullable=false)
     */
    private $version;

    /**
     * @var string
     *
     * @ORM\Column(name="priorite", type="string", length=0, nullable=false)
     */
    private $priorite;

    /**
     * @var string
     *
     * @ORM\Column(name="complexite", type="string", length=0, nullable=false)
     */
    private $complexite;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="ouverture_production", type="datetime", nullable=true, options={"default"="NULL"})
     */
    private $ouvertureProduction = 'NULL';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="livraison_recette_direction", type="datetime", nullable=true, options={"default"="NULL"})
     */
    private $livraisonRecetteDirection = 'NULL';

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="livraison_recette_srod", type="datetime", nullable=true, options={"default"="NULL"})
     */
    private $livraisonRecetteSrod = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="donnee_concernee", type="string", length=45, nullable=true, options={"default"="NULL"})
     */
    private $donneeConcernee = 'NULL';

    /**
     * @var \Dispositif
     *
     * @ORM\ManyToOne(targetEntity="Dispositif")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="dispositif_id", referencedColumnName="id")
     * })
     */
    private $dispositif;

    /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="srod_utilisateur_id", referencedColumnName="id")
     * })
     */
    private $srodUtilisateur;

    /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="dsi_utilisateur_id", referencedColumnName="id")
     * })
     */
    private $dsiUtilisateur;

    /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="bowi_utilisateur_id", referencedColumnName="id")
     * })
     */
    private $bowiUtilisateur;

    /**
     * @var \Statut
     *
     * @ORM\ManyToOne(targetEntity="Statut")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="statut_bowi_id", referencedColumnName="id")
     * })
     */
    private $statutBowi;

    /**
     * @var \Statut
     *
     * @ORM\ManyToOne(targetEntity="Statut")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="statut_id", referencedColumnName="id")
     * })
     */
    private $statut;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getVersion(): ?string
    {
        return $this->version;
    }

    public function setVersion(string $version): self
    {
        $this->version = $version;

        return $this;
    }

    public function getPriorite(): ?string
    {
        return $this->priorite;
    }

    public function setPriorite(string $priorite): self
    {
        $this->priorite = $priorite;

        return $this;
    }

    public function getComplexite(): ?string
    {
        return $this->complexite;
    }

    public function setComplexite(string $complexite): self
    {
        $this->complexite = $complexite;

        return $this;
    }

    public function getOuvertureProduction(): ?\DateTimeInterface
    {
        return $this->ouvertureProduction;
    }

    public function setOuvertureProduction(?\DateTimeInterface $ouvertureProduction): self
    {
        $this->ouvertureProduction = $ouvertureProduction;

        return $this;
    }

    public function getLivraisonRecetteDirection(): ?\DateTimeInterface
    {
        return $this->livraisonRecetteDirection;
    }

    public function setLivraisonRecetteDirection(?\DateTimeInterface $livraisonRecetteDirection): self
    {
        $this->livraisonRecetteDirection = $livraisonRecetteDirection;

        return $this;
    }

    public function getLivraisonRecetteSrod(): ?\DateTimeInterface
    {
        return $this->livraisonRecetteSrod;
    }

    public function setLivraisonRecetteSrod(?\DateTimeInterface $livraisonRecetteSrod): self
    {
        $this->livraisonRecetteSrod = $livraisonRecetteSrod;

        return $this;
    }

    public function getDonneeConcernee(): ?string
    {
        return $this->donneeConcernee;
    }

    public function setDonneeConcernee(?string $donneeConcernee): self
    {
        $this->donneeConcernee = $donneeConcernee;

        return $this;
    }

    public function getDispositif(): ?Dispositif
    {
        return $this->dispositif;
    }

    public function setDispositif(?Dispositif $dispositif): self
    {
        $this->dispositif = $dispositif;

        return $this;
    }

    public function getSrodUtilisateur(): ?Utilisateur
    {
        return $this->srodUtilisateur;
    }

    public function setSrodUtilisateur(?Utilisateur $srodUtilisateur): self
    {
        $this->srodUtilisateur = $srodUtilisateur;

        return $this;
    }

    public function getDsiUtilisateur(): ?Utilisateur
    {
        return $this->dsiUtilisateur;
    }

    public function setDsiUtilisateur(?Utilisateur $dsiUtilisateur): self
    {
        $this->dsiUtilisateur = $dsiUtilisateur;

        return $this;
    }

    public function getBowiUtilisateur(): ?Utilisateur
    {
        return $this->bowiUtilisateur;
    }

    public function setBowiUtilisateur(?Utilisateur $bowiUtilisateur): self
    {
        $this->bowiUtilisateur = $bowiUtilisateur;

        return $this;
    }

    public function getStatutBowi(): ?Statut
    {
        return $this->statutBowi;
    }

    public function setStatutBowi(?Statut $statutBowi): self
    {
        $this->statutBowi = $statutBowi;

        return $this;
    }

    public function getStatut(): ?Statut
    {
        return $this->statut;
    }

    public function setStatut(?Statut $statut): self
    {
        $this->statut = $statut;

        return $this;
    }


}
