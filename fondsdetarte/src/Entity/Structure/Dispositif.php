<?php

namespace App\Entity\Structure;

use Doctrine\ORM\Mapping as ORM;

/**
 * Dispositif
 *
 * @ORM\Table(name="dispositif", indexes={@ORM\Index(name="fk_dispositif_utilisateur1_idx", columns={"srod_utilisateur_id"}), @ORM\Index(name="fk_dispositif_structure1_idx", columns={"structure_id"})})
 * @ORM\Entity
 */
class Dispositif
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true, options={"default"="NULL"})
     */
    private $updatedAt = 'NULL';

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=128, nullable=false)
     */
    private $libelle;

    /**
     * @var string
     *
     * @ORM\Column(name="plateforme", type="string", length=0, nullable=false)
     */
    private $plateforme;

    /**
     * @var bool
     *
     * @ORM\Column(name="etat", type="boolean", nullable=false)
     */
    private $etat;

    /**
     * @var string
     *
     * @ORM\Column(name="criticite", type="string", length=0, nullable=false)
     */
    private $criticite;

    /**
     * @var string
     *
     * @ORM\Column(name="complexite", type="string", length=0, nullable=false)
     */
    private $complexite;

    /**
     * @var string
     *
     * @ORM\Column(name="version_prod", type="string", length=45, nullable=false)
     */
    private $versionProd;

    /**
     * @var bool
     *
     * @ORM\Column(name="eligible_reversibilite", type="boolean", nullable=false)
     */
    private $eligibleReversibilite = '0';

    /**
     * @var string|null
     *
     * @ORM\Column(name="cedre", type="string", length=255, nullable=true, options={"default"="NULL"})
     */
    private $cedre = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="referentiels", type="text", length=0, nullable=true, options={"default"="NULL"})
     */
    private $referentiels = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="domaine_bindings", type="text", length=0, nullable=true, options={"default"="NULL"})
     */
    private $domaineBindings = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="regles_metier", type="text", length=0, nullable=true, options={"default"="NULL"})
     */
    private $reglesMetier = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="javascript_partages", type="text", length=0, nullable=true, options={"default"="NULL"})
     */
    private $javascriptPartages = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="styles_partages", type="text", length=0, nullable=true, options={"default"="NULL"})
     */
    private $stylesPartages = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="formulaires_modeles", type="text", length=0, nullable=true, options={"default"="NULL"})
     */
    private $formulairesModeles = 'NULL';

    /**
     * @var string|null
     *
     * @ORM\Column(name="donnee_concernee", type="string", length=60, nullable=true, options={"default"="NULL"})
     */
    private $donneeConcernee = 'NULL';

    /**
     * @var \Structure
     *
     * @ORM\ManyToOne(targetEntity="Structure")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="structure_id", referencedColumnName="id")
     * })
     */
    private $structure;

    /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="srod_utilisateur_id", referencedColumnName="id")
     * })
     */
    private $srodUtilisateur;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getPlateforme(): ?string
    {
        return $this->plateforme;
    }

    public function setPlateforme(string $plateforme): self
    {
        $this->plateforme = $plateforme;

        return $this;
    }

    public function getEtat(): ?bool
    {
        return $this->etat;
    }

    public function setEtat(bool $etat): self
    {
        $this->etat = $etat;

        return $this;
    }

    public function getCriticite(): ?string
    {
        return $this->criticite;
    }

    public function setCriticite(string $criticite): self
    {
        $this->criticite = $criticite;

        return $this;
    }

    public function getComplexite(): ?string
    {
        return $this->complexite;
    }

    public function setComplexite(string $complexite): self
    {
        $this->complexite = $complexite;

        return $this;
    }

    public function getVersionProd(): ?string
    {
        return $this->versionProd;
    }

    public function setVersionProd(string $versionProd): self
    {
        $this->versionProd = $versionProd;

        return $this;
    }

    public function getEligibleReversibilite(): ?bool
    {
        return $this->eligibleReversibilite;
    }

    public function setEligibleReversibilite(bool $eligibleReversibilite): self
    {
        $this->eligibleReversibilite = $eligibleReversibilite;

        return $this;
    }

    public function getCedre(): ?string
    {
        return $this->cedre;
    }

    public function setCedre(?string $cedre): self
    {
        $this->cedre = $cedre;

        return $this;
    }

    public function getReferentiels(): ?string
    {
        return $this->referentiels;
    }

    public function setReferentiels(?string $referentiels): self
    {
        $this->referentiels = $referentiels;

        return $this;
    }

    public function getDomaineBindings(): ?string
    {
        return $this->domaineBindings;
    }

    public function setDomaineBindings(?string $domaineBindings): self
    {
        $this->domaineBindings = $domaineBindings;

        return $this;
    }

    public function getReglesMetier(): ?string
    {
        return $this->reglesMetier;
    }

    public function setReglesMetier(?string $reglesMetier): self
    {
        $this->reglesMetier = $reglesMetier;

        return $this;
    }

    public function getJavascriptPartages(): ?string
    {
        return $this->javascriptPartages;
    }

    public function setJavascriptPartages(?string $javascriptPartages): self
    {
        $this->javascriptPartages = $javascriptPartages;

        return $this;
    }

    public function getStylesPartages(): ?string
    {
        return $this->stylesPartages;
    }

    public function setStylesPartages(?string $stylesPartages): self
    {
        $this->stylesPartages = $stylesPartages;

        return $this;
    }

    public function getFormulairesModeles(): ?string
    {
        return $this->formulairesModeles;
    }

    public function setFormulairesModeles(?string $formulairesModeles): self
    {
        $this->formulairesModeles = $formulairesModeles;

        return $this;
    }

    public function getDonneeConcernee(): ?string
    {
        return $this->donneeConcernee;
    }

    public function setDonneeConcernee(?string $donneeConcernee): self
    {
        $this->donneeConcernee = $donneeConcernee;

        return $this;
    }

    public function getStructure(): ?Structure
    {
        return $this->structure;
    }

    public function setStructure(?Structure $structure): self
    {
        $this->structure = $structure;

        return $this;
    }

    public function getSrodUtilisateur(): ?Utilisateur
    {
        return $this->srodUtilisateur;
    }

    public function setSrodUtilisateur(?Utilisateur $srodUtilisateur): self
    {
        $this->srodUtilisateur = $srodUtilisateur;

        return $this;
    }


}
