<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Dispositif
 *
 * @ORM\Table(name="dispositif", indexes={@ORM\Index(name="fk_dispositif_utilisateur1_idx", columns={"srod_utilisateur_id"}), @ORM\Index(name="fk_dispositif_structure1_idx", columns={"structure_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\DispositifRepository")
 */
class Dispositif
{
    const NUM_ITEMS = 10;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true)
     */
    private $updatedAt;

    /**
     * @var string
     * @ORM\Column(name="libelle", type="string", length=128, nullable=false)
     */
    private $libelle;

    /**
     * @var string
     *
     * @ORM\Column(name="plateforme", type="string", length=0, nullable=false)
     */
    private $plateforme;

    /**
     * @var bool
     *
     * @ORM\Column(name="etat", type="boolean", nullable=false)
     */
    private $etat;

    /**
     * @var bool
     *
     * @ORM\Column(name="cp", type="boolean", nullable=false)
     */
    private $cp;

    /**
     * @var string
     *
     * @ORM\Column(name="criticite", type="string", length=0, nullable=false)
     */
    private $criticite;

    /**
     * @var string
     *
     * @ORM\Column(name="complexite", type="string", length=0, nullable=false)
     */
    private $complexite;

    /**
     * @var string
     *
     * @ORM\Column(name="version_prod", type="float", nullable=false)
     */
    private $versionProd;

    /**
     * @var bool
     *
     * @ORM\Column(name="eligible_reversibilite", type="boolean", nullable=true)
     */
    private $eligibleReversibilite = '0';

    /**
     * @var string|null
     * 
     * @ORM\Column(name="description", type="string", nullable=true)
     */
    private $description;

    /**
     * @var string|null
     *
     * @ORM\Column(name="cedre", type="string", length=255, nullable=true)
     */
    private $cedre;                                                                                                                                             
    /**
     * @var string|null                                                                                                                                                                                                                                                                 
     *
     * @ORM\Column(name="referentiels", type="text", length=0, nullable=true)
     */
    private $referentiels;

    /**
     * @var string|null
     *
     * @ORM\Column(name="domaine_bindings", type="text", length=0, nullable=true)
     */
    private $domaineBindings;

    /**
     * @var string|null
     *
     * @ORM\Column(name="regles_metier", type="text", length=0, nullable=true)
     */
    private $reglesMetier;

    /**
     * @var string|null
     *
     * @ORM\Column(name="javascript_partages", type="text", length=0, nullable=true)
     */
    private $javascriptPartages;

    /**
     * @var string|null
     *
     * @ORM\Column(name="styles_partages", type="text", length=0, nullable=true)
     */
    private $stylesPartages;

    /**
     * @var string|null
     *
     * @ORM\Column(name="formulaires_modeles", type="text", length=0, nullable=true)
     */
    private $formulairesModeles;

    /**
     * @ORM\OneToMany(targetEntity=Commentaire::class, mappedBy="dispositif", orphanRemoval=true)
     */
    private $commentaires;

    /**
     * @var \Structure
     *
     * @ORM\ManyToOne(targetEntity="Structure")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="structure_id", referencedColumnName="id")
     * })
     */
    private $structure;

    /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="srod_utilisateur_id", referencedColumnName="id")
     * })
     * @Assert\NotBlank(message="Veuillez saisir un référent SROD valide.")
     */
    private $srodUtilisateur;

    /**
     * @ORM\OneToMany(targetEntity="Evolution", mappedBy="dispositif")
     * @ORM\OrderBy({"createdAt" = "DESC"}) 
     */
    private $evolutions;

    public function __construct()
    {
        $this->commentaires = new ArrayCollection();
        $this->evolutions = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->libelle;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(?\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getPlateforme(): ?string
    {
        return $this->plateforme;
    }

    public function setPlateforme(string $plateforme): self
    {
        $this->plateforme = $plateforme;

        return $this;
    }

    public function getEtat(): ?bool
    {
        return $this->etat;
    }

    public function setEtat(bool $etat): self
    {
        $this->etat = $etat;

        return $this;
    }

    public function getCriticite(): ?string
    {
        return $this->criticite;
    }

    public function setCriticite(string $criticite): self
    {
        $this->criticite = $criticite;

        return $this;
    }

    public function getComplexite(): ?string
    {
        return $this->complexite;
    }

    public function setComplexite(string $complexite): self
    {
        $this->complexite = $complexite;

        return $this;
    }

    public function getVersionProd(): ?string
    {
        return $this->versionProd;
    }

    public function setVersionProd(string $versionProd): self
    {
        $this->versionProd = $versionProd;

        return $this;
    }

    public function getEligibleReversibilite(): ?bool
    {
        return $this->eligibleReversibilite;
    }

    public function setEligibleReversibilite(bool $eligibleReversibilite): self
    {
        $this->eligibleReversibilite = $eligibleReversibilite;

        return $this;
    }

    public function getCedre(): ?string
    {
        return $this->cedre;
    }

    public function setCedre(?string $cedre): self
    {
        $this->cedre = $cedre;

        return $this;
    }

    public function getReferentiels(): ?string
    {
        return $this->referentiels;
    }

    public function setReferentiels(?string $referentiels): self
    {
        $this->referentiels = $referentiels;

        return $this;
    }

    public function getDomaineBindings(): ?string
    {
        return $this->domaineBindings;
    }

    public function setDomaineBindings(?string $domaineBindings): self
    {
        $this->domaineBindings = $domaineBindings;

        return $this;
    }

    public function getReglesMetier(): ?string
    {
        return $this->reglesMetier;
    }

    public function setReglesMetier(?string $reglesMetier): self
    {
        $this->reglesMetier = $reglesMetier;

        return $this;
    }

    public function getJavascriptPartages(): ?string
    {
        return $this->javascriptPartages;
    }

    public function setJavascriptPartages(?string $javascriptPartages): self
    {
        $this->javascriptPartages = $javascriptPartages;

        return $this;
    }

    public function getStylesPartages(): ?string
    {
        return $this->stylesPartages;
    }

    public function setStylesPartages(?string $stylesPartages): self
    {
        $this->stylesPartages = $stylesPartages;

        return $this;
    }

    public function getFormulairesModeles(): ?string
    {
        return $this->formulairesModeles;
    }

    public function setFormulairesModeles(?string $formulairesModeles): self
    {
        $this->formulairesModeles = $formulairesModeles;

        return $this;
    }

    public function getStructure()
    {
        return $this->structure;
    }

    public function setStructure(?Structure $structure): self
    {
        $this->structure = $structure;

        return $this;
    }

    public function getSrodUtilisateur()
    {
        return $this->srodUtilisateur;
    }

    public function setSrodUtilisateur(?Utilisateur $srodUtilisateur): self
    {
        $this->srodUtilisateur = $srodUtilisateur;

        return $this;
    }

    public function getCp()
    {
        return $this->cp;
    }

    public function setCp(int $cp): self
    {
        $this->cp = $cp;

        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return Collection|Evolution[]
     */
    public function getEvolutions(): Collection
    {
        return $this->evolutions;
    }

    public function addEvolutions(Evolution $evolutions): self
    {
        if (!$this->evolutions->contains($evolutions)) {
            $this->evolutions[] = $evolutions;
            $evolutions->setDispositif($this);
        }

        return $this;
    }

    public function removeEvolutions(Evolution $evolutions): self
    {
        if ($this->evolutions->contains($evolutions)) {
            $this->evolutions->removeElement($evolutions);
            if ($evolutions->getDispositif() === $this) {
                $evolutions->setDispositif(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Commentaires[]
     */
    public function getCommentaires(): Collection
    {
        return $this->commentaires;
    }

    public function addCommentaire(Commentaire $commentaire): self
    {
        if (!$this->commentaires->contains($commentaire)) {
            $this->commentaires[] = $commentaire;
            $commentaire->setDispositif($this);
        }

        return $this;
    }

    public function removeCommentaire(Commentaire $commentaire): self
    {
        if ($this->commentaires->contains($commentaire)) {
            $this->commentaires->removeElement($commentaire);
            if ($commentaire->getDispositif() === $this) {
                $commentaire->setDispositif(null);
            }
        }

        return $this;
    }

    public function addEvolution(Evolution $evolution): self
    {
        if (!$this->evolutions->contains($evolution)) {
            $this->evolutions[] = $evolution;
            $evolution->setDispositif($this);
        }

        return $this;
    }

    public function removeEvolution(Evolution $evolution): self
    {
        if ($this->evolutions->contains($evolution)) {
            $this->evolutions->removeElement($evolution);
            if ($evolution->getDispositif() === $this) {
                $evolution->setDispositif(null);
            }
        }

        return $this;
    }

}