<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * MailReporting
 *
 * @ORM\Table(name="mail_reporting", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"})}, indexes={@ORM\Index(name="fk_copieA", columns={"copie_a"}), @ORM\Index(name="fk_destinataires", columns={"destinataires"})})
 * @ORM\Entity
 */
class MailReporting
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="vue_source", type="string", length=0, nullable=true)
     */
    private $vueSource;

    /**
     * @var int
     *
     * @ORM\Column(name="delai_statut", type="integer", nullable=false)
     */
    private $delaiStatut;

    /**
     * @var string
     *
     * @ORM\Column(name="objet", type="text", length=65535, nullable=false)
     */
    private $objet;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_etat_lieux", type="datetime", nullable=false)
     */
    private $dateEtatLieux;

    /**
     * @var int
     *
     * @ORM\Column(name="nombre_commentaires", type="integer", nullable=false)
     */
    private $nombreCommentaires;

    /**
    * @ORM\ManyToMany(targetEntity="Utilisateur")
    * @ORM\JoinTable(name="mail_reporting_utilisateur",
    *      joinColumns={@ORM\JoinColumn(name="mailreporting_id", referencedColumnName="id")},
    *      inverseJoinColumns={@ORM\JoinColumn(name="utilisateur_id", referencedColumnName="id")}
    *      )
    */
    private $destinataires;

    /**
     * @var string
     *
     * @ORM\Column(name="destinataires_en_plus", type="string", nullable=true)
     */
    private $destinatairesEnPlus;

    public function __construct()
    {
        $this->destinataires = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getVueSource(): ?string
    {
        return $this->vueSource;
    }

    public function setVueSource(string $vueSource): self
    {
        $this->vueSource = $vueSource;

        return $this;
    }

    public function getDelaiStatut(): ?int
    {
        return $this->delaiStatut;
    }

    public function setDelaiStatut(int $delaiStatut): self
    {
        $this->delaiStatut = $delaiStatut;

        return $this;
    }

    public function getObjet(): ?string
    {
        return $this->objet;
    }

    public function setObjet(string $objet): self
    {
        $this->objet = $objet;

        return $this;
    }

    public function getDateEtatLieux(): ?\DateTimeInterface
    {
        return $this->dateEtatLieux;
    }

    public function setDateEtatLieux(\DateTimeInterface $dateEtatLieux): self
    {
        $this->dateEtatLieux = $dateEtatLieux;

        return $this;
    }

    public function getNombreCommentaires(): ?int
    {
        return $this->nombreCommentaires;
    }

    public function setNombreCommentaires(int $nombreCommentaires): self
    {
        $this->nombreCommentaires = $nombreCommentaires;

        return $this;
    }

    /**
     * @return Collection|Utilisateur[]
     */
    public function getDestinataires(): Collection
    {
        return $this->destinataires;
    }

    public function addDestinataire(Utilisateur $destinataire): self
    {
        if (!$this->destinataires->contains($destinataire)) {
            $this->destinataires[] = $destinataire;
        }

        return $this;
    }

    public function removeDestinataire(Utilisateur $destinataire): self
    {
        if ($this->destinataires->contains($destinataire)) {
            $this->destinataires->removeElement($destinataire);
        }

        return $this;
    }

    public function getDestinatairesEnPlus(): ?string
    {
        return $this->destinatairesEnPlus;
    }

    public function setDesTinatairesEnPlus(string $destinatairesEnPlus): self
    {
        $this->destinatairesEnPlus = $destinatairesEnPlus;

        return $this;
    }


}
