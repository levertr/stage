<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Structure
 *
 * @ORM\Table(name="structure", indexes={@ORM\Index(name="fk_structure_structure1_idx", columns={"pere_id"})})
 * @ORM\Entity
 */
class Structure
{
    private $branches = [];

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string", length=8, nullable=false)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=128, nullable=false)
     */
    private $libelle;

    /**
     * @var \Structure
     *
     * @ORM\ManyToOne(targetEntity="Structure", inversedBy="enfants")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="pere_structure_id", referencedColumnName="id")
     * })
     */
    private $pere;

    /**
     * @var \StructureType
     *
     * @ORM\ManyToOne(targetEntity="StructureType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="structure_type_id", referencedColumnName="id")
     * })
     */
    private $structureType;

    /**
     * @var \Structure
     *
     * @ORM\OneToMany(targetEntity="Structure", mappedBy="pere")
     */
    private $enfants;

    public function __construct()
    {
        $this->enfants = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->libelle;
    }

    /**
     * Retourne le niveau d'une structure (celui de son type de structure)
     * @return int|null
     */
    public function getNiveau()
    {
        return $this->getStructureType()->getNiveau();
    }

    /**
     * Retourne le libellé indenté (avec le caractère UTF8 d'espace) par le niveau. Utile pour les selects
     * @return string
     */
    public function getLibelleIndente() {
        return str_repeat("\xC2\xA0\xC2\xA0\xC2\xA0\xC2\xA0", $this->getNiveau()) . $this->code . ' - ' . $this->libelle;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCode(): ?string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getPere()
    {
        return $this->pere;
    }

    public function setPere(?self $pere): self
    {
        $this->pere = $pere;

        return $this;
    }

    public function getStructureType()
    {
        return $this->structureType;
    }

    public function setStructureType(?StructureType $structureType): self
    {
        $this->structureType = $structureType;

        return $this;
    }

    /**
     * @return Collection|Structure[]
     */
    public function getEnfants()
    {
        return $this->enfants;
    }

    public function addEnfant(Structure $enfant): self
    {
        if (!$this->enfants->contains($enfant)) {
            $this->enfants[] = $enfant;
            $enfant->setPere($this);
        }

        return $this;
    }

    public function removeEnfant(Structure $enfant): self
    {
        if ($this->enfants->contains($enfant)) {
            $this->enfants->removeElement($enfant);
            // set the owning side to null (unless already changed)
            if ($enfant->getPere() === $this) {
                $enfant->setPere(null);
            }
        }

        return $this;
    }

}