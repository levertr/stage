<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Evolution
 *
 * @ORM\Table(name="evolution", uniqueConstraints={@ORM\UniqueConstraint(name="id_UNIQUE", columns={"id"}), @ORM\UniqueConstraint(name="dispositif_UNIQUE", columns={"dispositif_id"}), @ORM\UniqueConstraint(name="version_UNIQUE", columns={"version"})}, indexes={@ORM\Index(name="fk_evolution_utilisateur2_idx", columns={"dsi_utilisateur_id"}), @ORM\Index(name="fk_evolution_utilisateur1_idx", columns={"srod_utilisateur_id"}), @ORM\Index(name="fk_evolution_statut_id", columns={"evolution_statut_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\EvolutionRepository")
 */
class Evolution
{
    const NUM_ITEMS = 20;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime", nullable=false)
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=false)
     */
    private $updatedAt;

    /**
     * @var string
     *
     * @ORM\Column(name="libelle", type="string", length=128, nullable=false)
     */
    private $libelle;

    /**
     * @var string
     *
     * @ORM\Column(name="version", type="float", nullable=false)
     */
    private $version;

    /**
     * @var string
     *
     * @ORM\Column(name="priorite", type="string", length=0, nullable=false)
     */
    private $priorite;

    /**
     * @var string
     *
     * @ORM\Column(name="complexite", type="string", length=0, nullable=false)
     */
    private $complexite;

    /**
     * @var string|null
     * 
     * @ORM\Column(name="description", type="string", nullable=true)
     */
    private $description;

    /**
     * @var boolean
     *
     * @ORM\Column(name="bowi", type="boolean", nullable=false)
     * @Assert\Expression(
     *      "not ( this.getBowiUtilisateur() == NULL && this.getBowi() == 1 )",
     *      message = "Une affectation BOWI doit être renseignée."     
     * )
     */
    private $bowi;
    
                                                        //// DATES REELLES ////
    /**
     * @var \Date|null
     *
     * @ORM\Column(name="ouverture_production", type="date", nullable=true, options={"default"="NULL"})
     * @Assert\GreaterThanOrEqual(propertyPath="livraisonRecetteDirection", message="L'ouverture en production doit être après la livraison Direction.")
     */
    private $ouvertureProduction;

    /**
     * @var \Date|null
     *
     * @ORM\Column(name="livraison_recette_direction", type="date", nullable=true, options={"default"="NULL"})
     */
    private $livraisonRecetteDirection;

    /**
     * @var \Date|null
     *
     * @ORM\Column(name="livraison_recette_srod", type="date", nullable=true, options={"default"="NULL"})
     * @Assert\LessThanOrEqual(propertyPath="livraisonRecetteDirection", message="La recette SROD doit être livrée avant la direction.")
     * @Assert\LessThanOrEqual(propertyPath="ouvertureProduction", message="La recette SROD doit être avant l'ouverture production.")
     */
    private $livraisonRecetteSrod;

                                                        //// DATES PREVISIONNELLES ////
    /**
     * @var \Date|null
     *
     * @ORM\Column(name="ouverture_production_previsionnelle", type="date", nullable=true, options={"default"="NULL"})
     * @Assert\GreaterThanOrEqual(propertyPath="livraisonRecetteDirectionPrevisionnelle", message="L'ouverture en production doit être après la livraison Direction.")
     */
    private $ouvertureProductionPrevisionnelle;

    /**
     * @var \Date|null
     *
     * @ORM\Column(name="livraison_recette_direction_previsionnelle", type="date", nullable=true, options={"default"="NULL"})
     */
    private $livraisonRecetteDirectionPrevisionnelle;

                                                        
    /**
     * @var \Date|null
     *
     * @ORM\Column(name="livraison_recette_srod_previsionnelle", type="date", nullable=true, options={"default"="NULL"})
     * @Assert\LessThanOrEqual(propertyPath="livraisonRecetteDirectionPrevisionnelle", message="La recette SROD doit être livrée avant la direction.")
     * @Assert\LessThanOrEqual(propertyPath="ouvertureProductionPrevisionnelle", message="La recette SROD doit être avant l'ouverture production.")
     */
    private $livraisonRecetteSrodPrevisionnelle;

                                                        //// DATES SOUHAITEES ////
    /**
     * @var \Date|null
     *
     * @ORM\Column(name="ouverture_production_souhaitee", type="date", nullable=true, options={"default"="NULL"})
     * @Assert\GreaterThanOrEqual(propertyPath="livraisonRecetteDirectionSouhaitee", message="L'ouverture en production doit être après la livraison Direction.")
     */
    private $ouvertureProductionSouhaitee;

    /**
     * @var \Date|null
     *
     * @ORM\Column(name="livraison_recette_direction_souhaitee", type="date", nullable=true, options={"default"="NULL"})
     */
    private $livraisonRecetteDirectionSouhaitee;

    /**
     * @var \Date|null
     *
     * @ORM\Column(name="livraison_recette_srod_souhaitee", type="date", nullable=true, options={"default"="NULL"})
     * @Assert\LessThanOrEqual(propertyPath="livraisonRecetteDirectionSouhaitee", message="La recette SROD doit être livrée avant la direction.")
     * @Assert\LessThanOrEqual(propertyPath="ouvertureProductionSouhaitee", message="La recette SROD doit être avant l'ouverture production.")
     */
    private $livraisonRecetteSrodSouhaitee;

    /**
     * @var \Dispositif
     *
     * @ORM\ManyToOne(targetEntity="Dispositif", inversedBy="evolutions")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="dispositif_id", referencedColumnName="id")
     * })
     * @Assert\Expression(
     *      "not (this.getDispositif() == NULL)",
     *      message = "Veuillez saisir un dispositif valide."    
     * )
     */
    private $dispositif;

    /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="srod_utilisateur_id", referencedColumnName="id")
     * })
     * @Assert\NotBlank(message="Veuillez saisir un référent SROD valide.")
     */
    private $srodUtilisateur;

    /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="dsi_utilisateur_id", referencedColumnName="id")
     * })
     */
    private $dsiUtilisateur;

    /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="bowi_utilisateur_id", referencedColumnName="id")
     * })
     * @Assert\Expression(
     *      "not ( this.getBowiUtilisateur() && this.getBowi() == 0 )",
     *      message = "L'évolution doit être BOWI."     
     * )
     */
    private $bowiUtilisateur;

    /**
     * @ORM\OneToMany(targetEntity=Commentaire::class, mappedBy="evolution", orphanRemoval=true)
     */
    private $commentaires;

    /**
     * @ORM\OneToMany(targetEntity="EvolutionStatut", mappedBy="evolution", orphanRemoval=true, cascade={"persist"})
     * @Assert\Expression(
     *     "not ( this.getDsiUtilisateur() == NULL && ( this.getEvolutionStatuts()[0].getStatut().getId() > 1) )",
     *      message="L’affectation DSI est obligatoire ")
     * @ORM\OrderBy({"createdAt" = "desc"})
     */
    private $evolutionStatuts;

    private $nbCommentaires;

    public function __construct()
    {
        $this->createdAt = new \DateTime('now');
        $this->commentaires = new ArrayCollection();
        $this->evolutionStatuts = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->libelle;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->createdAt;
    }

    public function setCreatedAt(\DateTimeInterface $createdAt): self
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getLibelle(): ?string
    {
        return $this->libelle;
    }

    public function setLibelle(string $libelle): self
    {
        $this->libelle = $libelle;

        return $this;
    }

    public function getVersion(): ?string
    {
        return $this->version;
    }

    public function setVersion(string $version): self
    {
        $this->version = $version;

        return $this;
    }

    public function getPriorite(): ?string
    {
        return $this->priorite;
    }

    public function setPriorite(string $priorite): self
    {
        $this->priorite = $priorite;

        return $this;
    }

    public function getComplexite(): ?string
    {
        return $this->complexite;
    }

    public function setComplexite(string $complexite): self
    {
        $this->complexite = $complexite;

        return $this;
    }
    
    public function getBowi()
    {
        return $this->bowi;
    }

    public function setBowi(int $bowi)
    {
        $this->bowi = $bowi;

        return $this;
    }

    public function getaffectationDsi(): ?string
    {
        return $this->affectationDsi;
    }

    public function setaffectationDsi(?string $affectationDsi): self
    {
        $this->affectationDsi = $affectationDsi;

        return $this;
    }

                        //// DATES REELLES ////

    public function getOuvertureProduction(): ?\DateTimeInterface
    {
        return $this->ouvertureProduction;
    }

    public function setOuvertureProduction(?\DateTimeInterface $ouvertureProduction): self
    {
        $this->ouvertureProduction = $ouvertureProduction;

        return $this;
    }

    public function getLivraisonRecetteDirection(): ?\DateTimeInterface
    {
        return $this->livraisonRecetteDirection;
    }

    public function setLivraisonRecetteDirection(?\DateTimeInterface $livraisonRecetteDirection): self
    {
        $this->livraisonRecetteDirection = $livraisonRecetteDirection;

        return $this;
    }

    public function getLivraisonRecetteSrod(): ?\DateTimeInterface
    {
        return $this->livraisonRecetteSrod;
    }

    public function setLivraisonRecetteSrod(?\DateTimeInterface $livraisonRecetteSrod): self
    {
        $this->livraisonRecetteSrod = $livraisonRecetteSrod;

        return $this;
    }

                            //// DATES PREVISIONNELLES ////

    public function getOuvertureProductionPrevisionnelle(): ?\DateTimeInterface
    {
        return $this->ouvertureProductionPrevisionnelle;
    }

    public function setOuvertureProductionPrevisionnelle(?\DateTimeInterface $ouvertureProductionPrevisionnelle): self
    {
        $this->ouvertureProductionPrevisionnelle = $ouvertureProductionPrevisionnelle;

        return $this;
    }

    public function getLivraisonRecetteDirectionPrevisionnelle(): ?\DateTimeInterface
    {
        return $this->livraisonRecetteDirectionPrevisionnelle;
    }

    public function setLivraisonRecetteDirectionPrevisionnelle(?\DateTimeInterface $livraisonRecetteDirectionPrevisionnelle): self
    {
        $this->livraisonRecetteDirectionPrevisionnelle = $livraisonRecetteDirectionPrevisionnelle;

        return $this;
    }

    public function getLivraisonRecetteSrodPrevisionnelle(): ?\DateTimeInterface
    {
        return $this->livraisonRecetteSrodPrevisionnelle;
    }

    public function setLivraisonRecetteSrodPrevisionnelle(?\DateTimeInterface $livraisonRecetteSrodPrevisionnelle): self
    {
        $this->livraisonRecetteSrodPrevisionnelle = $livraisonRecetteSrodPrevisionnelle;

        return $this;
    }

                        //// DATES SOUHAITEES ////

    public function getOuvertureProductionSouhaitee(): ?\DateTimeInterface
    {
        return $this->ouvertureProductionSouhaitee;
    }

    public function setOuvertureProductionSouhaitee(?\DateTimeInterface $ouvertureProductionSouhaitee): self
    {
        $this->ouvertureProductionSouhaitee = $ouvertureProductionSouhaitee;

        return $this;
    }

    public function getLivraisonRecetteDirectionSouhaitee(): ?\DateTimeInterface
    {
        return $this->livraisonRecetteDirectionSouhaitee;
    }

    public function setLivraisonRecetteDirectionSouhaitee(?\DateTimeInterface $livraisonRecetteDirectionSouhaitee): self
    {
        $this->livraisonRecetteDirectionSouhaitee = $livraisonRecetteDirectionSouhaitee;

        return $this;
    }

    public function getLivraisonRecetteSrodSouhaitee(): ?\DateTimeInterface
    {
        return $this->livraisonRecetteSrodSouhaitee;
    }

    public function setLivraisonRecetteSrodSouhaitee(?\DateTimeInterface $livraisonRecetteSrodSouhaitee): self
    {
        $this->livraisonRecetteSrodSouhaitee = $livraisonRecetteSrodSouhaitee;

        return $this;
    }

    public function getDispositif()
    {
        return $this->dispositif;
    }

    public function setDispositif(?Dispositif $dispositif): self
    {
        $this->dispositif = $dispositif;

        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getSrodUtilisateur()
    {
        return $this->srodUtilisateur;
    }

    public function setSrodUtilisateur(?Utilisateur $srodUtilisateur): self
    {
        $this->srodUtilisateur = $srodUtilisateur;

        return $this;
    }

    public function getDsiUtilisateur()
    {
        return $this->dsiUtilisateur;
    }

    public function setDsiUtilisateur(?Utilisateur $dsiUtilisateur): self
    {
        $this->dsiUtilisateur = $dsiUtilisateur;

        return $this;
    }

    public function getBowiUtilisateur()
    {
        return $this->bowiUtilisateur;
    }

    public function setBowiUtilisateur(?Utilisateur $bowiUtilisateur): self
    {
        $this->bowiUtilisateur = $bowiUtilisateur;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getEvolutionStatuts(): Collection
    {
        return $this->evolutionStatuts;
    }

    public function addEvolutionStatut(EvolutionStatut $evolutionStatut): self
    {
        if (!$this->evolutionStatuts->contains($evolutionStatut)) {
            $this->evolutionStatuts[] = $evolutionStatut;
            $evolutionStatut->setEvolution($this);
        }

        return $this;
    }

    public function removeEvolutionStatut(EvolutionStatut $evolutionStatut): self
    {
        if ($this->evolutionStatuts->contains($evolutionStatut)) {
            $this->evolutionStatuts->removeElement($evolutionStatut);
            if ($evolutionStatut->getEvolution() === $this) {
                $evolutionStatut->setEvolution(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection
     */
    public function getCommentaires(): Collection
    {
        return $this->commentaires;
    }

    public function getNbCommentaires(): int
    {
        $this->nbCommentaires = count($this->commentaires);
        return $this->nbCommentaires;
    }

    public function addCommentaire(Commentaire $commentaire): self
    {
        if (!$this->commentaires->contains($commentaire)) {
            $this->commentaires[] = $commentaire;
            $commentaire->setEvolution($this);
        }

        return $this;
    }

    public function removeCommentaire(Commentaire $commentaire): self
    {
        if ($this->commentaires->contains($commentaire)) {
            $this->commentaires->removeElement($commentaire);
            if ($commentaire->getEvolution() === $this) {
                $commentaire->setEvolution(null);
            }
        }

        return $this;
    }

}