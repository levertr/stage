<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MailReportingUtilisateur
 *
 * @ORM\Table(name="mail_reporting_utilisateur", uniqueConstraints={@ORM\UniqueConstraint(name="id", columns={"id"})}, indexes={@ORM\Index(name="fk_mail_reporting_utilisateur_idx1", columns={"utilisateur_id"}), @ORM\Index(name="fk_mail_reporting_mailreporting_idx1", columns={"mailreporting_id"})})
 * @ORM\Entity(repositoryClass="App\Repository\MailReportingRepository")
 */
class MailReportingUtilisateur
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \MailReporting
     *
     * @ORM\ManyToOne(targetEntity="MailReporting", inversedBy="mailreportingUtilisateurs", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="mailreporting_id", referencedColumnName="id")
     * })
     */
    private $mailreporting;

    /**
     * @var \Utilisateur
     *
     * @ORM\ManyToOne(targetEntity="Utilisateur", inversedBy="mailreportingUtilisateurs", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="utilisateur_id", referencedColumnName="id")
     * })
     */
    private $utilisateur;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getMailreporting(): ?MailReporting
    {
        return $this->mailreporting;
    }

    public function setMailreporting(?MailReporting $mailreporting): self
    {
        $this->mailreporting = $mailreporting;

        return $this;
    }

    public function getUtilisateur(): ?Utilisateur
    {
        return $this->utilisateur;
    }

    public function setUtilisateur(?Utilisateur $utilisateur): self
    {
        $this->utilisateur = $utilisateur;

        return $this;
    }
}
