SET FOREIGN_KEY_CHECKS=0;
SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";

DROP TABLE IF EXISTS `acces`;
CREATE TABLE `acces` (
  `id` int(11) UNSIGNED NOT NULL,
  `utilisateur_id` int(10) UNSIGNED NOT NULL,
  `date` datetime NOT NULL,
  `ip` varchar(16) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE `utilisateur` (
  `id` int(10) UNSIGNED NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `civilite` varchar(3) NOT NULL,
  `nom` varchar(64) NOT NULL,
  `prenom` varchar(32) NOT NULL,
  `email` varchar(128) NOT NULL,
  `login` varchar(16) NOT NULL,
  `telephone` varchar(15) DEFAULT NULL,
  `role` varchar(32) NOT NULL,
  `actif` tinyint(1) NOT NULL DEFAULT 0,
  `token` varchar(64) DEFAULT NULL,
  `token_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `utilisateur` (`id`, `created_at`, `updated_at`, `civilite`, `nom`, `prenom`, `email`, `login`, `telephone`, `role`, `actif`, `token`, `token_date`) VALUES
(1, '2019-04-12 00:00:00', '2020-01-31 16:24:31', 'M.', 'BOURGEOIS', 'David', 'david.bourgeois@hautsdefrance.fr', 'dbourgeo', '0374276128', 'Super administrateur', 1, NULL, NULL),
(2, '2019-04-16 08:57:23', '2020-04-23 09:04:04', 'M.', 'LOUCHET', 'Olivier', 'olivier.louchet@hautsdefrance.fr', 'olouchet', '0374276110', 'Super administrateur', 0, NULL, NULL);


ALTER TABLE `acces`
  ADD PRIMARY KEY (`id`),
  ADD KEY `fk_acces_utilisateur1_idx` (`utilisateur_id`);

ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`id`);


ALTER TABLE `acces`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

ALTER TABLE `utilisateur`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;


ALTER TABLE `acces`
  ADD CONSTRAINT `fk_acces_utilisateur1` FOREIGN KEY (`utilisateur_id`) REFERENCES `utilisateur` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION;
SET FOREIGN_KEY_CHECKS=1;
COMMIT;
